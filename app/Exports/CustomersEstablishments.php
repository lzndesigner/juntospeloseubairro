<?php

namespace App\Exports;

use DB;
use App\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomersEstablishments implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('customer_plans as a')
        ->select('e.id as clienteid', 'e.name as cliente', 'e.email as clienteemail', 'e.phone as clientephone', 'e.document as clientedocumento', 'e.plan_discount as clientediscount', 'b.id as estabelecimentoid', 'b.name as estabelecimento', 'c.title as estabelecimentocategoria', 'b.cep as estabelecimentocep', 'b.distric as estabelecimentodistric', 'b.city as estabelecimentocity', 'b.state as estabelecimentostate', 'b.views as estabelecimentoview', 'b.slug as estabelecimentourl', 'd.name as plano', 'd.price as planoprice', 'a.plan_status as planostatus', 'a.renews as planorenovacao', 'a.plan_datebegin as planocriado', 'a.plan_dateend as planovencimento', 'a.updated_at as planoatualizacao', 'b.promotion as estabelecimentopromocao')
        ->join('establishments as b', 'a.establishment_id', 'b.id')
        ->join('categories as c', 'b.category_id', 'c.id')
        ->leftjoin('plans as d', 'a.plan_id', 'd.id')
        ->join('customers as e', 'a.customer_id', 'e.id')
        ->orderBy('cliente', 'ASC')
        ->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Cliente',
            'E-mail',
            'Telefone',
            'Documento',
            'Desconto',
            '#',
            'Estabelecimento',
            'Categoria',
            'CEP',
            'Bairro',
            'Cidade',
            'Estado',
            'Views',
            'URL Amigável',
            'Plano',
            'Valor',
            'Status',
            'Renovações',
            'Contratado',
            'Vencimento',
            'Atualizado',
            'Promoção',
        ];
    }

    /**
     * @var Customer $customer
     * @return array
     */
    public function map($customer): array
    {
        return [
            $customer->clienteid,
            $customer->cliente,
            $customer->clienteemail,
            $customer->clientephone,
            $customer->clientedocumento,
            $customer->clientediscount,
            $customer->estabelecimentoid,
            $customer->estabelecimento,
            $customer->estabelecimentocategoria,
            $customer->estabelecimentocep,
            $customer->estabelecimentodistric,
            $customer->estabelecimentocity,
            $customer->estabelecimentostate,
            $customer->estabelecimentoview,
            $customer->estabelecimentourl,
            $customer->plano,
            $customer->planoprice,
            $customer->planostatus,
            $customer->planorenovacao,
            $customer->planocriado,
            $customer->planovencimento,
            $customer->planoatualizacao,
            $customer->estabelecimentopromocao,
        ];
    }
}
