<?php

namespace App\Exports;

use DB;
use App\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Customers implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table("customers")
        ->select('customers.id as id', 'customers.name', 'customers.email', 'customers.phone', 'customers.document', 'customers.plan_discount', 'customers.created_at',
        DB::raw("COUNT(establishments.customer_id) as qtaEsta"))
        ->leftjoin("establishments", "establishments.customer_id", "customers.id")
        ->groupBy("customers.id")
        ->having('qtaEsta', '0')
        ->orderBy('customers.name', 'ASC')
        ->get();
    }

    public function headings(): array
    {
        return [
            '#',
            'Cliente',
            'E-mail',
            'Telefone',
            'Documento',
            'Desconto',
            'Criado',
        ];
    }

    /**
     * @var Customer $customer
     * @return array
     */
    public function map($customer): array
    {
        return [
            $customer->id,
            $customer->name,
            $customer->email,
            $customer->phone,
            $customer->document,
            $customer->plan_discount,
            $customer->created_at,
        ];
    }
}
