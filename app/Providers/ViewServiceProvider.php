<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider 
{
	public function boot()
	{
		// * -> va njetar em qualquer arquivo, como queremos por em caso de escopo de logado,
		// temos que nformar o nome da view -- entendi
		View::composer('front.customers.*', function($view) {
			return $view->with('currentUser', auth()->user());
		});
	}
}