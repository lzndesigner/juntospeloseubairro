<?php

namespace App\Providers;

use App\Models\ReservationInterest;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();

        Route::bind('slugCategoria', function($value) {
            return \App\Models\Category::whereSlug($value)->firstOrFail();
        });    
        Route::bind('slugPage', function($value) {
            return \App\Models\Page::whereSlug($value)->firstOrFail();
        });       
        Route::bind('slugEstabelecimento', function($value) {
            return \App\Models\Establishment::whereSlug($value)->firstOrFail();
        });
        Route::bind('slugEstablishment', function($value) {
            return \App\Models\Establishment::whereSlug($value)->firstOrFail();
        });
        Route::bind('slugBlog', function($value) {
            return \App\Models\Blog::whereSlug($value)->firstOrFail();
        });

        Route::bind('slugCalendario', function($value) {
            return \App\Models\Calendar::whereSlug($value)->firstOrFail();
        });

    }
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
