<?php

namespace App\Http\Requests\Admin\CustomerPlans;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'plan_id' => ['required'],
      'plan_status' => ['required'],
      'plan_datebegin' => ['required'],
      'plan_dateend' => ['required'],
      'plan_method' => ['required']
    ];
  }
}
