<?php

namespace App\Http\Requests\Admin\CustomerPlans;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'plan_id' => ['required'],
            'plan_status' => ['required'],
            'plan_datebegin' => ['required'],
            'plan_dateend' => ['required'],
            'plan_method' => ['required']
        ];

        return $rules;

    }

    public function attributes() {
        return [
            'plan_id' => 'plano',
            'plan_status' => 'status',
            'plan_datebegin' => 'data de início',
            'plan_dateend' => 'data de vencimento',
            'plan_method' => 'método de pagamento'
        ];
    }
    
    public function messages()
    {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_title não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
        return [
            'plan_id.required' => 'O plano é necessário.',
            'plan_status.required' => 'O status é necessário.',
            'plan_datebegin.required' => 'A data de início é necessário.',
            'plan_dateend.required' => 'A data de vencimento é necessário.',
            'plan_method.required' => 'O método de pagamento é é necessário.',
        ];
    }
}
