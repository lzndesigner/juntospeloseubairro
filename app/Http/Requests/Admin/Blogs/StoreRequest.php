<?php

namespace App\Http\Requests\Admin\Blogs;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => ['required', 'min:5', 'unique:blogs'],
            'slug' => ['required'], 
            'body' => ['required', 'min:30'],
            'status' => ['required']
        ];

        $photos =  (is_array($this->input('photos')) ? count($this->input('photos')) : 0);

        foreach(range(0, $photos) as $index) {
            $rules['photos.' . $index] = 'image|mimes:jpeg,bmp,png|max:2000';
        }
 
        return $rules;

    }

    public function attributes() {
        return [
            'title' => 'título',
            'slug' => 'url amigável',
            'body' => 'conteúdo',
            'status' => 'conteúdo'
        ];
    }
    
    public function messages()
        {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_title não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
            return [
                'title.required' => 'O título é necessário.',
                'slug.required' => 'A URL Amigável é necessário.',
                'body.required'  => 'O conteúdo da página é necessário.',
                'status.required'  => 'O status da página é necessário.',
            ];
        }
}
