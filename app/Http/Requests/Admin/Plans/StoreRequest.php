<?php

namespace App\Http\Requests\Admin\Plans;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'min:4'],
            'period' => ['required'],
            'price' => ['required'],
            'statistics' => ['required'],
            'status' => ['required']
        ];

        return $rules;

    }

    public function attributes() {
        return [
            'name' => 'plano',
            'period' => 'período',
            'price' => 'preço mensal',
            'statistics' => 'estatísticas',
            'status' => 'status'
        ];
    }
    
    public function messages()
    {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_title não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
        return [
            'name.required' => 'O plano é necessário.',
            'price.required' => 'O preço mensal é necessário.',
            'period.required' => 'O período é necessário.',
            'statistics.required' => 'A opção de estatísticas é necessário.',
            'status.required' => 'O Status é necessário.',
        ];
    }
}
