<?php

namespace App\Http\Requests\Admin\Customers;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [ 
            'name' => [
                'required', 
                'min:4',
                Rule::unique('customers')->ignore($this->route('customer'))
            ],
            'email' => ['required'],
            'document' => [
                'required', 
                'min:4',
                Rule::unique('customers')->ignore($this->route('customer'))
            ],
            'status' => ['required'],
            'plan_discount' => ['required'],
            'plan_method' => ['required']
        ];
    }
}
