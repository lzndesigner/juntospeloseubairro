<?php

namespace App\Http\Requests\Admin\Customers;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'min:4'],
            'email' => [
                'required',
                Rule::unique('customers', 'email')->ignore($this->customer)
            ],
            'document' => ['required|unique:customers'],
            'phone' => ['required'],
            'status' => ['required'],
            'plan_discount' => ['required'],
            'plan_method' => ['required']
        ];

        return $rules;

    }

    public function attributes() {
        return [
            'name' => 'cliente',
            'email' => 'e-mail',
            'document' => 'documento',
            'status' => 'status',
            'plan_discount' => 'desconto',
            'plan_method' => 'método'
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => 'O cliente é necessário.',
            'email.required' => 'O e-mail é necessário.',
            'document.required' => 'O CPF/CNPJ é necessário.',
            'status.required' => 'O status é necessário.',
            'plan_discount.required' => 'O desconto é necessário.',
            'plan_method.required' => 'O método é necessário.',
        ];
    }
}
