<?php

namespace App\Http\Requests\Admin\Categories;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         $rules = [
            'title' => ['required', 'min:2'],
            'slug' => ['required', 'unique:categories,slug'],
            'status' => ['required'],
        ];

        $photos =  (is_array($this->input('photos')) ? count($this->input('photos')) : 0);

        foreach(range(0, $photos) as $index) {
            $rules['photos.' . $index] = 'image|mimes:jpeg,bmp,png|max:2000';
        }
 
        return $rules;
    }

    public function attributes() {
        return [
            'title' => 'titulo',
            'slug' => 'slug',
            'status' => 'status'
        ];
    }
    
    public function messages()
        {
            // voce refez as mensagens por que? Porque o tratamendo delas usa o name="" para informar o erro. exemplo, o campo config_title não foi preenchido, acho isso para o Cliente final meio estranho.
            // tem uma slução
            return [
                'title.required' => 'O Título é necessário.',
                'slug.required' => 'A Url Amigável é necessário.',
                'status.required' => 'O Status é necessário.',
            ];
        }
}
