<?php

namespace App\Http\Requests\Admin\Pages;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'title' => ['required', 'min:10'],
            'slug' => ['required'],
            'body' => ['required', 'min:20'],
        ];
    }

    public function messages()
        {
            return [
                'title.required' => 'O título é necessário.',
                'slug.required' => 'A URL Amigável é necessário.',
                'body.required'  => 'O conteúdo da página é necessário.',
            ];
        }
}
