<?php

namespace App\Http\Requests\Admin\Apoios;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'title' => ['required', 'min:5'],
        ];
    }

    public function attributes() {
        return [
            'title' => 'titulo',
        ];
    }
    
    public function messages()
        {
            return [
                'title.required' => 'O título é necessário.',
            ];
        }
}
