<?php

namespace App\Http\Requests\Customer\Establishments;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'min:5'],
            'category_id' => ['required'],
            'customer_id' => ['required', 'min:4'],
            'cep' => ['required'],
            'address' => ['required'],
            // 'number' => ['required'],
            'distric' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'country' => ['required'],
            'phone' => ['required'],
            'slug' => ['required'],
            'status' => ['required'],
            'type' => ['required'],
            'photos'  => ['mimes:jpg,jpeg,png|image|max:2048'],
        ];

        // sem efeito
        $photos =  (is_array($this->input('photos')) ? count($this->input('photos')) : 0);

        foreach(range(0, $photos) as $index) {
            $rules['addPhotos.' . $index] = 'mimes:jpg,jpeg,png|image';
        }

        return $rules;

    }

    
    public function attributes() {
        return [
            'name' => 'estabelecimento',
            'category_id' => 'categoria',
            'customer_id' => 'proprietário',
            'cep' => 'CEP',
            'address' => 'endereço',
            // 'number' => 'número',
            'distric' => 'bairro',
            'city' => 'cidade',
            'state' => 'estado',
            'country' => 'país',
            'phone' => 'telefone',
            'slug' => 'slug',
            'status' => 'status',
            'type' => 'tipo',
            'photos' => 'fotos',
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => 'O estabelecimento é necessário.',
            'category_id.required' => 'A categoria é necessário.',
            'customer_id.required' => 'O proprietário é necessário.',
            'cep.required' => 'O CEP é necessário.',
            'address.required' => 'O endereço é necessário.',
            // 'number.required' => 'O número é necessário.',
            'distric.required' => 'O bairro é necessário.',
            'city.required' => 'A cidade é necessário.',
            'state.required' => 'O estado é necessário.',
            'country.required' => 'O país é necessário.',
            'phone.required' => 'O telefone é necessário.',
            'slug.required' => 'A URL é necessário.',
            'status.required' => 'O Status é necessário.',
            'type.required' => 'O Tipo é necessário.',
            'photos.mimes' => 'O tipo de imagem selecionada não é válido, selecione outra.2',
        ];
    }
}
