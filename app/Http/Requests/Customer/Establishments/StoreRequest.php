<?php

namespace App\Http\Requests\Customer\Establishments;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {       

        $rules = [
            'name' => ['required', 'min:4'],
            'category_id' => ['required'],
            'customer_id' => ['required'],
            'cep' => ['required'],
            'address' => ['required'],
            // 'number' => ['required'],
            'distric' => ['required'],
            'city' => ['required'],
            'state' => ['required'],
            'country' => ['required'],
            'phone' => ['required'],
            'slug' => ['required'],
            'status' => ['required'],
            'type' => ['required'],
        ];    

        $rules['image'] = 'mimes:jpg,jpeg,png|image';
        
        foreach ($this->files as $key => $file){
            foreach($file as $k => $f){                
                $rules['photos.'.$k]  = 'mimes:jpg,jpeg,png|image';
            }            
        }
  
        return $rules;
    }

    public function attributes() {
        $attributes = [
            'name' => 'estabelecimento',
            'image' => 'capa',
            'category_id' => 'categoria',
            'customer_id' => 'proprietário',
            'cep' => 'CEP',
            'address' => 'endereço',
            'number' => 'número',
            'distric' => 'bairro',
            'city' => 'cidade',
            'state' => 'estado',
            'country' => 'país',
            'phone' => 'telefone',
            'slug' => 'slug',
            'status' => 'status',
            'type' => 'tipo',
        ];

        foreach ($this->files as $key => $file){
            foreach($file as $k => $f){                
                $attributes['photos.'.$k]  = 'foto';
            }            
        }

        return $attributes;
    }
    
    public function messages()
    {
        $messages = [
            'name.required' => 'O estabelecimento é necessário.',
            'image.mimes' => 'O tipo de imagem selecionada não é válido. Selecione apenas .jpg, .jpeg, .png',
            'category_id.required' => 'A categoria é necessário.',
            'customer_id.required' => 'O proprietário é necessário.',
            'cep.required' => 'O CEP é necessário.',
            'address.required' => 'O endereço é necessário.',
            'number.required' => 'O número é necessário.',
            'distric.required' => 'O bairro é necessário.',
            'city.required' => 'A cidade é necessário.',
            'state.required' => 'O estado é necessário.',
            'country.required' => 'O país é necessário.',
            'phone.required' => 'O telefone é necessário.',
            'slug.required' => 'A URL é necessário.',
            'status.required' => 'O Status é necessário.',
            'type.required' => 'O Tipo é necessário.',
        ];

        foreach ($this->files as $key => $file){
            foreach($file as $k => $f){                
                $attributes['photos.'.$k]  = 'O tipo de imagem selecionada não é válido. Selecione apenas .jpg, .jpeg, .png';
            }            
        }

        return $messages;
    }
}
