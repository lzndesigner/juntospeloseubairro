<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Establishment;
use DB;

class CategoriesController extends Controller
{
    /**
     * Armazena uma nova instancia do model Establishment
     *
     * @var \App\Establishment
     */
    private $categories;
    private $establishments;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
        $this->categories = app(Establishment::class);
        $this->establishments = app(Establishment::class);
    }

    public function index()
    {
        return view('front.categories');
    }

    public function show(Category $category)
    {
        $dateNow = \Carbon\Carbon::now()->format('Y-m-d');
        $categoryestablishments = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method, a.name as estabelecimento, a.slug as estabelecimentoslug, a.image as image, a.distric, a.city, a.state, c.title as categoria, a.featured, c.slug as categoriaslug, d.name as plano, d.upgrade 
            from establishments a
            inner join customer_plans b on a.id = b.establishment_id 
            inner join categories c on a.category_id = c.id
            inner join plans d on b.plan_id = d.id
            where d.upgrade = 0 and b.plan_status = 'Pago' and b.plan_dateend >= '$dateNow' and c.id = " . $category->id . " "));

        return view('front.categories_show', compact('category', 'categoryestablishments'));
    }

    public function showEstablishment(Category $category, Establishment $establishment)
    {
        $verifyEstablishment = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method, a.name as estabelecimento, a.slug as estabelecimentoslug, a.image as image, a.distric, a.city, a.state, c.title as categoria, a.featured, c.slug as categoriaslug, d.name as plano, d.upgrade 
                from establishments a
                inner join customer_plans b on a.id = b.establishment_id 
                inner join categories c on a.category_id = c.id
                inner join plans d on b.plan_id = d.id
                where a.category_id = $category->id and a.id = $establishment->id"))[0];

        $dateAtual = \Carbon\Carbon::now()->format('Y-m-d');
        $dateEnd = $verifyEstablishment->plan_dateend;

        if($dateEnd >= $dateAtual){
            $establishment = Establishment::where('category_id', $category->id)->where('id', $establishment->id)->first();
            $establishment->increment('views');
            return view('front.establishment_show', compact('category', 'establishment'));
        }else{
            return redirect()->route('home');
        }
    }

    public function showEstablishmentTwo(Establishment $establishment)
    {
        $verifyEstablishment = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method, a.name as estabelecimento, a.slug as estabelecimentoslug, a.image as image, a.distric, a.city, a.state, a.featured, d.name as plano, d.upgrade 
                from establishments a
                inner join customer_plans b on a.id = b.establishment_id 
                inner join plans d on b.plan_id = d.id
                where a.id = $establishment->id"))[0];

        $dateAtual = \Carbon\Carbon::now()->format('Y-m-d');
        $dateEnd = $verifyEstablishment->plan_dateend;

        if($dateEnd >= $dateAtual){
            $establishment = Establishment::where('id', $establishment->id)->first();
            $establishment->increment('views');
            return view('front.establishment_show', compact('establishment'));
        }else{
            $establishment = null;
            // return view('front.establishment_show', compact('establishment'));
            return redirect()->route('home');
        }
    }
}
