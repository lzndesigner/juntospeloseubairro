<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Establishment;
use App\Models\Blog;
use App\Models\Page;
use App\Models\Plan;
use App\Models\Calendar;
use App\Support\SearchSupport;
use Route;
use DB;
use Auth;

class HomeController extends Controller
{

    public function index()
    {
        $homeestablishments = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method, a.name as estabelecimento, a.slug as estabelecimentoslug, a.image as image, a.distric, a.city, a.state, c.title as categoria, a.featured, c.slug as categoriaslug, d.name as plano, d.upgrade 
            from establishments a
            inner join customer_plans b on a.id = b.establishment_id 
            inner join categories c on a.category_id = c.id
            inner join plans d on b.plan_id = d.id
            where d.upgrade = 0 and b.plan_status = 'Pago' order by rand()"));

            //inner join (select establishment_id, min(filename) filename from establishment_photos group by establishment_id) ba on a.id = ba.establishment_id
        // dd($homeestablishments);

        $getViews = DB::table('establishments')->select('views')->get();
        $countAllViews = $getViews->sum('views');
        // dd($countAllViews);

        $categoriesHome = DB::table('categories')->where('status', 1)->orderBy('title', 'ASC')->take(10)->get();

        return view('front.home', compact('homeestablishments', 'countAllViews', 'categoriesHome'));
    }
    public function maintence()
    {
        return view('front.maintence');
    }

    public function encontrar()
    {
        return view('front.encontrar');
    }

    public function anunciar()
    {
        $getViews = DB::table('establishments')->select('views')->get();
        $countAllViews = $getViews->sum('views');

        $categoriesHome = DB::table('categories')->where('status', 1)->orderBy('title', 'ASC')->take(10)->get();

        $plans = Plan::where('status', '=', '1')->where('id', '>=', 1)->where('upgrade', '=', 0)->orderBy('price', 'DESC')->paginate();

        return view('front.anunciar', compact('countAllViews', 'categoriesHome', 'plans'));
    }

    public function planos()
    {
        // verificar se o usuario está logado no customer
        // dd(Auth::guard('customer')->check());

        $plans = Plan::where('status', '=', '1')->where('id', '>=', 1)->where('upgrade', '=', 0)->orderBy('price', 'DESC')->paginate();
        $upgrades = Plan::where('status', '=', '1')->where('id', '>=', 1)->where('upgrade', '=', 1)->orderBy('price', 'ASC')->paginate();
        $pageTerms = Page::where('slug', 'termos-de-contrato')->get();


        return view('front.planos', compact('plans'))->with('pageTerms', $pageTerms)->with('upgrades', $upgrades);
    }
    /**
     * @param Request $request
     */

    public function buscadistrito(Request $request)
    {
        $inputDistric = $request->input('sdistric');
        $result = '';
        $tipo = '';

        $distric = DB::table('establishments')
            ->where('status', '1')
            ->select('distric', 'city', 'state')
            ->where('distric', 'LIKE', "%$inputDistric%")
            ->groupBy('distric', 'city', 'state')
            ->take(20)->get();

        $city = DB::table('establishments')
            ->where('status', '1')
            ->select('distric', 'city', 'state')
            ->where('city', 'LIKE', "%$inputDistric%")
            ->groupBy('distric', 'city', 'state')
            ->take(20)->get();

        $state = DB::table('establishments')
            ->where('status', '1')
            ->select('distric', 'city', 'state')
            ->where('state', 'LIKE', "%$inputDistric%")
            ->groupBy('distric', 'city', 'state')
            ->take(20)->get();

        if (count($distric) > 0) {
            $result = $distric;
            $tipo = 'distric';
        }

        if (count($city) > 0) {
            $result = $city;
            $tipo = 'city';
        }
        if (count($state) > 0) {
            $result = $state;
            $tipo = 'state';
        }

        return response()->json(['result' => $result, 'tipo' => $tipo]);
    }

    public function busca(Request $request)
    {
        $all = $request->input('all');
        $distric = $request->input('sdistric');
        $description = $request->input('description');
        $dateNow = \Carbon\Carbon::now()->format('Y-m-d');
        $filtros = null;
       
        if ($description) {
            $filtros = 'a.description like %'. $all .'% and';
        }

        if ($distric && $all) {
            $keysearch = $all . ' e ' . $distric;

            // DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method, a.name as estabelecimento, a.slug as estabelecimentoslug, a.image as image, a.distric, a.city, a.state, c.title as categoria, a.featured, c.slug as categoriaslug, d.name as plano, d.upgrade 
            // from establishments a
            // inner join customer_plans b on a.id = b.establishment_id 
            // inner join categories c on a.category_id = c.id
            // inner join plans d on b.plan_id = d.id
            // where d.upgrade = 0 and b.plan_status = 'Pago' order by rand()"));
            
        $searchestablishments = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method,  a.name as estabelecimento, a.image as image, a.slug as estabelecimentoslug, a.distric, a.city, a.state, a.featured, c.title as categoria, c.slug as categoriaslug, d.name as plano, d.upgrade
            from establishments a
            inner join customer_plans b on a.id = b.establishment_id 
            inner join categories c on a.category_id = c.id
            inner join plans d on b.plan_id = d.id
            where (d.upgrade = 0 and b.plan_status = 'Pago' and b.plan_dateend >= '$dateNow') and (a.name like '%$all%' or c.title like '%$all%') and (a.distric like '%$distric%' or a.city like '%$distric%' or a.state like '%$distric%')
            "));
            // dd($searchestablishments);
        } else if ($distric) {
            $keysearch = $distric;
            $searchestablishments = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method,  a.name as estabelecimento, a.image as image,  a.slug as estabelecimentoslug, a.distric, a.city, a.state, a.featured, c.title as categoria, c.slug as categoriaslug, d.name as plano, d.upgrade
            from establishments a
            inner join customer_plans b on a.id = b.establishment_id 
            inner join categories c on a.category_id = c.id
            inner join plans d on b.plan_id = d.id
            where (d.upgrade = 0 and b.plan_status = 'Pago' and a.featured = 0 and b.plan_dateend >= '$dateNow') and (a.distric like '%$distric%' or a.city like '%$distric%' or a.state like '%$distric%')
            "));
            // dd($searchestablishments);
        } else if ($all) {
            $keysearch = $all;
            if($description){
                $searchestablishments = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method, a.name as estabelecimento, a.image as image, a.slug as estabelecimentoslug, a.distric, a.city, a.state, a.featured, a.description, c.title as categoria, c.slug as categoriaslug, d.name as plano, d.upgrade
                from establishments a
                inner join customer_plans b on a.id = b.establishment_id 
                inner join categories c on a.category_id = c.id
                inner join plans d on b.plan_id = d.id
                where (d.upgrade = 0 and b.plan_status = 'Pago' and b.plan_dateend >= '$dateNow') and a.description like '%$all%' or a.name like '%$all%' or c.title like '%$all%' 
                "));
            }else{
                $searchestablishments = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method, a.name as estabelecimento, a.image as image, a.slug as estabelecimentoslug, a.distric, a.city, a.state, a.featured, a.description, c.title as categoria, c.slug as categoriaslug, d.name as plano, d.upgrade
                from establishments a
                inner join customer_plans b on a.id = b.establishment_id 
                inner join categories c on a.category_id = c.id
                inner join plans d on b.plan_id = d.id
                where (d.upgrade = 0 and b.plan_status = 'Pago' and b.plan_dateend >= '$dateNow') and a.name like '%$all%' or c.title like '%$all%'
                "));
            }
            // dd($searchestablishments);
        } else {
            return redirect()->route('home');
        }

        if ($distric) {
            $featuredestablishments = DB::select(DB::raw("select b.id as id, b.customer_id, b.establishment_id, b.plan_id, b.plan_status as status, b.plan_datebegin, b.plan_dateend, b.plan_method, a.name as estabelecimento, a.slug as estabelecimentoslug, a.image as image, a.distric, a.city, a.state, a.featured, a.featured, c.title as categoria, c.slug as categoriaslug, d.name as plano, d.upgrade
            from establishments a
            inner join customer_plans b on a.id = b.establishment_id 
            inner join categories c on a.category_id = c.id
            inner join plans d on b.plan_id = d.id
            where d.upgrade = 0 and b.plan_status = 'Pago' and b.plan_dateend >= '$dateNow' and a.distric like '%$distric%' and a.featured = 1"));
            return view('front.resultado-de-busca', compact('searchestablishments'))->with('keysearch', $keysearch)->with('featuredestablishments', $featuredestablishments);
        } else {
            return view('front.resultado-de-busca', compact('searchestablishments'))->with('keysearch', $keysearch);
        }
    }

    public function pages(Page $page)
    {
        $page = Page::where('id', $page->id)->first();;

        //$establishment->increment('views');
        return view('front.pages', compact('page'));
    }
}
