<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Excel;
use App\Exports\Customers;
use App\Exports\CustomersEstablishments;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $order = $request->input('order') == 'asc' ? 'desc' : 'asc';
        $column_name = null;
        if($request->input('column')){
            $column = $request->input('column');
        }else{
            $column = "plan_dateend";
        }

        $proxvencimentos = \DB::table('customer_plans as a')
            ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'a.renews', 'b.id as estabelecimentoid', 'b.name as estabelecimento', 'b.slug as estabelecimentoslug', 'b.phone as phone', 'b.phone_whatsapp as iswhatsapp', 'c.title as categoria', 'd.name as plano', 'd.price as price', 'e.id as clienteid', 'e.name as cliente')
            ->join('establishments as b', 'a.establishment_id', 'b.id')
            ->join('categories as c', 'b.category_id', 'c.id')
            ->leftjoin('plans as d', 'a.plan_id', 'd.id')
            ->join('customers as e', 'a.customer_id', 'e.id')
            ->orderBy($column, $order)
            ->paginate();

            // dd($request->input('sort'));

        $totalvencimentos = \DB::table('customer_plans as a')
        ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'a.renews', 'b.id as estabelecimentoid', 'b.name as estabelecimento', 'b.slug as estabelecimentoslug', 'b.phone as phone', 'b.phone_whatsapp as iswhatsapp', 'c.title as categoria', 'd.name as plano', 'd.price as price', 'e.id as clienteid', 'e.name as cliente')
        ->join('establishments as b', 'a.establishment_id', 'b.id')
        ->join('categories as c', 'b.category_id', 'c.id')
        ->leftjoin('plans as d', 'a.plan_id', 'd.id')
        ->join('customers as e', 'a.customer_id', 'e.id')
        ->count();

        $totalCustomers = \DB::table('customers')->where('status', 1)->count();
        $totalCustomerPlans = \DB::table('customer_plans')->where('plan_status', 'Pago')->count();
        $totalEstablishments = \DB::table('establishments')->where('status', 1)->count();
        
        return view('admin.dashboard', compact('proxvencimentos'))->with('totalvencimentos', $totalvencimentos)
        ->with('totalCustomers', $totalCustomers)
        ->with('totalCustomerPlans', $totalCustomerPlans)
        ->with('totalEstablishments', $totalEstablishments)
        ->with('order', $order);
    }
    
    
    public function exportCustomers()
    {
        $dateNow = \Carbon\Carbon::now()->format('d-m-Y_H-i-s');
        return Excel::download(new Customers, 'clientes_sem_estabelecimentos_'.$dateNow.'.xlsx');
    }
    
    public function exportCustomersEstablishments()
    {
        $dateNow = \Carbon\Carbon::now()->format('d-m-Y_H-i-s');
        return Excel::download(new CustomersEstablishments, 'clientes_com_estabelecimentos_'.$dateNow.'.xlsx');
    }
}
