<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Customers\StoreRequest;
use App\Http\Requests\Admin\Customers\UpdateRequest;

use App\Models\Customer;
use App\Models\Transation;

use DB;
use Carbon\Carbon;

class CustomersController extends Controller
{
  /**
   * Armazena uma nova instancia do model Customer
   *
   * @var \App\Customer
   */
  private $customers;

  /**
   * Metodo construtor.
   */
  public function __construct()
  {
    $this->customers = app(Customer::class);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $order = $request->input('order') == 'asc' ? 'desc' : 'asc';
    $column_name = null;
    if($request->input('column')){
        $column = $request->input('column');
    }else{
        $column = "name";
    }

    $customers = \DB::table("customers")
    ->select('customers.id as id', 'customers.name', 'customers.email', 'customers.phone',
    'customers.status as status', 'customers.created_at', 'customers.updated_at', 
    DB::raw("COUNT(establishments.customer_id) as qtaEsta"))
    ->leftjoin("establishments", "establishments.customer_id", "customers.id")
    ->groupBy("customers.id")
    ->orderBy($column, $order)
    ->paginate();

    // dd($customers);
    
    return view('admin.customers.index', compact('customers'))->with('order', $order);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.customers.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // validate the data
    $this->validate($request, [
        'name'          => 'required',
        'email'         => 'required|unique:customers',
        'password'      => 'required',
        'document'      => 'required|unique:customers'
    ]);

    // store in the database
    $customers = new Customer;
    $customers->name = $request->name;
    $customers->email = $request->email;
    $customers->password = bcrypt($request->password);
    $customers->document = $request->document;
    $customers->status = 1;
    $customers->plan_discount =  '0';
    $customers->plan_method =  'banktransfer';

    $customers->save();
    session()->flash('messages.success', ['Usuário cadastrado com sucesso, faça seu login.']);
    return redirect()->route('customers.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $customer = Customer::findOrFail($id);
    $customerPlans = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'b.name as estabelecimento', 'c.title as categoria', 'd.name as plano', 'd.price')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->where('a.customer_id', $id)
      ->paginate(10);

    $customerTransations = \DB::table('transations as a')
      ->where('a.customer_id', $id)
      ->paginate(10);

    // dd($customerTransations);

    return view('admin.customers.show', compact('customer'))->with('customerPlans', $customerPlans)->with('customerTransations', $customerTransations);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $customer = Customer::findOrFail($id);
    $customerPlans = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'a.renews', 'b.id as estabelecimentoid',  'b.name as estabelecimento', 'c.title as categoria', 'd.name as plano', 'd.price')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->where('a.customer_id', $id)
      ->orderby('a.plan_dateend', 'DESC')
      ->paginate(10);

    $customerTransations = \DB::table('transations as a')
      ->where('a.customer_id', $id)
      ->paginate(10);

    // dd($customerTransations);
    return view('admin.customers.edit', compact('customer'))->with('customerPlans', $customerPlans)->with('customerTransations', $customerTransations);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $customer = $this->customers->where('id', $id)->first();

    if (!$customer) {
      session()->flash('messages.error', ['Cliente não existe!']);
      return redirect()->route('customers.index');
    }
       // validate the data
       $this->validate($request, [
        'name'          => 'required',
        'email'         => "required|unique:customers,email,$id,id",
        'password'      => 'nullable|min:5',
        'document'      => "required|unique:customers,document,$id,id"
    ]);
    if($request->password){
        $password = bcrypt($request->password);
        $customer->password = $password;
    }
    $customer->name = $request->name;
    $customer->email = $request->email;
    $customer->status = $request->status;
    $customer->phone = $request->phone;
    $customer->document = $request->document;
    $customer->plan_method = $request->plan_method;
    $customer->plan_discount = $request->plan_discount;
    $customer->save();

  session()->flash('messages.success', ['Usuário alterado com sucesso!']);
  return redirect()->route('customers.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  Customer $customer
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, Customer $customer)
  {
    $db = app('db');

    $db->beginTransaction();

    try {
      $id = $customer->id;
      
      \DB::table('establishments')->where('customer_id', $id)->delete();
      \DB::table('customer_plans')->where('customer_id', $id)->delete();
      
      $customer->delete();
      $db->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      $db->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }


  public function setstatus(Request $request)
  {
    $customerplan_id = $request->input('customerplan_id');
    $type_status = $request->input('type_status');


    $cp = DB::table('customer_plans as a')
      ->leftjoin('plans as b', 'a.plan_id', 'b.id')
      ->leftjoin('customers as c', 'a.customer_id', 'c.id')
      ->select('a.id as id', 'a.customer_id as customer_id', 'a.plan_status', 'a.plan_dateend', 'a.plan_method as plan_method', 'b.name as plan_name', 'b.price as plan_price', 'b.period as plan_period', 'c.name as customer_name')
      ->where('a.id', $customerplan_id)->first();

      




    if ($type_status === 'Pago') {
            // store in the database
            $transations = new Transation;
            $transations->customer = $cp->customer_name;
            $transations->customer_id = $cp->customer_id;
            $transations->plan_name = $cp->plan_name;
            $transations->plan_period = $cp->plan_period;
            $transations->plan_method = $cp->plan_method;
            $transations->plan_value = $cp->plan_price;
            $transations->date =  Carbon::now()->format('Y-m-d H:i:s');
            $transations->save();
            
      if ($cp->plan_dateend < Carbon::now()->format('Y-m-d')) {
        $dateend = Carbon::now()->addDays($cp->plan_period)->format('Y-m-d');
        DB::table('customer_plans')->where('id', $customerplan_id)->update([
          'plan_status'           => $type_status,
          'plan_dateend'          => $dateend,
          'updated_at'            => Carbon::now()
        ]);
      } else {
        $dateend = Carbon::parse($cp->plan_dateend)->addDays($cp->plan_period)->format('Y-m-d');
        DB::table('customer_plans')->where('id', $customerplan_id)->update([
          'plan_status'           => $type_status,
          'plan_dateend'          => $dateend,
          'updated_at'            => Carbon::now()
        ]);
      }
    } else if ($type_status === 'Pendente' || $type_status === 'Aguardando'  || $type_status === 'Expirado' ) {
      $dateend = Carbon::now()->format('Y-m-d');
      DB::table('customer_plans')->where('id', $customerplan_id)->update([
        'plan_status'           => $type_status,
        'plan_dateend'          => $dateend,
        'updated_at'            => Carbon::now()
      ]);
    } else {
      $dateend = null;
      DB::table('customer_plans')->where('id', $customerplan_id)->update([
        'plan_status'           => $type_status,
        'updated_at'            => Carbon::now()
      ]);
    }

    $dateformat = Carbon::parse($dateend)->format('d/m/y');
    return response()->json(['customerplan_id' => $customerplan_id, 'type_status' => $type_status, 'dateformat' => $dateformat]);
  }
}
