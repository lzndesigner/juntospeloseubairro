<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Apoios\StoreRequest;
use App\Http\Requests\Admin\Apoios\UpdateRequest;

use App\Models\Apoio;

use Image;

class ApoiosController extends Controller
{
  /**
   * Armazena uma nova instancia do model apoio
   *
   * @var \App\apoio
   */
  private $apoios;

  /**
   * Metodo construtor.
   */
  public function __construct()
  {
    $this->apoios = app(Apoio::class);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $apoios = Apoio::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
    return view('admin.apoios.index', compact('apoios'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.apoios.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreRequest $request)
  {
    $apoio = new Apoio;

    $foto_antiga = $apoio->image;

    if (Storage::has($foto_antiga)) {
      Storage::delete($foto_antiga);
    }

    $filename = $request->file('image')->getClientOriginalName();

    $retirar = [" ", "(", ")", "-"];

    $filenameReplace = str_replace($retirar, '_', $filename);

    $filenameReplace = rand() . $filenameReplace;

    $path = 'apoios/';
    $image_thumb = Image::make($request->file('image'))->resize(500, 500)->stream();

    $uploaded = Storage::disk('public')->put($path . $filenameReplace, $image_thumb->__toString());

    $apoio->image = $path . $filenameReplace;


    $apoio->title = $request->title;
    $apoio->url = $request->url;
    $apoio->status = $request->status;

    if (!$apoio->save()) {
      session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
      return redirect()->route('apoios.index');
    }

    session()->flash('messages.success', ['Apoio cadastrado com sucesso!']);
    return redirect()->route('apoios.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $apoio = Apoio::findOrFail($id);
    return view('admin.apoios.show', compact('apoio'));
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $apoio = Apoio::findOrFail($id);
    return view('admin.apoios.edit', compact('apoio'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(StoreRequest $request, $id)
  {
    $apoio = $this->apoios->where('id', $id)->first();

    if (!$apoio) {
      session()->flash('messages.error', ['Apoio não existe!']);
      return redirect()->route('apoios.index');
    }

    if (!Input::hasFile('image') == null) {
      $foto_antiga = $apoio->image;

      if (Storage::has($foto_antiga)) {
        Storage::delete($foto_antiga);
      }

      $filename = $request->file('image')->getClientOriginalName();

      $retirar = [" ", "(", ")", "-"];

      $filenameReplace = str_replace($retirar, '_', $filename);

      $filenameReplace = rand() . $filenameReplace;

      $path = 'apoios/';
      $image_thumb = Image::make($request->file('image'))->resize(500, 500)->stream();

      $uploaded = Storage::disk('public')->put($path . $filenameReplace, $image_thumb->__toString());

      $apoio->image = $path . $filenameReplace;
    }

    $apoio->title = $request->title;
    $apoio->url = $request->url;
    $apoio->status = $request->status;
    $apoio->save();

    session()->flash('messages.success', ['Apoio alterado com sucesso!']);
    return redirect()->route('apoios.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, Apoio $apoio)
  {

    $db = app('db');

    $db->beginTransaction();

    try {
      $id = $apoio->id;

      $apoio->delete();

      $db->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      $db->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }
}
