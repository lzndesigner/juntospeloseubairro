<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Transations\StoreRequest;
use App\Http\Requests\Admin\Transations\UpdateRequest;

use App\Models\Transation;

class TransationsController extends Controller
{
  /**
   * Armazena uma nova instancia do model Transation
   *
   * @var \App\Transation
   */
  private $transations;

  /**
   * Metodo construtor.
   */
  public function __construct()
  {
    $this->transations = app(Transation::class);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $transations = Transation::orderby('date', 'DESC')->paginate(10);
    return view('admin.transations.index', compact('transations'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.transations.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  Transation $transation
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, Transation $transation)
  {
    $db = app('db');

    $db->beginTransaction();

    try {
      $id = $transation->id;

      $transation->delete();

      $db->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      $db->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }
}
