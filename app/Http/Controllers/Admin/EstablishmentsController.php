<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Establishments\StoreRequest;
use App\Http\Requests\Admin\Establishments\UpdateRequest;

use App\Models\Establishment;
use App\Models\EstablishmentPhoto;

use Image;
use App\Support\UploadSupport;
use Auth;
use App\Models\Plan;
use DB;
use Carbon\Carbon;

class EstablishmentsController extends Controller
{
  /**
   * Armazena uma nova instancia do model Establishment
   *
   * @var \App\Establishment
   */
  private $establishments;

  /**
   * Metodo construtor.
   */
  public function __construct()
  {
    $this->establishments = app(Establishment::class);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
        $order = $request->input('order') == 'asc' ? 'desc' : 'asc';
        $column_name = null;
        if($request->input('column')){
            $column = $request->input('column');
        }else{
            $column = "estabelecimento";
        }

        $establishments = \DB::table('establishments as b')
            ->select('b.id as estabelecimentoid', 'b.name as estabelecimento', 'b.slug as estabelecimentoslug', 'b.phone as phone', 'b.phone_whatsapp as iswhatsapp', 'b.cellphone as cellphone', 'b.views as views', 'b.status as status', 'b.created_at as created_at', 'b.updated_at as updated_at', 'c.title as categoria', 'e.id as clienteid', 'e.name as cliente')
            ->join('categories as c', 'b.category_id', 'c.id')
            ->join('customers as e', 'b.customer_id', 'e.id')
            ->orderBy($column, $order)
            ->paginate();

    // $establishments = Establishment::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
    return view('admin.establishments.index', compact('establishments'))->with('order', $order);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.establishments.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreRequest $request)
  {
    try {
      app('db')->beginTransaction(); // inicia uma transaçao

      $data = $request->all();

      if (isset($data['phone_whatsapp'])) {
        $data['phone_whatsapp'] = 1;
      } else {
        $data['phone_whatsapp'] = 0;
      }

      $establishment = Establishment::create($data); // nesse momento o registro ja foi criado.

      foreach ($request->photos as $photo) {
        dd($photo);
        $filename = $photo->store("establishments/{$establishment->id}/photos");
        // associa fotos ao establishment recem criado.
        $establishment->photos()->create(['filename' => $filename]);
      }

      app('db')->commit(); // confirma transaço

      session()->flash('messages.success', ['Estabelecimento cadastrado com sucesso!']);
      return redirect()->route('establishments.index');
    } catch (\Execpetion $error) {
      //
      app('db')->rollback(); // reverte transacao

      session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
      return redirect()->route('establishments.index');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $establishment = Establishment::findOrFail($id);
    return view('admin.establishments.show', compact('establishment'));
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $establishment = Establishment::findOrFail($id);
    return view('admin.establishments.edit', compact('establishment'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(StoreRequest $request, $id)
  {
    // validate the data
    $this->validate($request, [
      'name'         => "required|unique:establishments,name,$id,id",
      'slug'         => "required|unique:establishments,slug,$id,id",
      'image'         => "mimes:jpg,jpeg,png|image",
    ]);

    $establishment = $this->establishments->where('id', $id)->first();

    if (!$establishment) {
      session()->flash('messages.error', ['Establecimento não existe!']);
      return redirect()->route('estabelecimentos.index');
    }

    $data = $request->all();

    if (isset($data['phone_whatsapp'])) {
      $data['phone_whatsapp'] = 1;
    } else {
      $data['phone_whatsapp'] = 0;
    }

    // dd($data);

    // Tratando Image principal
    $image = $request->file('image');

    // dd($image);

    if ($image) {
      $filename = md5($image->getClientOriginalName());
      $extensao = getimagesize($image)['mime'];
      $extensao = str_replace('image/', '', $extensao);
      if ($extensao == "jpeg") {
        $imgExt = strtolower(substr($image->getClientOriginalName(), -5));
      } else {
        $imgExt = strtolower(substr($image->getClientOriginalName(), -4));
      }
      $novaimagem = $filename . time() . $imgExt;



      $img = Image::make($image)->resize(700, 700);
      Storage::put("establishments/{$establishment->id}/photos/$novaimagem", $img->stream());

      $img = Image::make($image)->resize(200, 200);
      Storage::put("establishments/{$establishment->id}/photos/thumb_$novaimagem", $img->stream());

      $data['image'] = $novaimagem;
    }


    $establishment->fill($data); // armazena atributos

    $photos = $request->photos; // arquivos
    if (is_array($photos) && count($photos) > 0) {
      foreach ($photos as $id => $photo) {
        // Verificar se existe foto informada na lista de fotos pertencentes ao establishment.
        if ($establishment->photos->contains('id', $id)) {
          // registro atual
          $row = $establishment->photos->find($id);
          $old = $row->filename;
          $row->filename = $photo->store("establishments/{$establishment->id}/photos");
          $row->save(); // atualiza
          if ($row->isDirty() && app('filesystem')->exists($old)) {
            app('filesystem')->delete($old);
          }
          continue;
        }
      }

      foreach ($request->file('photos') as $photo) {
        $filename = md5($photo->getClientOriginalName());
        $extensao = getimagesize($photo)['mime'];
        $extensao = str_replace('image/', '', $extensao);
        if ($extensao == "jpeg") {
          $imgExt = strtolower(substr($photo->getClientOriginalName(), -5));
        } else {
          $imgExt = strtolower(substr($photo->getClientOriginalName(), -4));
        }
        $novaimagem = $filename . time() . $imgExt;

        $img = Image::make($photo)->resize(700, 700);
        Storage::put("establishments/{$establishment->id}/photos/$novaimagem", $img->stream());

        $img = Image::make($photo)->resize(200, 200);
        Storage::put("establishments/{$establishment->id}/photos/thumb_$novaimagem", $img->stream());

        $establishment->photos()->create(['filename' => $novaimagem]);
      }
    } // endif is array

    $establishment->save(); // guarda alteraçoes

    session()->flash('messages.success', ['Establecimento alterado com sucesso!']);
    return redirect()->route('establishments.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, Establishment $establishment)
  {

    $db = app('db');

    $db->beginTransaction();

    try {
      $id = $establishment->id;

      $establishment->delete();

      \DB::table('customer_plans')->where('establishment_id', $id)->delete();

      // remover diretorio
      app('filesystem')->delete("establishments/{$id}");

      $db->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      $db->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }



  public function photoRemove(Establishment $establishment, $photo_id)
  {
    //
    // verificar se existe relacionamento
    //
    if (!$establishment->photos->contains('id', $photo_id)) abort(403, 'Essa imagem não pertence ao Estabelecimento.');

    $photo = $establishment->photos->find($photo_id);

    app('db')->beginTransaction();

    try {
      $filename = $photo->filename;

      $photo->delete();

      if (app('filesystem')->exists($filename)) {
        // remover do diretorio
        app('filesystem')->delete($filename);
      }

      app('db')->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      app('db')->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }

  public function upload(Establishment $establishment, Request $request, $establishment_id)
  {
    $file = $request->file('file');

    $filename = md5($file->getClientOriginalName());
    $extensao = getimagesize($file)['mime'];
    $extensao = str_replace('image/', '', $extensao);
    if ($extensao == "jpeg") {
      $imgExt = strtolower(substr($file->getClientOriginalName(), -5));
    } else {
      $imgExt = strtolower(substr($file->getClientOriginalName(), -4));
    }
    $novaimagem = $filename . time() . $imgExt;

    $img = Image::make($file)->resize(700, 700);
    Storage::put("establishments/{$establishment_id}/photos/$novaimagem", $img->stream());

    $img = Image::make($file)->resize(200, 200);
    Storage::put("establishments/{$establishment_id}/photos/thumb_$novaimagem", $img->stream());

    $dadosForm['file'] = $novaimagem;

    DB::table('establishment_photos')->insert([ // substitui pelo a model 
      ['filename' => $novaimagem, 'establishment_id' => $establishment_id]
    ]);

    session()->flash('messages.success', ['Miniaturas alteradas com sucesso!']);
    return redirect()->route('establishments.index');
  }
}
