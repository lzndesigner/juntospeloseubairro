<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\CustomerPlans\StoreRequest;
use App\Http\Requests\Admin\CustomerPlans\UpdateRequest;

use App\Models\CustomerPlans;

use Image;
use App\Support\UploadSupport;

class CustomerPlansController extends Controller
{
  /**
   * Armazena uma nova instancia do model Plan
   *
   * @var \App\Plan
   */
  private $customerplans;

  /**
   * Metodo construtor.
   */
  public function __construct()
  {
    $this->customerplans = app(CustomerPlans::class);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $customerplans = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'b.id as estabelecimentoid', 'b.name as estabelecimento', 'b.slug as estabelecimentoslug', 'c.title as categoria', 'd.name as plano', 'd.price as price', 'e.id as clienteid', 'e.name as cliente')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->join('customers as e', 'a.customer_id', 'e.id')
      ->orderBy('plan_dateend', 'ASC')
      ->paginate(10);

    return view('admin.customerplans.index', compact('customerplans'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreRequest $request)
  {
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $customerplans = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'b.id as estabelecimentoid', 'b.name as estabelecimento', 'b.slug as estabelecimentoslug', 'c.title as categoria', 'd.id as planoid', 'd.name as plano', 'd.price as price', 'e.id as clienteid', 'e.name as cliente')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->join('customers as e', 'a.customer_id', 'e.id')
      ->orderBy('plan_dateend', 'ASC')
      ->where('a.id', $id)
      ->first();

      $plans = \DB::table('plans')->where('upgrade', 0)->where('status', 1)->paginate();
      
    return view('admin.customerplans.edit', compact('customerplans'))->with('plans', $plans);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(StoreRequest $request, $id)
  {
    $customerplans = $this->customerplans->where('id', $id)->first();

    if (!$customerplans) {
      session()->flash('messages.error', ['Plano não existe!']);
      return redirect()->route('customerplans.index');
    }

    $data = $request->all();

    $customerplans->fill($data); // armazena atributos

    $customerplans->save(); // guarda alteraçoes

    session()->flash('messages.success', ['Plano alterado com sucesso!']);
    return redirect()->route('customerplans.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, CustomerPlans $customerplans)
  {

    $db = app('db');

    $db->beginTransaction();

    try {
      $id = $customerplans->id;

      $customerplans->delete();

      $db->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      $db->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }
}
