<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\About\StoreRequest;
//use App\Http\Requests\Admin\About\UpdateRequest;

use App\Models\About;

class AboutController extends Controller
{
    /**
     * Armazena uma nova instancia do model About
     *
     * @var \App\About
     */
    private $abouts;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
      $this->abouts = app(About::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $about = About::firstOrFail();
      return view('admin.abouts.index', compact('about'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {

      $about = $this->abouts->firstOrFail();

      if(!$about) {
        session()->flash('messages.error', ['Sobre nós não existe!']);
        return redirect()->route('about.index');
      }

      $about->page_description = $request->page_description;
      $about->save();

      session()->flash('messages.success', ['Sobre nós alterado com sucesso!']);
      return redirect()->route('about.index');

    }





// Desativado
    public function photos()
    {
      $about = \App\Models\About::with('photos')->find(1);
      return view('admin.abouts.photos', compact('about'));
    }

    public function upload()
    {
      $file = \Request::file('documento');
      $aboutid = \Request::get('aboutid');
      $storagePath = $file->store('about');
      $fileName = $storagePath;

      $fileModel = new \App\Models\aboutPhotos();
      $fileModel->name = $fileName;

      $about = \App\Models\About::find($aboutid);
      $about->photos()->save($fileModel);
    }

    public function download($aboutId, $photoId)
    {
      $file = \App\Models\aboutPhotos::find($photoId);
      $storagePath = $file->store('about');
      return \Response::download($storagePath.'/'.$file->name);
      
    }
    public function destroy($aboutId, $photoId)
    {
      $file = \App\Models\aboutPhotos::find($photoId);
      $storagePath = 'storage';
      $file->delete();
      unlink($storagePath.'/'.$file->name);

      session()->flash('messages.success', ['Foto deletada com sucesso!']);
      return redirect()->route('about-photos');
    }


  }