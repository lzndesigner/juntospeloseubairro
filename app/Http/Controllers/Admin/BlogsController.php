<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Blogs\StoreRequest;
use App\Http\Requests\Admin\Blogs\UpdateRequest;

use App\Models\Blog;
use App\Models\BlogPhoto;

use Image;
use App\Support\UploadSupport;

class BlogsController extends Controller
{
  /**
   * Armazena uma nova instancia do model Blog
   *
   * @var \App\Blog
   */
  private $blogs;

  /**
   * Metodo construtor.
   */
  public function __construct()
  {
    $this->blogs = app(Blog::class);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $blogs = Blog::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
    return view('admin.blogs.index', compact('blogs'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.blogs.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreRequest $request)
  {
    try {
      app('db')->beginTransaction(); // inicia uma transaçao

      $blog = Blog::create($request->all()); // nesse momento o registro ja foi criado.

      if ($request->hasFile('photos')) {
        foreach ($request->photos as $photo) {
          // $filename = $photo->store("blogs/{$blog->id}/photos");
          // associa fotos ao blog recem criado.
          // $blog->photos()->create(['filename' => $filename]);
          $support = UploadSupport::make($photo);
          //
          $support->prepare($blog->id)->dispatchNow();

          // dd($support);

          $blog->photos()->create([
            'filename' => $support->getPath(),
            'sizes' => $support->getSizesFilename()
          ]);
        }
      }

      app('db')->commit(); // confirma transaço

      session()->flash('messages.success', ['Postagem cadastrada com sucesso!']);
      return redirect()->route('blogs.index');
    } catch (\Execpetion $error) {
      //
      app('db')->rollback(); // reverte transacao

      session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
      return redirect()->route('blogs.index');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $blog = Blog::findOrFail($id);
    return view('admin.blogs.show', compact('blog'));
  }
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $blog = Blog::findOrFail($id);
    return view('admin.blogs.edit', compact('blog'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UpdateRequest $request, $id)
  {
    $blog = $this->blogs->where('id', $id)->first();

    if (!$blog) {
      session()->flash('messages.error', ['Postagem não existe!']);
      return redirect()->route('blogs.index');
    }

    $blog->fill($request->all()); // armazena atributos

    $photos = $request->photos; // arquivos

    //
    // Caso exista fotos na ediçao
    //
    if (is_array($photos) && count($photos) > 0) {
      foreach ($photos as $id => $photo) {
        //
        // Verificar se existe foto informada na lista de fotos pertencentes ao blog.
        //
        if ($blog->photos->contains('id', $id)) {
          //
          // registro atual
          //
          $row = $blog->photos->find($id);

          $old = $row->filename;

          $support = UploadSupport::make($photo);
          $support->prepare($blog->id)->dispatchNow();
          $row->filename = $support->getPath();
          $row->sizes = $support->getSizesFilename();
          $row->save(); // atualiza

          if ($row->isDirty() && app('filesystem')->exists($old)) {
            app('filesystem')->delete($old);
          }

          continue;
        }
      }
    }

    if ($request->hasFile('addPhotos')) {
      //
      // Adicionar novas fotos
      //
      $photos = $request->addPhotos;

      foreach ($photos as $file) {
        //
        $support = UploadSupport::make($file);
        //
        $support->prepare($blog->id)->dispatchNow();
        //
        $filename = $support->getPath();
        if ($filename) {
          //
          // Adicionar foto ao blog
          //
          $blog->photos()->create([
            'filename' => $filename,
            'sizes' => $support->getSizesFilename()
          ]);

          continue;
        }
      }
    }

    $blog->save(); // guarda alteraçoes

    session()->flash('messages.success', ['Postagem alterada com sucesso!']);
    return redirect()->route('blogs.index');
  }

  public function photoRemove(Blog $blog, $photo_id)
  {
    //
    // verificar se existe relacionamento
    //
    if (!$blog->photos->contains('id', $photo_id)) abort(403, 'Essa imagem não pertence a Postagem.');

    $photo = $blog->photos->find($photo_id);

    app('db')->beginTransaction();

    try {
      $filename = $photo->filename;

      $photo->delete();

      if (app('filesystem')->exists($filename)) {
        // remover do diretorio
        app('filesystem')->delete($filename);
      }

      app('db')->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      app('db')->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, Blog $blog)
  {

    $db = app('db');

    $db->beginTransaction();

    try {
      $id = $blog->id;

      $blog->delete();

      // remover diretorio
      app('filesystem')->delete("blogs/{$id}");

      $db->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      $db->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }
}
