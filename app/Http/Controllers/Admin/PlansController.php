<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Admin\Plans\StoreRequest;
use App\Http\Requests\Admin\Plans\UpdateRequest;

use App\Models\Plan;

use Image;
use App\Support\UploadSupport;

class PlansController extends Controller
{
    /**
     * Armazena uma nova instancia do model Plan
     *
     * @var \App\Plan
     */
    private $plans;

    /**
     * Metodo construtor.
     */
    public function __construct()
    {
      $this->plans = app(Plan::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $plans = Plan::orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();
      return view('admin.plans.index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
     try {
        app('db')->beginTransaction(); // inicia uma transaçao

        $data = $request->all();

        if(isset($data['upgrade'])){
          $data['upgrade'] = 1;
        }else{
          $data['upgrade'] = 0;
        }

        $plan = Plan::create($data); // nesse momento o registro ja foi criado.
        
        app('db')->commit(); // confirma transaço

        session()->flash('messages.success', ['Plano cadastrado com sucesso!']);
        return redirect()->route('plans.index');
      }catch(\Execpetion $error) {
        //
        app('db')->rollback(); // reverte transacao

        session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
        return redirect()->route('plans.index');
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $plan = Plan::findOrFail($id);
      return view('admin.plans.show', compact('plan'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $plan = Plan::findOrFail($id);
      return view('admin.plans.edit', compact('plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
      $plan = $this->plans->where('id', $id)->first();

      if(!$plan) {
        session()->flash('messages.error', ['Plano não existe!']);
        return redirect()->route('plans.index');
      }

      $data = $request->all();

      if(isset($data['upgrade'])){
        $data['upgrade'] = 1;
      }else{
        $data['upgrade'] = 0;
      }

      $plan->fill($data); // armazena atributos

      $plan->save(); // guarda alteraçoes

      session()->flash('messages.success', ['Plano alterado com sucesso!']);
      return redirect()->route('plans.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Plan $plan)
    {

     $db = app('db');

     $db->beginTransaction();

     try {
      $id = $plan->id;

      $plan->delete();

      $db->commit();

      return response(null, 204);
    }catch(\Exception $error) {
     $db->rollback();
     return response()->json(['message' => $error->getMessage()], 400);
   }
 }

}
