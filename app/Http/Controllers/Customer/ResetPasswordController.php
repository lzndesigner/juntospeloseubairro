<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Password;
use Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DB;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Notifications\ResetPassword;
use URL;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;


class ResetPasswordController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

  use SendsPasswordResetEmails;

  /**
   * Where to redirect users after resetting their password.
   *
   * @var string
   */
  protected $redirectTo = '/minha-conta';

  public function __construct()
  {
    $this->middleware('customer');
  }
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  protected function guard()
  {
    return Auth::guard('customer');
  }

  public function index()
  {
    // mostra o form para preencher o e-mail
    return view('front.customers.passwords.email');
  }

  protected function broker()
  {
    return Password::broker('customers');
  }

  public function showLinkRequestForm()
  {
    return view('front.customers.passwords.email'); // aponta ela no route pra ca
  }



  public function showResetForm(Request $request, $token = null)
  {
    // dd($token);    
    return view('front.customers.passwords.reset')->with(
      ['token' => $token, 'email' => $request->email]
    );
  }

  public function sendResetLinkEmail(Request $request)
  {
    $this->validateEmail($request);

    // We will send the password reset link to this user. Once we have attempted
    // to send the link, we will examine the response then see the message we
    // need to show to the user. Finally, we'll send out a proper response.
    $response = $this->broker()->sendResetLink(
      $request->only('email')
    );
    session()->flash('messages.success', ['E-mail de recuperação encaminhado com sucesso. Acesse seu e-mail e clique no link para definir uma nova senha.']);
    return $response == Password::RESET_LINK_SENT
      ? $this->sendResetLinkResponse($response)
      : $this->sendResetLinkFailedResponse($request, $response);
  }


  public function showPasswordResetForm($token)
  {
    $tokenData = DB::table('customers_password_resets')
      ->where('token', $token)->first();

    if (!$tokenData) return redirect()->to('/home');
    return view('front.customers.passwords.reset');
  }

  public function sendPasswordResetNotification($token)
  {
    $this->notify(new ResetPassword($token));
  }




  public function saveNewPassword(Request $request)
  {
        // validate the data
        $this->validate($request, [
          'password'         => "required|min:5",
        ]);

    $credentials =  $request->only('email', 'token');

    if (is_null($user = $this->broker()->getUser($credentials))) {
      return trans(Password::INVALID_USER);
    }

    if (!$this->broker()->tokenExists($user, $credentials['token'])) {
      //return trans(Password::INVALID_TOKEN);

      session()->flash('messages.error', ['E-mail incorreto, ou senha não atende os requisitos mínimos.']);
      return redirect()->back()->withInput($credentials);
    }

    $customer = Customer::where('email', $request->email)->first();
    $customer->password = Hash::make($request->password);
    $customer->setRememberToken(Str::random(60));
    $customer->save();

    event(new PasswordReset($customer));

    $this->guard('auth:customer')->login($customer);
    return redirect()->route('customer.auth.login');
  }
}
