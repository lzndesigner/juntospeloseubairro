<?php

namespace App\Http\Controllers\Customer;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Customer\Establishments\StoreRequest;
use App\Http\Requests\Customer\Establishments\UpdateRequest;

use App\Models\Establishment;
use App\Models\EstablishmentPhoto;

use Image;
use App\Support\UploadSupport;
use Auth;
use App\Models\Plan;
use DB;
use Carbon\Carbon;

class EstablishmentController extends Controller
{
  /**
   * Armazena uma nova instancia do model Establishment
   *
   * @var \App\Establishment
   */
  private $establishments;

  /**
   * Metodo construtor.
   */
  public function __construct()
  {
    $this->middleware('auth:customer');
    $this->establishments = app(Establishment::class);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $customer_id = Auth::user()->id;

    $establishments = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'b.name as estabelecimento', 'b.slug as esabelecimenoslug', 'c.title as categoria', 'c.slug as categoriaslug', 'd.name as plano', 'd.upgrade')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->where('a.customer_id', $customer_id)
      //->where('d.upgrade', 0) 
      // ->whereraw('a.plan_id = null')
      ->where('d.upgrade', 0)
      // ->where('a.plan_id', null)
      ->paginate(10);

    $establishmentsNotplan = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'b.name as estabelecimento', 'b.slug as esabelecimenoslug', 'c.title as categoria', 'c.slug as categoriaslug', 'd.name as plano', 'd.upgrade')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->where('a.customer_id', $customer_id)
      //->where('d.upgrade', 0) 
      // ->whereraw('a.plan_id = null')
      ->where('a.plan_id', null)
      ->paginate(10);

    // Exibe os Planos de upgrades disponíveis
    $upgrades = Plan::where('status', '=', '1')->where('id', '>=', 1)->where('upgrade', '=', 1)->orderBy('price', 'ASC')->paginate();
    return view('front.customers.establishments.index', compact('establishments'))->with('upgrades', $upgrades)->with('establishmentsNotplan', $establishmentsNotplan);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $customer_id = Auth::user()->id;

    $establishment_upgrades = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'b.name as estabelecimento', 'b.slug as esabelecimenoslug', 'c.title as categoria', 'c.slug as categoriaslug', 'd.name as plano', 'd.upgrade')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->where('a.customer_id', $customer_id)
      ->where('d.upgrade', 1)
      ->where('a.establishment_id', $id)
      ->get();

    $establishment = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'e.name as cliente', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'b.name as estabelecimento', 'b.slug as esabelecimenoslug', 'c.title as categoria', 'c.slug as categoriaslug', 'd.name as plano', 'd.upgrade')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->join('customers as e', 'a.customer_id', 'e.id')
      ->where('a.customer_id', $customer_id)
      ->where('d.upgrade', 0)
      ->where('a.establishment_id', $id)
      ->get();

    return response()->json(['upgrades' => $establishment_upgrades, 'geral' => $establishment], 200);
  }


  public function statistics(Request $request)
  {
    $customer_id = Auth::user()->id;

    //$establishments = Establishment::where('customer_id', $customer_id)->orderBy($request->input('sort', 'created_at'), 'ASC')->paginate();        
    $establishments = \DB::table('customer_plans as a')
      ->select('a.id as id', 'a.customer_id', 'a.establishment_id', 'a.plan_id', 'a.plan_status as status', 'a.plan_datebegin', 'a.plan_dateend', 'a.plan_method', 'b.name as estabelecimento', 'b.slug as esabelecimenoslug', 'b.views as views', 'c.title as categoria', 'c.slug as categoriaslug', 'd.name as plano', 'd.upgrade', 'd.statistics')
      ->join('establishments as b', 'a.establishment_id', 'b.id')
      ->join('categories as c', 'b.category_id', 'c.id')
      ->leftjoin('plans as d', 'a.plan_id', 'd.id')
      ->where('a.customer_id', $customer_id)
      ->where('d.upgrade', 0)
      ->where('d.statistics', 1)
      ->orderBy('b.views', 'DESC')
      ->paginate(10);

    // dd($establishments);
    return view('front.customers.establishments.statistics', compact('establishments'));
  }


  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('front.customers.establishments.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreRequest $request)
  {
    // dd($request->all());
    // validate the data
    $this->validate($request, [
      'slug'         => 'required|unique:establishments'
    ]);

    try {
      app('db')->beginTransaction(); // inicia uma transaçao

      $data = $request->all();

      if (isset($data['phone_whatsapp'])) {
        $data['phone_whatsapp'] = 1;
      } else {
        $data['phone_whatsapp'] = 0;
      }

      $establishment = Establishment::create($data); // nesse momento o registro ja foi criado.

      // Tratando Image principal
      $image = $request->file('image');
      // dd($image);
      if ($image) {
        $filename = md5($image->getClientOriginalName());
        $extensao = getimagesize($image)['mime'];
        $extensao = str_replace('image/', '', $extensao);
        if ($extensao == "jpeg") {
          $imgExt = strtolower(substr($image->getClientOriginalName(), -5));
        } else {
          $imgExt = strtolower(substr($image->getClientOriginalName(), -4));
        }
        $novaimagem = $filename . time() . $imgExt;

        $img = Image::make($image)->resize(700, 700);
        Storage::put("establishments/{$establishment->id}/photos/$novaimagem", $img->stream());

        $img = Image::make($image)->resize(200, 200);
        Storage::put("establishments/{$establishment->id}/photos/thumb_$novaimagem", $img->stream());

        $establishment['image'] = $novaimagem;
      }

      // Tratando Imagem Miniaturas
      $miniaturas = $request->file('photos');
      if ($miniaturas) {
        foreach ($miniaturas as $miniatura) {
          $filename = md5($miniatura->getClientOriginalName());
          $extensao = getimagesize($miniatura)['mime'];
          $extensao = str_replace('image/', '', $extensao);
          if ($extensao == "jpeg") {
            $imgExt = strtolower(substr($miniatura->getClientOriginalName(), -5));
          } else {
            $imgExt = strtolower(substr($miniatura->getClientOriginalName(), -4));
          }
          $novaimagem = $filename . time() . $imgExt;

          $img = Image::make($miniatura)->resize(700, 700);
          Storage::put("establishments/{$establishment->id}/photos/$novaimagem", $img->stream());

          $img = Image::make($miniatura)->resize(200, 200);
          Storage::put("establishments/{$establishment->id}/photos/thumb_$novaimagem", $img->stream());


          $dadosForm['file'] = $novaimagem;
          // dd($novaimagem);

          $establishment->photos()->create(['filename' => $novaimagem]);
        }
      }

      // dd($establishment);
      $establishment->save(); // guarda alteraçoes

      $plan_GetId = DB::table('customer_plans')->insertGetId([
        'customer_id'           => Auth::user()->id,
        'establishment_id'      => $establishment->id,
        'plan_id'               => Null,
        'plan_status'           => 'Pendente',
        'plan_datebegin'        => Null,
        'plan_dateend'          => Null,
        'plan_method'           => '',
        'renews'                => '0',
        'created_at'            =>  Carbon::now(),
        'updated_at'            =>  Carbon::now()
      ]);


      app('db')->commit(); // confirma transaço

      session()->flash('messages.success', ['Estabelecimento cadastrado com sucesso!']);
      return redirect()->route('customer.plan', $establishment->id);
    } catch (\Execpetion $error) {
      //
      app('db')->rollback(); // reverte transacao

      session()->flash('messages.error', ['Houve um erro. Tente novamente!']);
      return redirect()->route('estabelecimentos.index');
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $idUserOn = Auth::user()->id;
    $establishment = Establishment::findOrFail($id);
    $idCustomerEsta = $establishment->customer_id;

    $rulePlan = \DB::select(DB::raw("select cplans.id as id, cplans.customer_id, cplans.establishment_id, cplans.plan_id, cplans.plan_status as status, cplans.plan_datebegin, cplans.plan_dateend, cplans.plan_method, estab.name as estabelecimento, estab.slug as estabelecimentoslug, estab.image as image, estab.distric, estab.city, estab.state, estab.featured, estab.featured
            from establishments estab
            inner join customer_plans cplans on estab.id = cplans.establishment_id 
            where (cplans.plan_id = 3 or cplans.plan_id = 4) and (estab.id = " . $id . " and cplans.establishment_id = " . $id . ")"));

    $ruleplan = ($rulePlan ? '1' : '0');
    // dd($ruleplan);

    if ($idUserOn === $idCustomerEsta) {
      return view('front.customers.establishments.edit', compact('establishment'))->with('ruleplan', $ruleplan);
    } else {
      session()->flash('messages.error', ['Você não tem permissão para editar esse estabelecimento.']);
      return redirect()->route('estabelecimentos.index');
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(StoreRequest $request, $id)
  {
    // validate the data
    $this->validate($request, [
      'name'         => "required|unique:establishments,name,$id,id",
      'slug'         => "required|unique:establishments,slug,$id,id",
      'image'         => "mimes:jpg,jpeg,png|image",
    ]);

    $establishment = $this->establishments->where('id', $id)->first();

    if (!$establishment) {
      session()->flash('messages.error', ['Establecimento não existe!']);
      return redirect()->route('estabelecimentos.index');
    }

    $data = $request->all();

    if (isset($data['phone_whatsapp'])) {
      $data['phone_whatsapp'] = 1;
    } else {
      $data['phone_whatsapp'] = 0;
    }

    // dd($data);

    // Tratando Image principal
    $image = $request->file('image');

    // dd($image);

    if ($image) {
      $filename = md5($image->getClientOriginalName());
      $extensao = getimagesize($image)['mime'];
      $extensao = str_replace('image/', '', $extensao);
      if ($extensao == "jpeg") {
        $imgExt = strtolower(substr($image->getClientOriginalName(), -5));
      } else {
        $imgExt = strtolower(substr($image->getClientOriginalName(), -4));
      }
      $novaimagem = $filename . time() . $imgExt;



      $img = Image::make($image)->resize(700, 700);
      Storage::put("establishments/{$establishment->id}/photos/$novaimagem", $img->stream());

      $img = Image::make($image)->resize(200, 200);
      Storage::put("establishments/{$establishment->id}/photos/thumb_$novaimagem", $img->stream());

      $data['image'] = $novaimagem;
    }


    $establishment->fill($data); // armazena atributos

    $photos = $request->photos; // arquivos
    if (is_array($photos) && count($photos) > 0) {
      foreach ($photos as $id => $photo) {
        // Verificar se existe foto informada na lista de fotos pertencentes ao establishment.
        if ($establishment->photos->contains('id', $id)) {
          // registro atual
          $row = $establishment->photos->find($id);
          $old = $row->filename;
          $row->filename = $photo->store("establishments/{$establishment->id}/photos");
          $row->save(); // atualiza
          if ($row->isDirty() && app('filesystem')->exists($old)) {
            app('filesystem')->delete($old);
          }
          continue;
        }
      }

      foreach ($request->file('photos') as $photo) {
        $filename = md5($photo->getClientOriginalName());
        $extensao = getimagesize($photo)['mime'];
        $extensao = str_replace('image/', '', $extensao);
        if ($extensao == "jpeg") {
          $imgExt = strtolower(substr($photo->getClientOriginalName(), -5));
        } else {
          $imgExt = strtolower(substr($photo->getClientOriginalName(), -4));
        }
        $novaimagem = $filename . time() . $imgExt;

        $img = Image::make($photo)->resize(700, 700);
        Storage::put("establishments/{$establishment->id}/photos/$novaimagem", $img->stream());

        $img = Image::make($photo)->resize(200, 200);
        Storage::put("establishments/{$establishment->id}/photos/thumb_$novaimagem", $img->stream());

        $establishment->photos()->create(['filename' => $novaimagem]);
      }
    } // endif is array

    $establishment->save(); // guarda alteraçoes

    session()->flash('messages.success', ['Establecimento alterado com sucesso!']);
    return redirect()->route('estabelecimentos.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, Establishment $establishment, $id)
  {
    try {
      Establishment::findOrFail($id)->delete();
      return response(null, 204);
    } catch (\Exception $error) {
      $db->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }

  public function photoRemove(Establishment $establishment, $photo_id)
  {
    // dd('teste');
    //
    // verificar se existe relacionamento
    //
    if (!$establishment->photos->contains('id', $photo_id)) abort(403, 'Essa imagem não pertence ao Estabelecimento.');

    $photo = $establishment->photos->find($photo_id);

    app('db')->beginTransaction();

    try {
      $filename = $photo->filename;

      $photo->delete();

      if (app('filesystem')->exists($filename)) {
        // remover do diretorio
        app('filesystem')->delete($filename);
      }

      app('db')->commit();

      return response(null, 204);
    } catch (\Exception $error) {
      app('db')->rollback();
      return response()->json(['message' => $error->getMessage()], 400);
    }
  }

  public function upload(Establishment $establishment, Request $request, $establishment_id)
  {
    $file = $request->file('file');

    $filename = md5($file->getClientOriginalName());
    $extensao = getimagesize($file)['mime'];
    $extensao = str_replace('image/', '', $extensao);
    if ($extensao == "jpeg") {
      $imgExt = strtolower(substr($file->getClientOriginalName(), -5));
    } else {
      $imgExt = strtolower(substr($file->getClientOriginalName(), -4));
    }
    $novaimagem = $filename . time() . $imgExt;

    $img = Image::make($file)->resize(700, 700);
    Storage::put("establishments/{$establishment_id}/photos/$novaimagem", $img->stream());

    $img = Image::make($file)->resize(200, 200);
    Storage::put("establishments/{$establishment_id}/photos/thumb_$novaimagem", $img->stream());

    $dadosForm['file'] = $novaimagem;

    DB::table('establishment_photos')->insert([ // substitui pelo a model 
      ['filename' => $novaimagem, 'establishment_id' => $establishment_id]
    ]);

    session()->flash('messages.success', ['Miniaturas alteradas com sucesso!']);
    return redirect()->route('estabelecimentos.index');
  }
}
