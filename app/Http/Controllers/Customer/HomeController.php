<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Plan;
use App\Models\Page;
use App\Models\Transation;
use App\Models\Establishment;
use Auth;
use View;
use Carbon\Carbon;
use DB;
use Validator;
use Exception;
use MP;

class HomeController extends Controller
{
    /**
     * Armazena uma nova instancia do model Customer
     *
     * @var \App\Customer
     */
    private $customers;

    protected $redirectTo = '/minha-conta';

    public function __construct()
    {
        $this->middleware('auth:customer')->only(['index', 'show', 'edit', 'plan', 'planpayment', 'upgrades', 'upgradespayment', 'planrenew', 'planrenewpayment', 'plansuccess', 'planfree', 'transation']);
        $this->customers = app(Customer::class);
        $this->transations = app(Transation::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer_id = Auth::id();
        $myestablishments = Establishment::where('customer_id', $customer_id)->get();

        $checkValidateEstabs = DB::table('customer_plans as a')
        ->select('a.id as idCustomerPlans', 'a.customer_id', 'a.establishment_id', 'a.plan_dateend', 'b.id', 'b.name')
        ->join('establishments as b', 'a.establishment_id', 'b.id')
        ->where('a.customer_id', $customer_id)
        ->where('a.plan_status', 'Pago')
        ->get();

        $dateNow = \Carbon\Carbon::now()->format('Y-m-d');

        $checkValidates = [];
        foreach ($checkValidateEstabs as $checkValidateEstab) {
            $dateWarning = \Carbon\Carbon::parse($checkValidateEstab->plan_dateend)->subDay('7')->format('Y-m-d');
            if (strtotime($dateWarning) <= strtotime($dateNow)) {
                $checkValidates[] = $checkValidateEstab;
            }
        }

        return view('front.customers.dashboard', compact('myestablishments', 'checkValidates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front.customers.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'name'          => 'required',
            'email'         => 'required|unique:customers',
            'password'      => 'required|min:5',
            'document'      => 'required|unique:customers',
            'phone'         => 'required'
        ]);

        // store in the database
        $customers = new Customer;
        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->password = bcrypt($request->password);
        $customers->document = $request->document;
        $customers->phone = $request->phone;
        $customers->status = 1;
        $customers->plan_discount =  '0';
        $customers->plan_method =  'banktransfer';

        $customers->save();

        Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password]);

        session()->flash('messages.success', ['Usuário cadastrado com sucesso, seja bem vindo. <br><br> Agora vamos preencher as informações do seu estabelecimento.']);
        return redirect()->route('estabelecimentos.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $customer = Customer::findOrFail($id);
        // return view('front.customers.view', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::id() === (int) $id) {
            $customer = Customer::findOrFail($id);
            $myestablishments = Establishment::where('customer_id', $id)->count();
            return view('front.customers.edit', compact('customer', 'myestablishments'));
        } else {
            session()->flash('messages.error', ['Você não tem permissão para editar esse usuário.']);
            return redirect()->route('customer.dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = $this->customers->where('id', $id)->first();

        if (!$customer) {
            session()->flash('messages.error', ['Usuário não existe!']);
            return redirect()->route('customer.dashboard');
        }

        // validate the data
        $this->validate($request, [
            'name'          => 'required',
            'email'         => "required|unique:customers,email,$id,id",
            'password'      => 'nullable|min:5',
            'document'      => "required|unique:customers,document,$id,id",
            'phone'         => "required"
        ]);
        if ($request->password) {
            $password = bcrypt($request->password);
            $customer->password = $password;
        }
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->save();

        session()->flash('messages.success', ['Usuário alterado com sucesso!']);
        return redirect()->route('customer.dashboard');

        // // validate the data
        // $rules = [
        //     'name'          => 'required',
        //     'email'         => "required|unique:customers,email,$id,id",
        //     'password'      => 'nullable|min:5',
        //     'document'      => "required|nullable",
        // ];

        // $validator = Validator::make($request->all(), $rules);

        // if ($validator->fails()) {
        //     $messages = $validator->messages();

        //     $displayErrors = '';
        //     foreach ($messages->all("<p style=\"margin:0;padding:0;\">:message</p>") as $error) {
        //         $displayErrors .= $error;
        //     }

        //     //return $displayErrors;
        //     session()->flash('messages.error', [$displayErrors]);
        //     return redirect()->route('customer.dashboard');
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($customer_id)
    {
        $db = app('db');

        $db->beginTransaction();

        try {
            $id = $customer_id;
            //   dd($id);   

            $getCustomer = \DB::table('customers')->where('id', $id)->first();
            $hasEstablishment = \DB::table('establishments')->where('customer_id', $id)->first();
            if ($hasEstablishment) {
                session()->flash('messages.error', ['Você não pode deletar a conta, há estabelecimentos cadastrados.']);
                return redirect()->back();
            } else {
                Auth::guard('customer')->logout();
                \DB::table('establishments')->where('customer_id', $id)->delete();
                \DB::table('customer_plans')->where('customer_id', $id)->delete();
                \DB::table('customers')->where('id', $id)->delete();
                $db->commit();
                session()->flash('messages.success', ['Usuário deletado com sucesso!']);
                return redirect()->route('customer.auth.login');
            }
        } catch (\Exception $error) {
            $db->rollback();
            return response()->json(['message' => $error->getMessage()], 400);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function plan($estabelecimento_id)
    {
        $plans = Plan::where('status', '=', '1')->where('id', '>=', 1)->where('upgrade', '=', 0)->orderBy('price', 'DESC')->paginate();
        $pageTerms = Page::where('slug', 'termos-e-condicoes')->get();
        $customer_id = Auth::user()->id;

        // quantidade de planos
        $count_plans = DB::table('customer_plans')->where('customer_id', $customer_id)->where('plan_status', 'Pago')->where('plan_id', '<', '5')->count();
        // dd($count_plans);

        // obtem o desconto do usuario
        $discount_customer = Auth::user()->plan_discount;
        // dd($discount_customer);

        return view('front.customers.plan', compact('plans'))->with('pageTerms', $pageTerms)->with('estabelecimento_id', $estabelecimento_id)->with('discount_customer', $discount_customer)->with('count_plans', $count_plans);
    }

    public function planpayment(Request $request, $estabelecimento_id)
    {
        $plan_id = $request->input('plan_id');
        $customer_id = Auth::user()->id;

        $count_plans = DB::table('customer_plans')->where('customer_id', $customer_id)->where('plan_status', 'Pago')->where('plan_id', '<', '4')->count();
        // dd($count_plans);

        if ($count_plans > 1) {
            $discount = (float) $request->discount_customer;
            $discount = $discount / 100.0;
            $price = (float) $request->input('price');
            $pricefinal = $price - ($discount * $price);
        } else {
            $pricefinal = (float) $request->input('price');
        }

        // dd($pricefinal);

        DB::table('customer_plans')->where('establishment_id', $estabelecimento_id)->update([
            'plan_id'               => $plan_id,
            'plan_status'           => 'Aguardando',
            'plan_datebegin'        => Carbon::now(),
            'plan_dateend'          => Carbon::now(),
            'plan_method'           => $request->input('plan_method'),
            'renews'                => '0',
            'updated_at'            => Carbon::now()
        ]);

        // dd($result);

        $planDetails = Plan::findOrFail($plan_id);

        // // store in the database
        // $transations = new Transation;
        // $transations->customer = Auth::user()->name;
        // $transations->customer_id = $customer_id;
        // $transations->plan_name = $planDetails->name;
        // $transations->plan_period = $request->select_period;
        // $transations->plan_method = $request->plan_method;
        // $transations->plan_value = $pricefinal;
        // $transations->date =  Carbon::now()->format('Y-m-d H:i:s');
        // $transations->save();

        if ($request->plan_method === "gratis") {
            return redirect()->route('customer.plan.free');
        } else if ($request->plan_method === "banktransfer") {
            return redirect()->route('customer.plan.success');
        } else if ($request->plan_method === "mercadopago") {
            // BEGIN - Envia para o Mercado Pago - SDK            
            $preference_data = array(
                "items" => array(
                    array(
                        "title" => '#' . $estabelecimento_id . ' - ' . Auth::user()->name . ' - ' . $planDetails->name . ' - ' . $request->input('select_period') . ' dias',
                        "quantity" => 1,
                        "currency_id" => "BRL",
                        "unit_price" => $pricefinal
                    )
                )
            );
            try {
                $preference = MP::create_preference($preference_data);
                return redirect()->to($preference['response']['init_point']);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
            // END - Envia para o Mercado Pago - SDK
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upgrades($estabelecimento_id)
    {
        $upgrades = Plan::where('status', '=', '1')->where('id', '>=', 1)->where('upgrade', '=', '1')->orderBy('id', 'ASC')->paginate();
        $pageTerms = Page::where('slug', 'termos-e-condicoes')->get();

        return view('front.customers.upgrade', compact('upgrades'))->with('pageTerms', $pageTerms)->with('estabelecimento_id', $estabelecimento_id);
    }

    public function upgradespayment(Request $request, $estabelecimento_id)
    {
        $period = 0;
        if ($request->input('select_period')) {
            $period = Carbon::now()->addDays($request->input('select_period'))->format('Y-m-d');
        }

        $plan_id = $request->input('plan_id');

        DB::table('customer_plans')->where('establishment_id', $estabelecimento_id)->insert([
            'customer_id'           => Auth::user()->id,
            'establishment_id'      => $estabelecimento_id,
            'plan_id'               => $plan_id,
            'plan_status'           => 'Aguardando',
            'plan_datebegin'        => Carbon::now(),
            'plan_dateend'          => Carbon::now(),
            'plan_method'           => $request->input('plan_method'),
            'renews'                => '0',
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now()
        ]);

        // dd($result);

        $planDetails = Plan::findOrFail($plan_id);

        // // store in the database
        // $transations = new Transation;
        // $transations->customer = Auth::user()->name;
        // $transations->customer_id = Auth::user()->id;
        // $transations->plan_name = $planDetails->name;
        // $transations->plan_period = $request->select_period;
        // $transations->plan_method = $request->plan_method;
        // $transations->plan_value = $request->price;
        // $transations->date =  Carbon::now()->format('Y-m-d H:i:s');
        // $transations->save();


        if ($request->plan_method === "banktransfer") {
            return redirect()->route('customer.plan.success');
        } else if ($request->plan_method === "mercadopago") {
            // BEGIN - Envia para o Mercado Pago - SDK            
            $preference_data = array(
                "items" => array(
                    array(
                        "title" => '#' . $estabelecimento_id . ' - ' . Auth::user()->name . ' - ' . $planDetails->name . ' - ' . $request->input('select_period') . ' dias',
                        "quantity" => 1,
                        "currency_id" => "BRL",
                        "unit_price" => (float) $request->input('price')
                    )
                )
            );
            try {
                $preference = MP::create_preference($preference_data);
                return redirect()->to($preference['response']['init_point']);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
            // END - Envia para o Mercado Pago - SDK
        }
    }


    public function planrenew($id)
    {
        $planAtual = \DB::table('customer_plans as a')
            ->join('plans as b', 'a.plan_id', 'b.id')
            ->where('a.id', $id)
            // ->where('b.id', '<', '4')
            ->first();

        // dd($establishment);

        $plansAvailable = Plan::where('status', '=', '1')->where('id', '>=', 2)->where('upgrade', '=', 0)->orderBy('price', 'DESC')->paginate();

        $establishment_id = $planAtual->establishment_id;
        $customer_id = Auth::user()->id;

        // quantidade de planos
        $count_plans = DB::table('customer_plans')->where('customer_id', $customer_id)->where('plan_status', 'Pago')->where('plan_id', '<', '4')->count();

        // dd($count_plans);

        // obtem o desconto do usuario
        $discount_customer = Auth::user()->plan_discount;

        $pageTerms = Page::where('slug', 'termos-e-condicoes')->get();
        return view('front.customers.renew', compact('planAtual'))->with('pageTerms', $pageTerms)->with('discount_customer', $discount_customer)->with('count_plans', $count_plans)->with('plansAvailable', $plansAvailable);
    }

    public function planrenewpayment(Request $request, $establishment_id)
    {
        // dd($request->all());
        $plan_id = $request->input('plan_id');
        $plan_atual_id = $request->input('plan_atual_id');

        DB::table('customer_plans')->where('establishment_id', $establishment_id)->where('plan_id', $plan_atual_id)->update([
            'plan_id'               => $plan_id,
            'plan_status'           => 'Aguardando',
            // assim que renovar não muda a data de vencimento
            // 'plan_dateend'          => Carbon::parse($request->input('plan_dateend')),
            'plan_method'           => $request->input('plan_method'),
            'renews'                => DB::raw('renews + 1'),
            'updated_at'            => Carbon::now()
        ]);

        $planDetails = Plan::findOrFail($plan_id);

        // // store in the database
        // $transations = new Transation;
        // $transations->customer = Auth::user()->name;
        // $transations->customer_id = Auth::user()->id;
        // $transations->plan_name = $planDetails->name;
        // $transations->plan_period = $request->select_period;
        // $transations->plan_method = $request->plan_method;
        // $transations->plan_value = $request->price;
        // $transations->date =  Carbon::now()->format('Y-m-d H:i:s');
        // $transations->save();

        if ($request->plan_method === "gratis") {
            return redirect()->route('customer.plan.free');
        } else if ($request->plan_method === "banktransfer") {
            return redirect()->route('customer.plan.success');
        } else if ($request->plan_method === "mercadopago") {
            // BEGIN - Envia para o Mercado Pago - SDK            
            $preference_data = array(
                "items" => array(
                    array(
                        "title" => '#' . $establishment_id . ' - ' . Auth::user()->name . ' - ' . $planDetails->name . ' - ' . $request->input('select_period') . ' dias',
                        "quantity" => 1,
                        "currency_id" => "BRL",
                        "unit_price" => (float) $request->input('price')
                    )
                )
            );
            try {
                $preference = MP::create_preference($preference_data);
                return redirect()->to($preference['response']['init_point']);
            } catch (Exception $e) {
                dd($e->getMessage());
            }
            // END - Envia para o Mercado Pago - SDK
        }
    }


    public function plansuccess()
    {
        return view('front.customers.plansuccess');
    }

    public function planfree()
    {
        return view('front.customers.planfree');
    }

    public function transation()
    {
        $transations = Transation::where('customer_id', Auth::id())->orderBy('date', 'DESC')->paginate();
        return view('front.customers.transation', compact('transations'));
    }
}
