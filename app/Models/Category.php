<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public function establishments()
	{
		return $this->hasMany('Establishment');
	}
    
    protected $fillable = [
    	'title',
    	'image',
    	'slug',
    	'status'
    ];


    public function photos()
    {
        return $this->hasMany('App\Models\CategoryPhoto');
    }
}
