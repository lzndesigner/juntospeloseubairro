<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPlans extends Model
{	

	protected $fillable = [
        'customer_id',
        'establishment_id',
        'plan_id',
        'plan_status',
        'plan_datebegin',
        'plan_dateend',
        'plan_method',
        'renews'
    ];

}