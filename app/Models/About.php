<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [
    	'page_description'
    ];

    // Desativado
    public function photos(){
    	return $this->hasMany('App\Models\OfficePhotos');
    }
}
