<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
    	'title',
    	'body',
    	'slug',
    	'tags',
    	'status'
    ];

    protected $appends = [
        'photo_default'
    ];

    public function photos()
    {
    	return $this->hasMany('App\Models\BlogPhoto');
    }
    
    public function getPhotoDefaultAttribute() 
    {
        if($this->photos->isEmpty()) return '/sem_image.png';

        $photo = $this->photos()->first();
        
        return $photo->has(256) ? $photo->url(256) : '/sem_image.png';
    }
}