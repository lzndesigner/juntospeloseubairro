<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPhoto extends Model
{
    protected $fillable = ['filename'];
    
    public function blog()
    {
        return $this->belongsTo('App\Models\Category');
    }
    
}