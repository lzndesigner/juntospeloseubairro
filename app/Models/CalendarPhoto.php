<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarPhoto extends Model
{
    protected $fillable = ['filename'];
    
    public function calendar()
    {
        return $this->belongsTo('App\Models\Calendar');
    }
    
}