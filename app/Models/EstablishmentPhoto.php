<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstablishmentPhoto extends Model
{
    protected $fillable = ['filename', 'sizes'];

    /**
     * @var array
     */
    protected $casts = [
        'sizes' => 'collection'
    ];

    public function blog()
    {                
        return $this->belongsTo('App\Models\Establishment');
    }

    public function url($size = 100)
    {        
        //aqui é onde ele salva?
        // no blog foi preciso subir muitas fotos ai ele utilizou o s3, ele me disse que na parte do Blog tem esse tratamento do resize.        
        return app('filesystem')->drive('s3')->url(
            $this->sizes->get($size)
        );
    }
}
