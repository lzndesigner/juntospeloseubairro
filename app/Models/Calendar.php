<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Support\CalendarSupport;

class Calendar extends Model
{
    protected $fillable = [
    'name',
    'description',
    'day',
    'month',
    'year',
    'hour',
    'local',
    'classification',
    'slug',
    'status'
    ];

    //temos alguns modos de trabalho para no poluir nosso valor iniciail
    // digamos que queria carregar ambos os modos em um eloquent, como fazer?

    protected $appends = [
    'abreviado',
    'extenso'
    ];

    public function getAbreviadoAttribute()
    {
        // como no existe na tabela um campo com o nome "abreviado" no temos o suporte igual embaixo,
        // esta conseguindo comprreender? compreendi um pouco.
        return CalendarSupport::abreviado($this->attributes['month'] - 1);
    }

    public function getExtensoAttribute()
    {
        // como no existe na tabela um campo com o nome "abreviado" no temos o suporte igual embaixo,
        // esta conseguindo comprreender? compreendi um pouco.
        return CalendarSupport::extenso($this->attributes['month'] - 1);
    }

    public function scopeEventsInOpened($query) {

        $now = now();

        return $query
        ->where('day', '>=', $now->day)
        ->where('month', '>=', $now->month)
        ->where('year', '>=', $now->year);
    }

    public function photos()
    {
        return $this->hasMany('App\Models\CalendarPhoto');
    }
}
