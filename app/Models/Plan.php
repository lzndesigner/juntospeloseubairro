<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{	
	public function customers(){
		return $this->hasMany('Customer');
	}

	protected $fillable = [
        'name',
        'period',
        'price',
        'description',
        'statistics',
        'upgrade',
        'status'
    ];

}