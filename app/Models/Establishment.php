<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    protected $fillable = [
    'name',
    'image',
    'category_id',
    'customer_id',
    'type',
    'featured',
    'description',
    'promotion',
    'cep',
    'address',
    'number',
    'complement',
    'distric',
    'city',
    'state',
    'country',
    'phone',
    'phone_whatsapp',
    'cellphone',
    'email',
    'website',
    'slug',
    'status',
    'redesocial_facebook',
	'redesocial_instagram',
	'redesocial_twitter',
    'redesocial_linkedin'
    ];

    public function getPostCategory(){
        return Category::where('id', $this->category_id)->first()->title;
    }

    public function getPostCategorySlug(){
        return Category::where('id', $this->category_id)->first()->slug;
    }

    public function getPostCustomer(){
        return Customer::where('id', $this->customer_id)->first();
    }

    public function getAllCustomer(){
        return Customer::where('status', 1)->get();
    }

    public function photos()
    {
        return $this->hasMany('App\Models\EstablishmentPhoto'); 
    }
}