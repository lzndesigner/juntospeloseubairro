<?php

namespace App\Support;


final class CalendarSupport {
	
	protected static $month = [
        'Jan' => 'Janeiro',
        'Fev' => 'Fevereiro',
        'Mar' => 'Março',
        'Abr' => 'Abril',
        'Mai' => 'Maio',
        'Jun' => 'Junho',
        'Jul' => 'Julho',
        'Ago' => 'Agosto',
        'Set' => 'Setembro',
        'Out' => 'Outubro',
        'Nov' => 'Novembro',
        'Dez' => 'Dezembro'
    ];


	public static function abreviado($index = 0) : string 
	{
		$list = array_keys(self::$month);
		
		return $list[$index];
	}

	public static function extenso($index = 0) : string 
	{
		$list = array_values(self::$month);
		
		return $list[$index];
	}
}