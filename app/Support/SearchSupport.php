<?php

namespace App\Support;

use App\Models\Blog;
use App\Models\Establishment;
use App\Models\Calendar;

class SearchSupport
{
    /**
     * Parametros de busca
     * 
     * @var array 
     */
    protected $queries = [
        //
        // Filtrar em estabelecimentos
        //
        Establishment::class => [
            'name' => '%{variable}%',
            'description' => '%{variable}%',
            'address' => '%{variable}%',
            'cep' => '%{variable}%',
            'state' => '%{variable}%',
            'city' => '%{variable}%',
            'distric' => '%{variable}%'
        ],
        //
        // Filtrar em blogs
        //
        Blog::class => [
            'title' => '%{variable}%',
            'body' => '%{variable}%',
            'tags' => '%{variable}%'
        ],
        //
        // Filtrar em Calendarios
        //
        Calendar::class => [
            'name' => '%{variable}%',
            'local' => '%{variable}%',
            'description' => '%{variable}%'
        ]
    ];

    /**
     * @var string
     */
    protected $text;

    /**
     * 
     * @param string $text
     */
    private function __construct(string $text)
    {
        $this->text = $text;
    }

    /**
     * 
     * @return SearchSupport
     */
    public static function find(string $text): SearchSupport
    {
        return new self($text);
    }
    /**
     * Preparar parametros de busca.
     * 
     * return array
     */
    public function mountQueries(): array
    {
        $queries = [];

        foreach ($this->queries as $model => $attributes) {
            // $name = strtolower(str_replace('\\', '.', $model));
            $name = $model;

            if (!isset($queries[$name])) {
                $queries[$name] = [];
            }

            if (count($attributes) >= 0) {
                foreach ($attributes as $column => $value) {
                    if (count($queries[$name]) <= 0) {
                        $queries[$name]['where'][] = [$column, 'like', $this->formatField($value)];
                    } else {
                        $queries[$name]['orWhere'][] = [$column, 'like', $this->formatField($value)];
                    }
                }
            }
        }

        return $this->prepareQueryBuilder($queries);
    }

    /**
     * Montar consulta
     * 
     * @return array
     */
    public function prepareQueryBuilder($queries): array
    {

        $results = [];
        $search = [];

        foreach ($queries as $model => $builder) {
            if (!isset($results[$model])) {
                //
                // Iniciar instancia
                //
                $results[$model] = app($model)->query();
            }
            //
            //
            //
            foreach ($builder as $func => $params) {
                //
                // Preparar querys
                //
                foreach ($params as $param) {
                    $results[$model]->{$func}(...$param);
                }
            }
            $name = strtolower(explode('\\', $model)[2]);
            // dd($name);
            //
            // Executar consulta
            //
            $search[$name] = $results[$model]->get();
        }

        return $search;
    }

    /**
     * Customizar
     * 
     * @return string
     */
    public function formatField($text): string
    {
        return str_replace('{variable}', $this->text, $text);
    }
}
