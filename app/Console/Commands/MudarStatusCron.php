<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
class MudarStatusCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mudarstatus:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mudar Status quando expirado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = date('Y-m-d');
        DB::table('customer_plans')->where('plan_status', 'Pago')->where('plan_dateend','<', $data)->update([
            'plan_status' => 'Expirado'
        ]);
        
    }
}
