<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Page;

class PagesTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->delete();

        Page::create([
            'title' => 'Termos & Condições',
            'slug' => 'termos-e-condicoes',
            'body' => 'Esses são os termos e condições de uso.',
        ]);

        Page::create([
            'title' => 'Como Funcionamos',
            'slug' => 'como-funcionamos',
            'body' => '',
        ]);

    }
}
