<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Customer;

class CustomersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->delete();

        Customer::create([
            'name'=>'Leonardo Augusto',
            'email'=>'customer@innsystem.com.br',
            'password'=>bcrypt('123456'),
            'cpf_cnpj'=>'000.000.000-99',
            'status'=>'1',
            'plan_id'=>'1',
            'plan_status'=>'1',
            'plan_dateend'=>'2020-05-30',
            'plan_method'=>'banktransfer',
            'remember_token' => str_random(10),
        ]);

    }
}
