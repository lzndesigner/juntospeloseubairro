<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Config;

class ConfigsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->delete();

        Config::create([
            'config_title' => 'Juntos pelo seu Bairro',
            'config_description' => 'Conectando negócios locais à consumidores',
            'config_keywords' => 'juntos pelo seu bairro, juntos, bairro, catalogo, estabelecimentos, apoie os pequenos, compre do pequeno, compre local, juntos pelo seu negócio, compre do bairro',
            'config_email' => 'atendimento@juntospeloseubairro.com.br',
            'config_phone' => '',
            'config_cellphone' => '',
            'config_information' => '',
            'company_name' => 'Juntos pelo seu Bairro',
            'company_proprietary' => 'Alexandra',
            'company_cep' => '05727-240',
            'company_address' => 'Endereço completo',
            'company_city' => 'São Paulo',
            'company_state' => 'São Paulo',
            'company_country' => 'BR',
            'redesocial_facebook' => 'https://www.facebook.com/juntospeloseubairro/',
            'redesocial_instagram' => 'https://www.instagram.com/juntospeloseubairro/',
            'redesocial_twitter' => '#',
            'redesocial_linkedin' => 'https://www.linkedin.com/company/juntospeloseubairro',
            'payment_banktransfer_status' => '1',
            'payment_banktransfer_info' => '** Trabalhamos com Banco Itau **',
        ]);
    }
}
