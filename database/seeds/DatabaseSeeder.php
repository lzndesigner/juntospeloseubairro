<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $this->call(ConfigsTablesSeeder::class);
    $this->call(UsersTablesSeeder::class);
    $this->call(PlansTablesSeeder::class);
    $this->call(AboutsTablesSeeder::class);
    $this->call(PagesTablesSeeder::class);
    // $this->call(CustomersTablesSeeder::class);
  }
}
