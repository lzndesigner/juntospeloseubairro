<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\Plan;

class PlansTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->delete();

        Plan::create([
            'name' => 'Apreciar',
            'period' => '30',
            'price' => '0',
            'description' => '<p>* Divulgação do estabelecimento na nossa plataforma</p><p>* Maior e Melhor Exposição em sites de Busca</p>',
            'statistics' => '0',
            'upgrade' => '0',
            'status' => '1',
        ]);

        Plan::create([
            'name' => 'Aproveitar',
            'period' => '90',
            'price' => '149.70',
            'description' => '<p>* Divulgação do estabelecimento na nossa plataforma</p><p>* Maior e Melhor Exposição em sites de Busca</p><p>* Divulgação nas nossas mídias sociais Facebook, Instagram, Linkedin</p>',
            'statistics' => '0',
            'upgrade' => '0',
            'status' => '1',
        ]);

        Plan::create([
            'name' => 'Desfrutar',
            'period' => '180',
            'price' => '239.40',
            'description' => '<p>* Divulgação do estabelecimento na nossa plataforma</p><p>* Maior e Melhor Exposição em sites de Busca</p><p>* Divulgação nas nossas mídias sociais Facebook, Instagram, Linkedin</p><p>* Programa de Fidelização de Clientes</p><p>* Estatística de Acesso ao seu anúncio</p><p>* Dicas sobre Planejamento, Finanças, Controle e Organização</p>',
            'statistics' => '1',
            'upgrade' => '0',
            'status' => '1',
        ]);

        Plan::create([
            'name' => 'Saborear',
            'period' => '360',
            'price' => '358.80',
            'description' => '<p>* Divulgação do estabelecimento na nossa plataforma</p><p>* Maior e Melhor Exposição em sites de Busca</p><p>* Divulgação nas nossas mídias sociais Facebook, Instagram, Linkedin</p><p>* Programa de Fidelização de Clientes</p><p>* Estatística de Acesso ao seu anúncio</p><p>* Dicas sobre Planejamento, Finanças, Controle e Organização</p><p>* Links patrocinados e anúncios direcionados pela plataforma (vide regras)</p>',
            'statistics' => '1',
            'upgrade' => '0',
            'status' => '1',
        ]);

        Plan::create([
            'name' => 'Destaque',
            'period' => '30',
            'price' => '50.00',
            'description' => 'Fique em Destaque',
            'statistics' => '0',
            'upgrade' => '1',
            'status' => '1',
        ]);

        Plan::create([
            'name' => 'Slider',
            'period' => '30',
            'price' => '65.00',
            'description' => 'Mostrar no Slide',
            'statistics' => '0',
            'upgrade' => '1',
            'status' => '1',
        ]);
    }
}
