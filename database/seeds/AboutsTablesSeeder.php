<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\About;

class AboutsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abouts')->delete();

        About::create([
            'page_description'=>'<p><span style="background-color: transparent; color: inherit;">Juntos pelo seu Bairro é uma iniciativa com um único propósito: conectar negócios aos consumidores da sua região, por meio de divulgação, parcerias, posts impulsionados* ou anúncios pagos pela própria plataforma*.</span><br></p><p><span style="background-color: transparent; color: inherit;">Somos muito mais que uma plataforma para seu cadastro, somos seu parceiro de divulgação.</span><br></p><p><span style="background-color: transparent; color: inherit;">Por “negócio” entende-se:&nbsp; autônomos, micros, pequenas, médias e grandes empresas.</span><br></p><p><span style="background-color: transparent; color: inherit;">Trata-se de uma iniciativa que terá perpetuidade e sem intenção de induzir ou oferecer qualquer produto no pós-cadastro, ou seja, nosso propósito é genuíno: divulgar o seu negócio junto com você.</span><br></p><p><span style="background-color: transparent; color: inherit;">Juntos pelo seu Bairro conecta, une, aproxima: estamos JUNTOS.&nbsp; Acreditamos que você pode sempre mais!</span><br></p><p>De acordo com a regras definidas pelo “Juntos pelo seu Bairro”</p>',
        ]);
    }
}
