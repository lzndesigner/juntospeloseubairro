<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('category_id');
            $table->integer('customer_id');
            $table->string('type');
            $table->string('featured')->nullable();
            $table->text('description')->nullable();
            $table->text('promotion')->nullable();
            $table->string('cep')->nullable();
            $table->string('address')->nullable();
            $table->string('number')->nullable();
            $table->string('complement')->nullable();
            $table->string('distric');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('phone');
            $table->string('phone_whatsapp');
            $table->string('cellphone')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->integer('views')->nullable()->default('0');
            $table->string('slug', 100)->unique();
            $table->index('slug');
            $table->string('status');
            $table->string('redesocial_twitter')->nullable();
            $table->string('redesocial_instagram')->nullable();
            $table->string('redesocial_facebook')->nullable();
            $table->string('redesocial_linkedin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishments');
    }
}
