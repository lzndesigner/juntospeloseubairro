<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('establishment_id');
            $table->integer('plan_id')->nullable();
            $table->string('plan_status');
            $table->date('plan_datebegin')->nullable();
            $table->date('plan_dateend')->nullable();
            $table->string('plan_method');
            $table->integer('renews')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_plans');
    }
}
