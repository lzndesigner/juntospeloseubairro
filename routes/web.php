<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/webmail', function () {
  return redirect('http://webmail.innsystem.com.br/');
});
Route::get('/admin', function () {
  return redirect('/dashboard');
});

Route::get('/manutencao', 'Front\HomeController@maintence');
Route::get('/', 'Front\HomeController@index')->name('home');

Route::get('/sobre', 'Front\AboutController@index');
Route::get('/planos', 'Front\HomeController@planos');

Route::get('/encontrar-estabelecimentos', 'Front\HomeController@index');
Route::get('/anunciar', 'Front\HomeController@anunciar');

Route::get('categorias', 'Front\CategoriesController@index');
Route::get('categorias/{slugCategoria}', 'Front\CategoriesController@show');
Route::get('categorias/{slugCategoria}/{slugEstabelecimento}', 'Front\CategoriesController@showEstablishment');
Route::get('estabelecimento/{slugEstablishment}', 'Front\CategoriesController@showEstablishmentTwo');

Route::get('calendario/', 'Front\CalendarsController@index');
Route::get('calendario/{slugCalendario}', 'Front\CalendarsController@showCalendar');

Route::get('blogs', 'Front\BlogsController@index');
Route::get('blogs/{slugBlog}', 'Front\BlogsController@show');
Route::get('/depoimentos', 'Front\ReviewController@index');
Route::post('/depoimentos-create', 'Front\ReviewController@store');
Route::get('/contato', 'Front\ContactController@index');

Route::get('/pagina/{slugPage}', 'Front\HomeController@pages');

Route::post('/estabelecimentos/busca', 'Front\HomeController@busca');
Route::get('/estabelecimentos/buscadistrito', 'Front\HomeController@buscadistrito');


Route::prefix('services')->namespace('Services')->group(function () {
  Route::post('send-mail', 'MailController@sendContact');
});




Auth::routes();

// Painel Administrativo
Route::prefix('dashboard')->middleware(['auth'])->group(function () {

  Route::get('/export-customers', 'Admin\DashboardController@exportCustomers')->name('export.customers');
  Route::get('/export-customers-establishments', 'Admin\DashboardController@exportCustomersEstablishments')->name('export.customers.establishments');

  // Geral
  Route::get('/', 'Admin\DashboardController@index');
  Route::resource('users', 'Admin\UsersController');
  Route::resource('pages', 'Admin\PagesController');
  Route::resource('configs', 'Admin\ConfigController');

  // Catalogo
  Route::resource('categories', 'Admin\CategoriesController');
  Route::delete('categories/{category}/photos/{photo}', 'Admin\\CategoriesController@photoRemove')->name('remove.photo');

  Route::resource('establishments', 'Admin\EstablishmentsController');
  Route::delete('establishments/{establishment}/photos/{photo}', 'Admin\\EstablishmentsController@photoRemove')->name('remove.photo');
  Route::post('establishments-upload/{establishment_id}', 'Admin\EstablishmentsController@upload')->name('admin-upload-miniaturas');
  Route::resource('customers', 'Admin\CustomersController');
  Route::resource('customerplans', 'Admin\CustomerPlansController');
  Route::resource('transations', 'Admin\TransationsController');
  Route::resource('plans', 'Admin\PlansController');

  Route::get('customerssetstatus', 'Admin\CustomersController@setstatus')->name('customers.setstatus');


  // Extras
  Route::resource('about', 'Admin\AboutController');
  Route::resource('blogs', 'Admin\BlogsController');
  Route::delete('blogs/{blog}/photos/{photo}', 'Admin\\BlogsController@photoRemove')->name('remove.photo');
  Route::resource('reviews', 'Admin\ReviewsController');
  Route::resource('sliders', 'Admin\SlidersController');
  Route::resource('apoios', 'Admin\ApoiosController');
  Route::resource('calendars', 'Admin\CalendarsController');
  Route::delete('calendars/{calendar}/photos/{photo}', 'Admin\\CalendarsController@photoRemove')->name('remove.photo');

  // Newsletter
  Route::prefix('newsletter')->group(function () {
    Route::get('list', 'Admin\NewsletterController@index');
    Route::delete('/{newsletter}', 'Admin\NewsletterController@destroy');
    Route::get('generate-list', 'Admin\NewsletterController@generate');
    Route::view('/', 'admin.newsletter.index')->name('newsletter.index');
  });
});

// Minha Conta
Route::prefix('minha-conta')->group(function () {
  // Login - Register
  Route::get('login', 'Customer\LoginController@login')->name('customer.auth.login');
  Route::post('login', 'Customer\LoginController@loginCustomer')->name('customer.auth.loginCustomer');
  Route::get('logout', 'Customer\LoginController@logout')->name('customer.auth.logout');
  // Registro
  Route::get('register', 'Customer\HomeController@create')->name('customer.register');
  Route::post('register', 'Customer\HomeController@store')->name('customer.register.store');

  // Reset
  Route::get('reset', 'Customer\ResetPasswordController@showLinkRequestForm')->name('customer.reset');
  Route::get('reset/new/{token}', 'Customer\ResetPasswordController@showResetForm')->name('customer.reset.new');
  Route::post('reset/request', 'Customer\ResetPasswordController@sendResetLinkEmail')->name('customer.reset.request');
  Route::post('reset/save', 'Customer\ResetPasswordController@saveNewPassword')->name('customer.reset.newpassword');



  Route::get('/', 'Customer\HomeController@index')->name('customer.dashboard');
  Route::resource('editar', 'Customer\HomeController');
  Route::post('cliente-deletar/{customer_id}', 'Customer\HomeController@destroy')->name('customer.destroy');
  Route::get('/plano/{estabelecimento_id}', 'Customer\HomeController@plan')->name('customer.plan');
  Route::get('/plano/contratar/{estabelecimento}', 'Customer\HomeController@planpayment')->name('customer.plan.payment');
  Route::get('/planoupgrades/{estabelecimento_id}', 'Customer\HomeController@upgrades')->name('customer.plan.upgrades');
  Route::get('/planoupgrades/contratar/{estabelecimento}', 'Customer\HomeController@upgradespayment')->name('customer.plan.upgradespayment');
  Route::get('/plano/renovar/{establishment_id}', 'Customer\HomeController@planrenew')->name('customer.plan.renew');
  Route::get('/plano/renovar/pagamento/{establishment_id}', 'Customer\HomeController@planrenewpayment')->name('customer.plan.renewpayment');
  Route::get('/planosuccess/sucesso', 'Customer\HomeController@plansuccess')->name('customer.plan.success');
  Route::get('/planosuccess/free', 'Customer\HomeController@planfree')->name('customer.plan.free');


  Route::get('/transacoes', 'Customer\HomeController@transation')->name('customer.transation');


  // Gerenciamento
  Route::resource('estabelecimentos', 'Customer\EstablishmentController');
  Route::get('estatisticas', 'Customer\EstablishmentController@statistics')->name('statistics');
  Route::delete('estabelecimentos/{establishment}/photos/{photo}', 'Customer\EstablishmentController@photoRemove')->name('remove.photo');

  Route::post('estabelecimentos-upload/{establishment_id}', 'Customer\EstablishmentController@upload')->name('upload-miniaturas');
});


// Route::get('/home', 'HomeController@index')->name('home');
