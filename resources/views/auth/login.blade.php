<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Área Restrita - {{ $configs->config_title ?? 'Catalogo Base' }}</title>

  <!-- ========== Css Files ========== -->
    <!-- <link href="{{ asset('backend/css/root.css?13') }}" rel="stylesheet"> -->
    <link href="{{ asset('backend/css/bootstrap.css?16') }}" rel="stylesheet">
  <link href="{{ asset('backend/css/style.css?16') }}" rel="stylesheet">
  <link href="{{ asset('backend/css/responsive.css?16') }}" rel="stylesheet">
  <link href="{{ asset('backend/css/shortcuts.css?16') }}" rel="stylesheet">
  <link href="{{ asset('backend/css/custons.css?16') }}" rel="stylesheet">
  <link rel="shortcut icon" href="{{ asset('galerias/favicon.ico?6') }}" />
  <style type="text/css">
    body{background: #F5F5F5;}
  </style>
  <script src="https://kit.fontawesome.com/04571ab3d2.js" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="login-form">
      <a href="/dashboard"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}" class="img-responsive" style="width:80%; margin:0 auto;"></a>
        <br class="clearfix">
      <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="top">
          <h1>Área Restrita</h1>
        </div>
        <div class="form-area">
          <div class="group{{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="E-mail" required autofocus class="form-control">
            <i class="fa fa-user"></i>
          </div>
          
          <div class="group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" name="password" placeholder="Senha" required  class="form-control">
            <i class="fa fa-key"></i>
          </div>


          @if ($errors->has('email'))
          <div class="alert alert-danger">
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            </div>
            @endif
            
          @if ($errors->has('password'))
          <div class="alert alert-danger">
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            </div>
            @endif
          <button type="submit" class="btn btn-default btn-block">Logar</button>
        </div>
      </form>
      <div class="footer-links row">
        <div class="col-xs-12 text-right"><a href="{{ route('password.request') }}"><i class="fa fa-lock"></i> Esqueceu sua senha?</a></div>
      </div>
    </div>

</body>
</html>