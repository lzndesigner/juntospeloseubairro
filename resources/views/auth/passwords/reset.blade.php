<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Área Restrita - {{ $configs->config_title ?? 'Catalogo Base' }}</title>

    <!-- ========== Css Files ========== -->
    <!-- <link href="{{ asset('backend/css/root.css?13') }}" rel="stylesheet"> -->
    <link href="{{ asset('backend/css/bootstrap.css?16') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style.css?16') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/responsive.css?16') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/shortcuts.css?16') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/custons.css?16') }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset('galerias/favicon.ico?6') }}" />
    <style type="text/css">
        body {
            background: #F5F5F5;
        }
    </style>
    <script src="https://kit.fontawesome.com/04571ab3d2.js" crossorigin="anonymous"></script>
</head>

<body>
    <div class="login-form">
        <a href="/dashboard"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}" class="img-responsive" style="width:80%; margin:0 auto;"></a>
        <br class="clearfix">

        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <div class="top">
                <h1>{{ isset($configs->config_title) ? $configs->config_title : 'Meu site' }}</h1>
                <h4>Recuperar Senha</h4>
            </div>
            <div class="form-area">
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required placeholder="E-mail de Acesso" autofocus>
                    <i class="fa fa-user"></i>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" required placeholder="*******">
                    <span class="help-block">Mínimo de 5 caracteres.</span>
                    <i class="fa fa-key"></i>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="*******">
                    <i class="fa fa-key"></i>
                    @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-default btn-block">
                    Resetar Senha
                </button>


            </div><!-- form-area -->
        </form>

        <div class="footer-links row">
            <div class="col-xs-12"><a href="{{ route('login') }}"><i class="fa fa-sign-in"></i> Acessar Conta</a></div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('backend/js/jquery.min.js') }}"></script>
    <script>
      // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
      $("form").submit(function(e) {
        //disable the submit button
        $("form button").attr("disabled", true);
        $('form').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
        setTimeout(function() {
          $('form button').prop("disabled", false);
          $('form').parent().find('svg.fa-spinner').html('');
        }, 8000);
      });
    </script>
</body>

</html>
