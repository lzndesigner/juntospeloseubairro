@component('vendor.mail.html.message')

    # {{ ucwords($mail['service']) }}

    Nome : {{ $mail['name'] }}
    E-mail: {{ $mail['email'] }}

    {{ $mail['message'] }}


    Obrigado.
    {{ config('app.name') }}
@endcomponent
