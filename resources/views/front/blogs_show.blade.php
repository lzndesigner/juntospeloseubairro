@extends('front.base')
@section('title', $blog->title)
@section('breadcrumb')
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1 style="line-height:100%;">@yield('title')</h1>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/blogs') }}">Todas publicações</a></li>
      <li class="active">@yield('title')</li>
    </ol>

  </div>
  @include('front.includes.box-search')
</div><!-- section-home -->
@endsection

@section('content')
<section class="section sub-page">
  <div class="container">
    <div class="box-general">
      <div class="row">

        <div class="col-xs-12 col-md-12">
          <div class="single-post nobottommargin">

            <div class="entry clearfix">

              <ul class="entry-meta clearfix">
                <li><i class="icon-calendar2"></i> {{ $blog->created_at->format('d/m/Y H:i:s') }}</li>
                <li><i class="icon-tag"></i> {{ $blog->tags }}</li>
              </ul>

              <div class="entry-content notopmargin">

                <blockquote>
                  {!! $blog->body !!}
                </blockquote>
                <div class="clear"></div>
              </div>

              <div class="entry-image">
                <ul id="lightSlider">
                  @foreach($blog->photos as $photo)
                  <li data-thumb="{{ $photo->url(512) }}">
                    <img src="{{ $photo->url(1024) }}" />
                  </li>
                  @endforeach
                </ul>
              </div>

            </div>

          </div><!-- single-post -->
        </div>
        <!--col-md-12 -->
      </div><!-- row -->
    </div><!-- box-general -->
  </div><!-- container -->
</section>
@include('front.includes.box-blogs')
@include('front.includes.box-categories')
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link type="text/css" rel="stylesheet" href="/front/css/lightslider.css?1" />
@endsection
@section('jsPage')
<script src="/front/js/lightslider.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $("#lightSlider").lightSlider({
    gallery: true,
    item: 1,
    loop: true,
    slideMargin: 0,
    thumbItem: 8,

    onBeforeStart: function(el) {},
    onSliderLoad: function(el) {},
    onBeforeSlide: function(el) {},
    onAfterSlide: function(el) {},
    onBeforeNextSlide: function(el) {},
    onBeforePrevSlide: function(el) {}
  });
});
</script>
@endsection