@extends('front.base')
@section('title', $establishment->name)
@section('metaHead')
<meta property="og:description" content="{{ strip_tags(str_limit($establishment->description, '150')) ?? '' }}" />
<meta property="og:image" content="{{ asset('storage/establishments/'. $establishment->id .'/photos/' . $establishment->image) }}" />
@endsection
@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="/categorias/{{ $establishment->getPostCategorySlug() }}">{{ $establishment->getPostCategory() }}</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<!-- START CONTAINER -->
<section class="section sub-page">
  <div class="container">
    <div class="row">
      <div class="box-general">
        <div class="col-xs-12 col-md-12">

          <div class="clearfix">
            <div class="entry clearfix">

              <div class="row">
                <div class="col-xs-12 col-md-6">
                  <div class="entry-image">
                    @if($establishment->featured == 1)
                    <span class="badge badge-thema">Em Destaque</span>
                    @endif
                    <ul id="lightSlider">
                    <li data-thumb="{{ asset('storage/establishments/'. $establishment->id .'/photos/' . $establishment->image) }}">
                        <img src="{{ asset('storage/establishments/'. $establishment->id .'/photos/' . $establishment->image) }}" />
                      </li>
                      @foreach($establishment->photos as $photo)
                      <li data-thumb="{{ asset('storage/establishments/'. $photo->establishment_id .'/photos/' . $photo->filename) }}">
                        <img src="{{ asset('storage/establishments/'. $photo->establishment_id .'/photos/' . $photo->filename) }}" />
                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div><!-- col -->
                <div class="col-xs-12 col-md-6">
                  @if($establishment->description)
                  <div class="panel panel-default" style="margin:0;">
                    <div class="panel-body">
                      <div class="entry-content notopmargin box-description">
                        <h4>Descrição</h4>
                        {!! $establishment->description !!}
                      </div>
                    </div>
                  </div><!-- entry-content notopmargin -->
                  @endif
                  @if($establishment->promotion)
                  <div class="panel panel-default" style="margin:20px 0 0 0;">
                    <div class="panel-body">
                      <div class="entry-content notopmargin box-promotion">
                        <h4>Promoções Exclusivas</h4>
                        {!! $establishment->promotion !!}
                      </div>
                    </div>
                  </div><!-- entry-content notopmargin -->
                  @endif

                  @if($establishment->redesocial_facebook != '#' || $establishment->redesocial_instagram != '#' || $establishment->redesocial_twitter != '#' || $establishment->redesocial_linkedin != '#')
                  <div class="panel panel-default" style="margin:20px 0 0 0;">
                    <div class="panel-body">
                      <h4>Nossas Redes Sociais</h4>

                      <div class="fleft clearfix">
                        @if($establishment->redesocial_facebook != '')
                        <a href="https://facebook.com/{{ $establishment->redesocial_facebook }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Facebook" class="social-icon si-small si-borderless si-facebook">
                          <i class="icon-facebook"></i>
                          <i class="icon-facebook"></i>
                        </a>
                        @endif

                        @if($establishment->redesocial_instagram != '')
                        <a href="https://instagram.com/{{ $establishment->redesocial_instagram }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Instagram" class="social-icon si-small si-borderless si-instagram">
                          <i class="icon-instagram"></i>
                          <i class="icon-instagram"></i>
                        </a>
                        @endif

                        @if($establishment->redesocial_twitter != '')
                        <a href="https://twitter.com/{{ $establishment->redesocial_twitter }}" data-toggle="tooltip" target="_Blank" data-placement="bottom" data-original-title="Twitter" class="social-icon si-small si-borderless si-twitter">
                          <i class="icon-twitter"></i>
                          <i class="icon-twitter"></i>
                        </a>
                        @endif

                        @if($establishment->redesocial_linkedin != '')
                        <a href="https://linkedin.com/{{ $establishment->redesocial_linkedin }}" data-toggle="tooltip" target="_Blank" data-placement="bottom" data-original-title="linkedin" class="social-icon si-small si-borderless si-linkedin">
                          <i class="icon-linkedin"></i>
                          <i class="icon-linkedin"></i>
                        </a>
                        @endif
                      </div>
                    </div>
                  </div>
                  @endif
                </div><!-- col -->
              </div><!-- row -->

              <hr>

              <div class="row">
                <div class="col-xs-12 col-md-4">
                  <h3>Informações de Contato</h3>
                  <div class="panel panel-default" style="margin:10px 0;">
                    <div class="panel-body">
                      <div class="widget widget-links">
                        @php $retira = ["(", ")", " ", "-"]; @endphp
                        <ul>
                          <li><i class="fa fa-home"></i> Estabelecimento {{ $establishment->type }}</li>
                          @if($establishment->phone_whatsapp == 1)
                          <li><i class="fab fa-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=55{{ str_replace($retira, '', $establishment->phone) }}" target="_Blank">{{ $establishment->phone }}</a></li>
                          @else
                          <li><i class="fa fa-phone"></i> {{ $establishment->phone }}</li>
                          @endif
                          @if($establishment->cellphone)
                          <li><i class="fa fa-phone"></i> {{ $establishment->cellphone }} </li>
                          @endif
                          @if($establishment->email)<li><i class="fa fa-envelope"></i> <a href="mailto:{{ $establishment->email }}">{{ $establishment->email }}</a> </li>@endif
                          @if($establishment->website)
                            @php $verifyWWW = str_limit($establishment->website, $limit = 3, $end = '') @endphp
                          <li><i class="fa fa-globe"></i> <a href="{{ $verifyWWW == 'www' ? 'https://'.$establishment->website : $establishment->website }}" target="_Blank">{{ $verifyWWW == 'www' ? 'https://'.$establishment->website : $establishment->website }}</a> </li>
                          @endif
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div><!-- entry clearfix -->
          </div><!-- col full -->

        </div>
        <!--col-md-12 -->
      </div><!-- box-general -->
    </div><!-- row -->
  </div><!-- container -->
</section>

@include('front.includes.box-categories')

@endsection

@section('cssPage')
<link type="text/css" rel="stylesheet" href="/front/css/lightslider.css?1" />
@endsection
@section('jsPage')
<script src="/front/js/lightslider.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#lightSlider").lightSlider({
      gallery: true,
      item: 1,
      loop: true,
      slideMargin: 0,
      thumbItem: 8,

      onBeforeStart: function(el) {},
      onSliderLoad: function(el) {},
      onBeforeSlide: function(el) {},
      onAfterSlide: function(el) {},
      onBeforeNextSlide: function(el) {},
      onBeforePrevSlide: function(el) {}
    });
  });
</script>


@endsection