@extends('front.base')
@section('title', 'Encontre os Estabelecimentos')

@section('content')
<div class="container text-center mb-5">
  <div class="emphasis-title center">
    <h2>Faça sua Busca</h2>
    <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
  </div>
  <div id="top-search" class="searchTop">
    <form action="{{ url('/estabelecimentos/busca') }}" method="post">
      {{ csrf_field() }}
      <!-- <select id="sdistric" name="sdistric" id="sdistric" class="form-control form-gorup select2"></select> -->
      <input type="text" name="sdistric" id="sdistric" class="form-control" value="" placeholder="bairro ou cidade" style="width:150px;">
      <input type="text" name="all" id="all" class="form-control" value="" placeholder="nome ou categoria do estabelecimento">
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div><!-- #top-search end -->
</div>

@if($sliders->count() > 0)
<section id="slider" class="my-5">
  <div class="container">
    <div class="emphasis-title center">
      <h2>OUTDOOR</h2>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>
    <div class="row">
      @if($sliders->count() == 1)
      <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="1" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
          @elseif($sliders->count() == 2)
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="2" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
              @else
              <div class="col-xs-12 col-sm-12">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="3" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
                  @endif
                  @foreach($sliders as $slider)
                  @if($slider->url)
                  <a href="{{ $slider->url }}" data-toggle="tooltip" data-original-title="{{$slider->title}} - Clique para abrir"><img src="/storage/{{ $slider->image }}" alt="{{ $slider->title}}"></a>
                  @else
                  <a href="javascript:;" style="cursor:w-resize;"><img src="/storage/{{ $slider->image }}" alt="{{ $slider->title}}"></a>
                  @endif
                  @endforeach
                </div>
              </div>
            </div>
          </div>
</section>
@endif

@if($categories->count() > 0)
<div id="section-home-categories" class="page-section">
  <div class="container clearfix">
    <div class="row">
      <div class="emphasis-title center">
        <h2>Conheça os Estabelecimentos por Categorias</h2>
        <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
      </div>
      <div class="col-xs-12 col-md-12 text-center">
        <div class="row">
          @foreach($categories as $category)
          <div class="col-xs-6 col-sm-3 col-lg-2 col-md-3 mb-3 oc-item item-category">
            <a href="/categorias/{{ $category->slug }}">
              <picture><img src="{{ $category->image ? asset('storage/categories/'. $category->id .'/photos/' . $category->image) : asset('galerias/sem_image.png') }}" alt="{{ $category->title }}"></picture>
              <h2>{{ $category->title }}</h2>
            </a>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endsection

@section('jsPage')
@endsection