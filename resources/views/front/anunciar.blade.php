@extends('front.base')
@section('title', '')
@section('metaHead')
<meta name="description" content="{{ $configs->config_description ?? '' }}">
<meta name="keywords" content="{{ $configs->config_keywords ?? '' }}" />
@endsection
@section('headerHome')
<header class="headerHome">
  <div id="header-wrap">
    <div class="container clearfix">
      <div class="d-column d-sm-column d-md-flex">
        <div class="">
          <div id="logo">
            <a href="{{ url('/')}}" class="standard-logo" data-dark-logo="{{ asset('galerias/logo_branco.png') }}"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}"></a>
            <a href="{{ url('/')}}" class="retina-logo" data-dark-logo="{{ asset('galerias/logo_branco.png') }}"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}"></a>
          </div><!-- #logo end -->
        </div>
        <div class="flex-grow-1">
          @include('front.includes.menu-top')
          @yield('search')
        </div>
      </div>
    </div><!-- container -->
  </div><!-- header-wrap -->
  <div id="header-descrip">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-9">
          <h1><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Juntos pelo seu Bairro</span> é uma plataforma que <span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">conecta vendedores e compradores</span> o que gera Impacto Social e Impacto Positivo</h1>
        </div><!-- cols -->
        <div class="col-xs-12 col-md-3">
          @if($configs->redesocial_facebook != '#' || $configs->redesocial_instagram != '#' || $configs->redesocial_twitter != '#' || $configs->redesocial_linkedin != '#')
          <div class="text-center">
            <h4>Siga-nos</h4>

            <div class="clearfix">
              @if($configs->redesocial_facebook != '#')
              <a href="{{ $configs->redesocial_facebook }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Facebook" class="social-icon si-small si-borderless si-facebook">
                <i class="icon-facebook"></i>
                <i class="icon-facebook"></i>
              </a>
              @endif

              @if($configs->redesocial_instagram != '#')
              <a href="{{ $configs->redesocial_instagram }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Instagram" class="social-icon si-small si-borderless si-instagram">
                <i class="icon-instagram"></i>
                <i class="icon-instagram"></i>
              </a>
              @endif

              @if($configs->redesocial_twitter != '#')
              <a href="{{ $configs->redesocial_twitter }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Twitter" class="social-icon si-small si-borderless si-twitter">
                <i class="icon-twitter"></i>
                <i class="icon-twitter"></i>
              </a>
              @endif

              @if($configs->redesocial_linkedin != '#')
              <a href="{{ $configs->redesocial_linkedin }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="linkedin" class="social-icon si-small si-borderless si-linkedin">
                <i class="icon-linkedin"></i>
                <i class="icon-linkedin"></i>
              </a>
              @endif
            </div>
          </div>
          @endif

          <div class="text-center mt-4">
            <h5 class="title-counter">Visualizações no site</h5>
            <i class="i-plain i-xlarge mb-0 mr-4 icon-heart"></i>
            <div class="counter counter-large" style="color: #3ab8bf;">
              <span data-from="5000" data-to="{{ $countAllViews }}" data-refresh-interval="50" data-speed="2000">0000</span>
            </div>
          </div><!-- text-center -->
        </div>
      </div><!-- row -->
    </div><!-- container -->
  </div><!-- header-descrip -->
</header><!-- #header end -->
@endsection

@section('content')
<div class="page-section py-2 m-0">
  <div class="container">
    <div class="row">

      <div id="section-home-direct" class="col-xs-12 col-md-12 col-lg-12 py-0">
        <div class="feature-box fbox-center fbox-home fbox-bg fbox-light fbox-effect mb-5">
          <div class="fbox-icon">
            @if(Auth::guard('customer')->check())
            <a href="{{ route('estabelecimentos.index', Auth::guard('customer')->user()->id) }}"><i class="i-alt">Quero Anunciar</i></a>
            @else
            <a href="{{ url('minha-conta/register') }}"><i class="i-alt">Quero Anunciar</i></a>
            @endif
          </div>
          <p><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Vantagens para você Vendedor:</span></p>
          <ul>
            <li><span class="inside">•</span> Maior alcance para seu negócio: alcance novos consumidores</li>
            <li><span class="inside">•</span> Não pare de vender! Com o suporte da nossa plataforma, você pode realizar suas vendas pelo Whatsapp, Instagram, Facebook ou seu site.</li>
            <li><span class="inside">•</span> Oferecemos o link do seu anúncio para que você use nas suas divulgações via whatsapp ou suas mídias sociais </li>
          </ul>

          <p class="mt-5"><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Outros benefícios:</span></p>
          <ul class="mb-5">
            <li><span class="inside">•</span> Você pode destacar sua empresa na busca do Bairro</li>
            <li><span class="inside">•</span> Você também pode divulgar sua empresa no Outdoor da página principal na nossa plataforma</li>
            <li><span class="inside">•</span> Publicar várias fotos ou vídeos no seu anúncio</li>
            <li><span class="inside">•</span> Cadastrar mais de 1 estabelecimento</li>
            <li><span class="inside">•</span> E muito mais! confira nos Planos e Benefícios</li>
          </ul>

        </div>

        <div id="section-home-plans" class="mb-5">
          <div class="">
            <div class="emphasis-title center">
              <h2>Nossos Planos & Benefícios</h2>
              <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
            </div><!-- emphasis -->

            <div class="plans-box">
              <!-- <div class="d-flex flex-column flex-sm-column flex-md-row flex-lg-row"> -->
              <div class="row">
                @foreach($plans as $plan)
                <!-- <div id="plan-{{ $plan->id }}" class="flex-fill panel panel-default"> -->
                <div id="plan-{{ $plan->id }}" class="col-xs-12 col-sm-12 col-md-3 mb-3">
                  <div class="panel panel-default">
                    <form action="{{ route('estabelecimentos.index') }}" name="plan-{{ $plan->id }}" id="plan-{{ $plan->id }}" class="panel-body">
                      <h2>{{ $plan->name }}</h2>

                      @php
                      $valor = $plan->price;
                      switch ($plan->period) {
                      case 30:
                      $period = 1;
                      break;
                      case 90:
                      $period = 3;
                      break;
                      case 180:
                      $period = 6;
                      break;
                      case 360:
                      $period = 12;
                      break;
                      }
                      $pricemensal = $valor / $period;
                      @endphp

                      <div class="price">
                        <p class="price_plan hide">R$ {{ $plan->price }}</p>
                        @if($plan->price > 0)
                        <p><small>investimento de</small></p>
                        <p class="price_plan">R$ {{ number_format($pricemensal, 2, ',', '') }}/mês</p>
                        @else
                        <p>Grátis</p>
                        @endif
                        <input type="hidden" name="price" id="price" value="{{ $plan->price }}">
                        <input type="hidden" name="plan_id" id="plan_id" value="{{ $plan->id }}">
                        @php
                        if($plan->period == 30){
                        $mesplano = '1 mês';
                        }elseif($plan->period == 90){
                        $mesplano = '3 meses';
                        }elseif($plan->period == 180){
                        $mesplano = '6 meses';
                        }elseif($plan->period == 360){
                        $mesplano = '12 meses';
                        }else{
                        $mesplano = '-';
                        }
                        @endphp

                        <select name="select_period" id="select_period" class="form-control" readonly onclick="someValue({{ $plan->id }})">
                          <option value="{{ $plan->period }}" data-pricemonth="{{ $plan->price }}">{{ $mesplano }}</option>
                        </select>
                        @if($plan->price > 0)
                        <p><small>Investimento total de R$ {{ number_format($plan->price, 2, ',', '') }}</small></p>
                        @endif

                      </div>
                      <div class="info">
                        @if($plan->description)
                        {!! html_entity_decode(str_replace('*', '<i class="fa fa-check-circle text-success mr-2"></i>', $plan->description)) !!}
                        @endif
                      </div>
                      <div class="buttons hide">
                        <button type="submit" class="btn btn-thema"><i class="fa fa-bullhorn"></i> Anuncie já!</button>
                      </div>
                    </form>
                  </div><!-- panels -->
                </div><!-- cols -->
                @endforeach
              </div>
            </div>



            <div class="text-center">
              @if(Auth::guard('customer')->check())
              <a href="{{ route('estabelecimentos.index', Auth::guard('customer')->user()->id) }}" class="btn btn-lg btn-thema mt-2">Clique aqui para Anunciar</a>
              @else
              <a href="{{ url('minha-conta/register') }}" class="btn btn-lg btn-thema mt-2">Clique aqui para Anunciar</a>
              @endif
            </div>

          </div>
        </div>

        <hr class="invisible">

        @if($categoriesHome->count() > 0)
        <div id="section-home-categories" class="nobg noshadowbox mb-5 mt-5">
          <div class="">
            <div class="emphasis-title center">
              <h2>Conheça algumas Categorias</h2>
              <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
            </div>
            <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="40" data-loop="true" data-nav="true" data-pagi="false" data-items-xxs="2" data-items-xs="3" data-items-sm="6" data-items-md="7" data-items-lg="7">
              @foreach($categoriesHome as $categoryHome)
              <div class="oc-item item-category">
                <a href="/categorias/{{ $categoryHome->slug }}">
                  <picture><img src="{{ $categoryHome->image ? asset('storage/categories/'. $categoryHome->id .'/photos/' . $categoryHome->image) : asset('galerias/sem_image.png') }}" class="img-rounded" alt="{{ $categoryHome->title }}"></picture>
                  <h2>{{ $categoryHome->title }}</h2>
                </a>
              </div>
              @endforeach
            </div>
            <p>Se não encontrar a categoria do seu negócio, entre em contato por e-mail <a href="{{ url('/contato') }}">clicando aqui</a>.</p>
            <a href="{{ url('categorias/')}}">ver todas categorias</a>
          </div>
        </div>
        @endif

      </div><!-- col -->
    </div><!-- row -->
  </div><!-- container -->
</div><!-- page -->


<!-- END CONTAINER -->
@endsection