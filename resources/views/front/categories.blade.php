@extends('front.base')
@section('title', 'Categorias')

@section('breadcrumb')
<div id="section-categorias" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<!-- START CONTAINER -->
@if($categories->count() > 0)
<div id="section-home-categories" class="page-section">
  <div class="container clearfix">
    <div class="row">
      <div class="col-xs-12 col-md-12 text-center">
        <div class="row">
          @foreach($categories as $category)
          <div class="col-xs-6 col-sm-3 col-lg-2 col-md-3 mb-3 oc-item item-category">
            <a href="/categorias/{{ $category->slug }}">
              <picture><img src="{{ $category->image ? asset('storage/categories/'. $category->id .'/photos/' . $category->image) : asset('galerias/sem_image.png') }}" alt="{{ $category->title }}"></picture>
              <h2>{{ $category->title }}</h2>
            </a>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endif
<!-- END CONTAINER -->
@endsection