@extends('front.base')
@section('title', 'Resultados de '. $keysearch)


@section('content')


@if(isset($featuredestablishments))
@if(count($featuredestablishments) > 0)
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>Destaques por {{$keysearch}}</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>
  </div>
</div><!-- section-home -->

<div id="section-home-establishments" class="page-section">
  <div class="container clearfix">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        @foreach($featuredestablishments as $featuredestablishment)
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
          <div class="testimonial">
            <div class="testi-image">
              <a href="/estabelecimento/{{ $featuredestablishment->estabelecimentoslug }}">
                <img src="{{ $featuredestablishment->image ? asset('storage/establishments/'. $featuredestablishment->establishment_id .'/photos/thumb_' . $featuredestablishment->image) : asset('galerias/sem_image.png') }}" alt="{{ $featuredestablishment->estabelecimento }}">
              </a>
              <span class="label label-thema">Destaque</span>
            </div>
            <div class="testi-content d-flex align-items-center justify-content-center">
              <div class="testi-meta">
                <a href="/estabelecimento/{{ $featuredestablishment->estabelecimentoslug }}">
                  <h2>{{ $featuredestablishment->estabelecimento }}</h2>
                </a>
                <p>{{ $featuredestablishment->categoria }}</p>
                <div class="tags mt-3">
                  <small> <i class="fa fa-map-marker-alt"></i> {{ $featuredestablishment->distric }} - {{ $featuredestablishment->city }} <br> {{ $featuredestablishment->state }}</small>
                </div>
              </div>
            </div>
          </div>
        </div><!-- col-md-4 -->
        @endforeach
      </div>
    </div>
  </div>
</div>
@endif
@endif


<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->

<div id="section-home-establishments" class="page-section">
  <div class="container clearfix">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        @if(count($searchestablishments) > 0)
        @foreach($searchestablishments as $searchestablishment)
        @if($searchestablishment->plan_dateend >= \Carbon\Carbon::now())
        @if($searchestablishment->upgrade == 0)
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
          <div class="testimonial">
            <div class="testi-image">
              <a href="/estabelecimento/{{ $searchestablishment->estabelecimentoslug }}">
                <img src="{{ $searchestablishment->image ? asset('storage/establishments/'. $searchestablishment->establishment_id .'/photos/thumb_' . $searchestablishment->image) : asset('galerias/sem_image.png') }}" alt="{{ $searchestablishment->estabelecimento }}">
              </a>
              @if($searchestablishment->featured == 1)
              <span class="label label-thema">Destaque</span>
              @endif
            </div>
            <div class="testi-content d-flex align-items-center justify-content-center">
              <div class="testi-meta">
                <a href="/estabelecimento/{{ $searchestablishment->estabelecimentoslug }}">
                  <h2>{{ $searchestablishment->estabelecimento }}</h2>
                </a>
                <p>{{ $searchestablishment->categoria }}</p>
                <div class="tags mt-3">
                  <small> <i class="fa fa-map-marker-alt"></i> {{ $searchestablishment->distric }} - {{ $searchestablishment->city }} <br> {{ $searchestablishment->state }}</small>
                </div>
              </div>
            </div>
          </div>
        </div><!-- col-md-4 -->
        @endif
        @else
        @endif
        @endforeach
        @endif

        @if(count($searchestablishments) > 0)
        @else
        <hr>
        <div class="alert alert-info">
          @if(count($searchestablishments) > 0)
          @else
          Não foi encontrado resultados em <b>Estabelecimentos</b>. <br>
          @endif
        </div>
        @endif

      </div>
    </div>
  </div>
</div>
<!-- END CONTAINER -->
@include('front.includes.box-categories')
@endsection