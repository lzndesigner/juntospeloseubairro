@extends('front.base')
@section('title', 'Fale com a gente')

@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<section class="section sub-page" id="oc-contact">
  <div class="container">
    <div class="box-general">
      <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
          <div class="contact-widget">
            <div class="contact-form-result"></div>
            <form-contato></form-contato>
          </div><!-- contact-widget -->
        </div><!-- col-xs-12 col-md-7 -->

        <hr class="invisible clearfix">

        @if(!empty($configs->config_phone) && !empty($configs->config_cellphone))
        <div class="col-xs-12 col-sm-6 col-md-6 bottommargin clearfix">
          <div class="feature-box fbox-center fbox-bg fbox-plain">
            <div class="fbox-icon">
              <a href="#"><i class="icon-phone3"></i></a>
            </div>
            <h3>Telefone
              <span class="subtitle">{{$configs->config_phone}}

                @if(!empty($configs->config_cellphone))
                / {{$configs->config_cellphone}}
              </span>
              @endif</h3>
          </div>
        </div>
        @endif
        @if(!empty($configs->config_phone) && !empty($configs->config_cellphone))
        <div class="col-xs-12 col-sm-6 col-md-6 bottommargin clearfix">
        @else
          <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-2 bottommargin clearfix">
            @endif
            <div class="feature-box fbox-center fbox-bg fbox-plain">
              <div class="fbox-icon">
                <a href="#"><i class="icon-envelope"></i></a>
              </div>
              <h3>E-mail<span class="subtitle" style="font-size:24px;"><a href="mailto:{{$configs->config_email}}">{{$configs->config_email}}</a></span></h3>
            </div>
          </div>

        </div><!-- row -->
      </div><!-- box-general -->

    </div><!-- container -->
</section>

<!-- END CONTAINER -->
@endsection