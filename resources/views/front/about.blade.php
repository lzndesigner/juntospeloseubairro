@extends('front.base')
@section('title', 'Sobre Nós')

@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div id="section-home-establishments" class="page-section">
  <div class="container clearfix">
    <div class="row">
      <div class="col-xs-12 col-md-12">
        {!! html_entity_decode($abouts->page_description) !!}
      </div>
    </div>
  </div>
</div>
<!-- END CONTAINER -->
@include('front.includes.box-categories')
@endsection