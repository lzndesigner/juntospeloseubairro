@extends('front.base')
@section('title', $calendar->name)
@section('breadcrumb')
<div id="section-header" class="section parallax nomargin notopborder dark skrollable skrollable-between" style="background-image: url('/galerias/bg_section_home_2.jpg?3'); padding: 50px 0px 250px; background-position: 0px 31.5574px;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/calendario') }}">Calendário</a></li>
      <li class="active">@yield('title')</li>
    </ol>

  </div>
  @include('front.includes.box-search')
</div><!-- section-home -->
@endsection

@section('content')
<!-- START CONTAINER -->
<section class="section sub-page">
  <div class="container">
    <div class="row">
      <div class="box-general">
        <div class="col-xs-12 col-md-12">

          <div class="col_full bottommargin-lg clearfix">
            <div class="postcontent nobottommargin clearfix">
              <div class="single-post nobottommargin">
                <div class="entry clearfix">
                  <div class="row">
                    <div class="col-xs-12 col-md-8">

                      <div class="entry-title">
                        <h2>{{ $calendar->name }}</h2>
                      </div><!-- entry-title -->

                      <ul class="entry-meta clearfix">
                        <li><i class="icon-calendar3"></i> Publicado em {{ $calendar->created_at }}</li>
                        <li><i class="icon-eye"></i> Visualizações: {{ $calendar->views }}</li>
                      </ul>

                      <div class="entry-image">
                        <ul id="lightSlider">
                          @foreach($calendar->photos as $photo)
                          <li data-thumb="{{ asset('storage/' . $photo->filename) }}">
                            <img src="{{ asset('storage/' . $photo->filename) }}" />
                          </li>
                          @endforeach
                        </ul>
                      </div>
                      @if($calendar->description)
                      <blockquote>
                        {!! $calendar->description !!}
                      </blockquote>
                      @endif
                    </div><!-- col-md-12 -->
                    <div class="col-xs-12 col-md-4 mb-20">
                      <div class="widget widget_links" style="margin:10px 0;">
                        <h4>Informações:</h4>
                        <ul class="infoCalendars">                        
                          <li><i class="fa fa-calendar"></i> Data: {{ $calendar->day }}/{{ $calendar->month }}</li>
                          <li><i class="fa fa-clock"></i> Horário: {{ $calendar->hour }}</li>
                          <li><i class="fa fa-heart"></i> Classificação: {{ $calendar->classification }} @if($calendar->classification != 'livre') anos @endif</li>
                          <li><i class="fa fa-map-marker"></i> Local: {{ $calendar->local }}</li>
                        </ul>
                      </div>

                      <div class="widget widget_links">
                        <h4>Mapa</h4>
                        @php
                        $queryMaps = str_replace('.', '', $calendar->local);
                        $queryMaps = str_replace('-', '', $queryMaps);
                        $queryMaps = str_replace(',', '', $queryMaps);
                        $queryMaps = str_replace(':', '', $queryMaps);
                        $queryMaps = str_replace('  ', '+', $queryMaps);
                        $queryMaps = str_replace(' ', '+', $queryMaps);
                        @endphp
                        <iframe
                        width="400"
                        height="350"
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBKrjwCIuK892A4JTgfxzHN371ugnAZka4&q={{ $queryMaps }}&zoom=15" allowfullscreen>
                      </iframe>
                    </div>

                    <div class="widget widget_links" style="margin:10px 0;">
                      <h4>Comentários</h4>                                     
                      <div id="fb-root"></div>
                      <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v5.0&appId=1094654680570525&autoLogAppEvents=1"></script>
                      <div class="fb-comments" data-href="{{Request::url()}}" data-width="350" data-numposts="8"></div>
                    </div> 


                  </div>
                </div><!-- row -->   

              </div><!-- entry clearfix -->
            </div><!-- single-post nobottommargin -->
          </div><!-- postcontent nobottommargin clearfix -->
        </div><!-- col full -->

      </div><!--col-md-12 -->


    </div><!-- box-general -->
  </div><!-- row -->
</div><!-- container -->
</section>

@include('front.includes.box-categories')

<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link type="text/css" rel="stylesheet" href="/front/css/lightslider.css?1" />
@endsection
@section('jsPage')
<script src="/front/js/lightslider.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $("#lightSlider").lightSlider({
   gallery: true,
   item: 1,
   loop: true,
   slideMargin: 0,
   thumbItem: 8,

   onBeforeStart: function (el) {},
   onSliderLoad: function (el) {},
   onBeforeSlide: function (el) {},
   onAfterSlide: function (el) {},
   onBeforeNextSlide: function (el) {},
   onBeforePrevSlide: function (el) {}
 });
});
</script>
@endsection