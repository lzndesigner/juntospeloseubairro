@extends('front.base')
@section('title', 'Benefícios & Planos')

@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<section class="section sub-page">
  <div class="container">
    <div class="box-general">
      <div class="row">
        <div class="">
          <div class="plans-box">
            <!-- <div class="d-flex flex-column flex-sm-column flex-md-row flex-lg-row"> -->
            <div class="row">
              @foreach($plans as $plan)
              <!-- <div id="plan-{{ $plan->id }}" class="flex-fill panel panel-default"> -->
              <div id="plan-{{ $plan->id }}" class="col-xs-12 col-sm-12 col-md-4 mb-3">
                <div class="panel panel-default">
                  <form action="{{ route('estabelecimentos.index') }}" name="plan-{{ $plan->id }}" id="plan-{{ $plan->id }}" class="panel-body">
                    <h2>{{ $plan->name }}</h2>

                    @php
                    $valor = $plan->price;
                    switch ($plan->period) {
                    case 30:
                    $period = 1;
                    break;
                    case 90:
                    $period = 3;
                    break;
                    case 180:
                    $period = 6;
                    break;
                    case 360:
                    $period = 12;
                    break;
                    }
                    $pricemensal = $valor / $period;
                    @endphp

                    <div class="price">
                      <p class="price_plan hide">R$ {{ $plan->price }}</p>
                      @if($plan->price > 0)
                      <p><small>investimento de</small></p>
                      <p class="price_plan">R$ {{ number_format($pricemensal, 2, ',', '') }}/mês</p>
                      @else
                      <p>Grátis</p>
                      @endif
                      <input type="hidden" name="price" id="price" value="{{ $plan->price }}">
                      <input type="hidden" name="plan_id" id="plan_id" value="{{ $plan->id }}">
                      @php
                      if($plan->period == 30){
                      $mesplano = '1 mês';
                      }elseif($plan->period == 90){
                      $mesplano = '3 meses';
                      }elseif($plan->period == 180){
                      $mesplano = '6 meses';
                      }elseif($plan->period == 360){
                      $mesplano = '12 meses';
                      }else{
                      $mesplano = '-';
                      }
                      @endphp

                      <select name="select_period" id="select_period" class="form-control" readonly onclick="someValue({{ $plan->id }})">
                        <option value="{{ $plan->period }}" data-pricemonth="{{ $plan->price }}">{{ $mesplano }}</option>
                      </select>
                      @if($plan->price > 0)
                      <p><small>Investimento total de R$ {{ number_format($plan->price, 2, ',', '') }}</small></p>
                      @endif

                    </div>
                    <div class="info">
                      @if($plan->description)
                      {!! html_entity_decode(str_replace('*', '<i class="fa fa-check-circle text-success mr-2"></i>', $plan->description)) !!}
                      @endif
                    </div>
                    <div class="buttons hide">
                      <button type="submit" class="btn btn-thema"><i class="fa fa-bullhorn"></i> Anuncie já!</button>
                    </div>
                  </form>
                </div><!-- panels -->
              </div><!-- cols -->
              @endforeach

              <div class="col-xs-12 col-sm-12 col-md-8">
                <h4 class="mb-5">Quer que seu negócio fique em evidência? <br> Temos opções para você:</h4>
                <div class="row">
                  @foreach($upgrades as $upgrade)
                  <div id="plan-{{ $upgrade->id }}" class="col-md-6">
                    <div class="panel panel-default">
                    <form action="{{ route('estabelecimentos.index') }}" name="plan-{{ $upgrade->id }}" id="plan-{{ $upgrade->id }}" class="panel-body">
                      <h2>{{ $upgrade->name }}</h2>

                      @php
                      $valor = $upgrade->price;
                      switch ($upgrade->period) {
                      case 30:
                      $period = 1;
                      break;
                      case 90:
                      $period = 3;
                      break;
                      case 180:
                      $period = 6;
                      break;
                      case 360:
                      $period = 12;
                      break;
                      }
                      $pricemensal = $valor / $period;
                      @endphp

                      <div class="price">
                        <p class="price_plan hide">R$ {{ $upgrade->price }}</p>
                        @if($upgrade->price > 0)
                        <p class="price_plan">R$ {{ number_format($pricemensal, 2, ',', '') }}/mês</p>
                        @else
                        <p>Grátis</p>
                        @endif
                        <input type="hidden" name="price" id="price" value="{{ $upgrade->price }}">
                        <input type="hidden" name="plan_id" id="plan_id" value="{{ $upgrade->id }}">

                        <select name="select_period" id="select_period" class="form-control" disabled onclick="someValue({{ $upgrade->id }})">
                          <option value="{{ $upgrade->period }}" data-pricemonth="{{ $upgrade->price }}">{{ $upgrade->period }} dias</option>
                        </select>
                      </div>
                      <div class="info">
                        @if($upgrade->description)
                        {!! html_entity_decode(str_replace('*', '<i class="fa fa-check-circle text-success mr-2"></i>', $upgrade->description)) !!}
                        @endif
                      </div>
                    </form>
                  </div>
                  </div>
                  @endforeach
                </div>
              </div><!-- col-9 -->


            </div>
          </div>
        </div>
      </div><!-- row -->
    </div><!-- box-general -->
  </div><!-- container -->
</section>

<div id="section-home-welcome" class="page-section">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-12 text-center">
        <h1>Anuncie agora mesmo <br> seu Estabelecimento</h1>
        <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
        @if(Auth::guard('customer')->check())
        <a href="{{ route('estabelecimentos.index', Auth::guard('customer')->user()->id) }}" class="btn btn-lg btn-thema mt-4"><i class="fa fa-check"></i> Começar agora!</a>
        @else
        <a href="{{ url('/minha-conta/register') }}" class="btn btn-lg btn-thema mt-4"><i class="fa fa-check"></i> Começar agora!</a>
        @endif
      </div>
    </div><!-- row -->
    <div class="text-center my-5">
      <h5><b>Dúvidas?</b> Entre em contato com nosso <a href="mailto:{{ $configs->config_email }}">{{ $configs->config_email }}</a></h5>
    </div>
  </div><!-- box-general -->
</div><!-- container -->
</section>
@endsection



@section('jsPage')
<script>
  /*
// Function de Soma de valores das opções de acordo quando é marcado
function someValue(idplan) {
// Criar a var que armazena o valor do produto original e retira cifrao e pontuacao
var priceProduct = Number($('#plan-'+ idplan +' .price .price_plan').text().replace("R$ ", "").replace(",", "."));

// Quando um item foi click aciona a funcao
$('#plan-'+ idplan +' .price select#select_period').click(function() {
  var target = $('#plan-'+ idplan +' .price select#select_period').children('option:selected');

  // Quando um input for marcado faz a soma do valor do produto
  var total = $('#plan-'+ idplan +' .price select#select_period option:selected').get().reduce(function(tot, el) {
    if(el.dataset.pricemonth){
      return Number(el.dataset.pricemonth);
    }else{
      return tot;
    }
  }, priceProduct);

  // Formata o valor da soma e insere em uma var
  var totalFormated = total.toLocaleString('pt-BR', { style: 'currency' , currency:'BRL'});

  // Aplica no HTML o novo valor
  $('#plan-'+ idplan +' .price input[name="price"]').val(total);
  $('#plan-'+ idplan +' .price .price_plan').text(totalFormated);

});   
} 

*/
</script>
@endsection