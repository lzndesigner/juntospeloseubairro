<div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>

<div id="section-category-home" style="padding:20px 0;">
  <div class="container clearfix">
    <div class="box-category-home"  style="width:100%; margin:0;">
      <h3>Outras postagens</h3>
      <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="20" data-loop="true" data-nav="true" data-pagi="true" data-items-xxs="1" data-items-xs="2" data-items-sm="4" data-items-md="4" data-items-lg="4">

        @if($blogs->count() > 0)
        @foreach($blogs as $blog)
        <div class="oc-item item-category">
          <a href="/blogs/{{ $blog->slug }}">
            <picture><img src="{{ $blog->photos->isNotEmpty() ? $blog->photos[0]->url(256) : asset('storage/sem_image.png') }}" alt="{{ $blog->title }}"></picture>
            <h2>{{ $blog->title }}</h2>
          </a>
        </div>
        @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
<!-- END CONTAINER -->
