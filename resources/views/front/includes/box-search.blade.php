<div id="top-search" class="searchTop">
	<form action="{{ url('/estabelecimentos/busca') }}" method="post">
		{{ csrf_field() }}
		<!-- <select id="sdistric" name="sdistric" id="sdistric" class="form-control form-gorup select2"></select> -->
		<div class="d-flex  flex-column flex-sm-column flex-md-row search-content">
			<div class="">
				<input type="text" name="sdistric" id="sdistric" class="form-control input-sdistric" value="" placeholder="bairro ou cidade">
			</div>
			<div class="flex-grow-1">
				<input type="text" name="all" id="all" class="form-control input-all" value="" placeholder="estabelecimento, categoria, produto ou serviço">
				<button type="submit"><i class="fa fa-search"></i></button>
			</div>
		</div><!-- d-flex -->
		<div class="d-flex">
			<div>
				<div class="form-group">
					<div class="checkbox">
						<label>
						<input type="checkbox" name="description" value="1" class="mt-1" checked> Buscar nas descrições dos estabelecimentos?
						</label>
					</div>
				</div>
			</div>
		</div>
	</form>
</div><!-- #top-search end -->