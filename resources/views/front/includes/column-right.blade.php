<div class="widget widget_links clearfix">
    <h4>Categorias</h4>
    <div class="nobottommargin">
        <ul>
            @foreach($categories as $category)
            <li><a href="/categorias/{{ $category->slug }}">{{ $category->title }}</a></li>
            @endforeach
        </ul>
    </div>
</div><!-- widget -->


<div class="widget clearfix">
    <div class="tabs nobottommargin clearfix ui-tabs ui-corner-all ui-widget ui-widget-content" id="sidebar-tabs">
        <ul class="tab-nav clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
            <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Estabelecimentos</a></li>
            <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Calendário</a></li>
        </ul>
        <div class="tab-container">
            <div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                <div id="popular-post-list-sidebar">

                    @foreach($establishmentsColumns as $establishmentsColumn)
                    <div class="spost clearfix">
                        <div class="entry-image">
                            <a href="/categorias/{{ $establishmentsColumn->getPostCategorySlug() }}/{{ $establishmentsColumn->slug }}" class="nobg"><img class="rounded-circle" src="{{ $establishmentsColumn->photos->isNotEmpty() ? asset('storage/' . $establishmentsColumn->photos[0]->filename) : '' }}" alt="{{ $establishmentsColumn->name }}"></a>
                        </div>
                        <div class="entry-c">
                            <div class="entry-title">
                                <h4><a href="/categorias/{{ $establishmentsColumn->getPostCategorySlug() }}/{{ $establishmentsColumn->slug }}">{{ $establishmentsColumn->name }}</a></h4>
                            </div>
                            <ul class="entry-meta">
                                <li>Categoria: <span class="badge badge-primary">{{ $establishmentsColumn->getPostCategory() }}</span></li>
                            </ul>
                        </div>
                    </div><!-- sposts -->
                    @endforeach

                </div>
            </div><!-- ui 1 -->
            <div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                <div id="recent-post-list-sidebar">

                    @foreach($calendarsColumns as $calendarsColumn)
                    <div class="spost clearfix">
                        <div class="entry-image">
                            <a href="/calendario/{{ $calendarsColumn->slug }}" class="nobg"><img class="rounded-circle" src="{{ $calendarsColumn->photos->isNotEmpty() ? asset('storage/' . $calendarsColumn->photos[0]->filename) : '' }}" alt="{{ $calendarsColumn->name }}" alt="{{ $calendarsColumn->name }}"></a>
                        </div>
                        <div class="entry-c">
                            <div class="entry-title">
                                <h4><a href="/calendario/{{ $calendarsColumn->slug }}">{{ $calendarsColumn->name }}</a></h4>
                            </div>
                            <ul class="entry-meta">
                                <li><i class="icon-calendar3"></i>{{ $calendarsColumn->day }}/{{ $calendarsColumn->month }} às {{ $calendarsColumn->hour }}</li>
                            </ul>
                        </div>
                    </div><!-- sposts -->
                    @endforeach

                </div>
            </div><!-- ui 2 -->

        </div>
    </div>
</div><!-- widget -->

<hr class="clear">