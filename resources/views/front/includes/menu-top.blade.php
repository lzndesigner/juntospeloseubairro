<nav id="primary-menu" class="style-2">
	<div id="primary-menu-trigger">MENU <i class="icon-reorder"></i></div>
	<ul class="">
		<li class="@if(collect(request()->segments())->last() == '') current @endif"><a href="{{ url('/') }}">
				<div>Início</div>
			</a></li>

		<li class="@if(collect(request()->segments())->last() == 'sobre') current @endif"><a href="{{ url('/sobre') }}">
				<div>Sobre Nós</div>
			</a>
		</li>

		@if($categories->count() > 0)
		<li class="mega-menu"><a href="#">
				<div>Estabelecimentos <i class="fa fa-angle-down"></i></div>
			</a>
			<div class="mega-menu-content style-2 clearfix">
				@foreach($categories->chunk(8) as $chunk)
				<ul class="mega-menu-column col-md-3">
					<li class="mega-menu-title">
						<ul>
							@foreach($chunk as $category)
							<li><a href="/categorias/{{ $category->slug }}">
									<div>{{ $category->title }}</div>
								</a></li>
							@endforeach
						</ul>
					</li>
				</ul>
				@endforeach
			</div><!-- end menu-mega -->
		</li>
		@endif

		<li class="currentAnunciar sub-menu @if(collect(request()->segments())->last() == 'anunciar' || collect(request()->segments())->last() == 'planos' || collect(request()->segments())->last() == 'como-funcionamos') current @endif">
			<a href="javascript:;">
				<div>Quero Anunciar <i class="fa fa-angle-down"></i></div>
			</a>
			<ul>
				<li class="@if(collect(request()->segments())->last() == 'anunciar') current @endif"><a href="{{ url('/anunciar') }}">
						<div>Comece já</div>
					</a>
				</li>
				<li class="@if(collect(request()->segments())->last() == 'como-funcionamos') current @endif"><a href="{{ url('/pagina/como-funcionamos') }}">
						<div>Como funcionamos</div>
					</a>
				</li>
				<li class="@if(collect(request()->segments())->last() == 'planos') current @endif">
					<a href="{{ url('/planos') }}">
						<div>Benefícios & Planos</div>
					</a>
				</li>
			</ul>
		</li>




		<li class="account">
			@if(Auth::guard('customer')->check())
		<li class="sub-menu">
			<a href="javascript:;">
				<div>Minha Conta <i class="fa fa-angle-down"></i></div>
			</a>
			<ul>
				<li class="@if(collect(request()->segments())->last() == 'minha-conta') current @endif"><a href="{{ route('customer.dashboard') }}">Página Inicial</a></li>
				<li class="@if(collect(request()->segments())->last() == 'estabelecimentos') current @endif"><a href="{{ route('estabelecimentos.index', Auth::guard('customer')->user()->id) }}">Meus Estabelecimentos</a></li>
				<li class="@if(collect(request()->segments())->last() == 'estatisticas') current @endif"><a href="{{ route('statistics', Auth::guard('customer')->user()->id) }}">Minhas Estatísticas</a></li>
				<li class="@if(collect(request()->segments())->last() == 'transacoes') current @endif"><a href="{{ route('customer.transation', Auth::guard('customer')->user()->id) }}">Minhas Transações</a></li>
				<li class="divider"></li>
				<li class="@if(collect(request()->segments())->last() == 'edit') current @endif"><a href="{{ route('editar.edit', Auth::guard('customer')->user()->id) }}">Editar Perfil</a></li>
				<li><a href="{{ route('customer.auth.logout') }}">Sair da Conta</a></li>
			</ul>
		</li>
		@else
		<a href="{{ url('/minha-conta/login') }}">
			<div>Minha Conta</div>
		</a>
		@endauth
		</li>

	</ul>
</nav><!-- #primary-menu end -->