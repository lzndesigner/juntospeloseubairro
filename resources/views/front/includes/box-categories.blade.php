<div class="container text-center mb-5">
  <div class="emphasis-title center">
    <h2>Faça sua Busca</h2>
    <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
  </div>
  <div id="top-search" class="searchTop">
	<form action="{{ url('/estabelecimentos/busca') }}" method="post">
		{{ csrf_field() }}
		<!-- <select id="sdistric" name="sdistric" id="sdistric" class="form-control form-gorup select2"></select> -->
		<div class="d-flex search-content">
			<div class="">
				<input type="text" name="sdistric" id="sdistric" class="form-control" value="" placeholder="bairro ou cidade" style="width:150px;">
			</div>
			<div class="flex-grow-1">
				<input type="text" name="all" id="all" class="form-control" value="" placeholder="estabelecimento, categoria, produto ou serviço">
			</div>
			<div class="">
				<button type="submit"><i class="fa fa-search"></i></button>
			</div>
		</div><!-- d-flex -->
		<div class="d-flex">
			<div>
				<div class="form-group">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="description" value="1" class="mt-1" checked> Buscar nas descrições dos estabelecimentos?
						</label>
					</div>
				</div>
			</div>
		</div>
	</form>
</div><!-- #top-search end -->
</div>

@if($categories->count() > 0)
<div id="section-home-categories" class="page-section">
  <div class="container clearfix">
    <div class="row">
      <div class="emphasis-title center">
        <h2>Conheça Empresas pelas Categorias</h2>
        <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
      </div>
      <div class="col-xs-12 col-md-12 text-center">
        <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="40" data-loop="true" data-nav="true" data-pagi="true" data-items-xxs="1" data-items-xs="2" data-items-sm="7" data-items-md="7" data-items-lg="7">
          @foreach($categories as $category)
          <div class="oc-item item-category">
            <a href="/categorias/{{ $category->slug }}">
              <picture><img src="{{ $category->image ? asset('storage/categories/'. $category->id .'/photos/' . $category->image) : asset('galerias/sem_image.png') }}" alt="{{ $category->title }}"></picture>
              <h2>{{ $category->title }}</h2>
            </a>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endif