@extends('front.base')
@section('title', $category->title)

@section('breadcrumb')
<div id="section-categorias" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/categorias') }}">Categorias</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<!-- START CONTAINER -->
<section id="section-home-establishments" class="section sub-page">
  <div class="container">
    <div class="box-general">
      <div class="row">
        @if(count($categoryestablishments) > 0)
        @foreach($categoryestablishments as $categoryestablishment)
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
        <div class="testimonial">
          <div class="testi-image">
            <a href="/estabelecimento/{{ $categoryestablishment->estabelecimentoslug }}">
              <img src="{{ $categoryestablishment->image ? asset('storage/establishments/'. $categoryestablishment->establishment_id .'/photos/thumb_' . $categoryestablishment->image) : asset('galerias/sem_image.png') }}" alt="{{ $categoryestablishment->estabelecimento }}">
            </a>
            @if($categoryestablishment->featured == 1)
            <span class="label label-thema">Destaque</span>
            @endif
          </div>
          <div class="testi-content d-flex align-items-center justify-content-center">
            <div class="testi-meta">
              <a href="/estabelecimento/{{ $categoryestablishment->estabelecimentoslug }}">
                <h2>{{ $categoryestablishment->estabelecimento }}</h2>
              </a>
              <p>{{ $categoryestablishment->categoria }}</p>
              <div class="tags mt-3">
                <small> <i class="fa fa-map-marker-alt"></i> {{ $categoryestablishment->distric }} - {{ $categoryestablishment->city }} <br> {{ $categoryestablishment->state }}</small>
              </div>
            </div>
          </div>
        </div>
      </div><!-- col-md-4 -->
        @endforeach
        @else
        <div class="alert alert-info">
          Não há estabelecimentos cadastrados nesta categoria.
        </div>
        @endif
      </div><!-- row -->
    </div><!-- box-general -->

  </div><!-- container -->
</section>
<!-- END CONTAINER -->

@include('front.includes.box-categories')

@endsection