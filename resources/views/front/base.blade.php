<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  @php
  $titleSubpage = null;
  if(trim($__env->yieldContent('title'))){
  $titleSubpage = trim($__env->yieldContent('title'));
  }
  @endphp
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175376641-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-175376641-1');
  </script>
  <title>{{ $titleSubpage ? $titleSubpage.' -' : '' }} {{ $configs->config_title ?? 'Catalogo Base' }}</title>
  <base href="{{Request::url()}}" />
  <link rel="canonical" href="{{Request::url()}}" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="follow" />
  <meta name="googlebot" content="index, follow, all" />
  <meta name="language" content="pt-br" />
  <meta name="revisit-after" content="1 days">
  <meta name="rating" content="general" />
  <meta property="og:title" content="{{ $titleSubpage ? $titleSubpage.' -' : '' }} {{ $configs->config_title ?? 'Catalogo Base' }}" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="{{Request::url()}}" />
  @if (trim($__env->yieldContent('metaHead')))
  @yield('metaHead')
  @else
  <meta property="og:description" content="{{ $configs->config_description ?? '' }}" />
  <meta property="og:image" content="/galerias/facebook2.png" />
  @endif
  <meta property="og:locale" content="pt_BR" />
  <meta property="og:type" content="website" />

  <link rel="shortcut icon" href="{{ asset('galerias/favicon.ico?5') }}" />
  @php
  header('Set-Cookie: cross-site-cookie=name; SameSite=None; Secure');
  @endphp
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Stylesheets
  ============================================= -->
  <link href="https://fonts.googleapis.com/css?family=Poppins|Lato:300,400,400&display=swap|italic,600,700&display=swap|Raleway:300,400,500,600,700&display=swap|Crete+Round:400italic&display=swap" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ asset('front/css/app.css?131') }}" type="text/css" />
  <!--[if lt IE 9]>
  <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->

  @yield('cssPage')
</head>

<body class="stretched no-transition">
  <div id="app">
    <!-- Document Wrapper
  ============================================= -->
    <div id="wrapper" class="clearfix">

      <div class="">

        <div class="boxed">
          <!-- Header
        ============================================= -->
          @if (trim($__env->yieldContent('headerHome')))
          @yield('headerHome')
          @else
          <header>
            <div id="header-wrap">
              <div class="container clearfix">
                <div class="d-column d-sm-column d-md-flex">
                  <div class="">
                    <div id="logo">
                      <a href="{{ url('/')}}" class="standard-logo" data-dark-logo="{{ asset('galerias/logo_branco.png') }}"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}"></a>
                      <a href="{{ url('/')}}" class="retina-logo" data-dark-logo="{{ asset('galerias/logo_branco.png') }}"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}"></a>
                    </div><!-- #logo end -->
                  </div>
                  <div class="flex-grow-1">
                    @include('front.includes.menu-top')
                    @yield('search')
                  </div>
                </div>
              </div><!-- container -->
            </div><!-- header-wrap -->
          </header><!-- #header end -->
          @endif

          <!-- Content
  ============================================= -->
          <section id="content">
            <div class="content-wrap">
              @yield('breadcrumb')

              @yield('content')
            </div><!-- .content-wrap -->
          </section><!-- #content -->
          <!-- End Content -->

          @if(!empty($configs->config_phone))
          @php $retira = ["(", ")", " ", "-"]; @endphp
          <a href="https://api.whatsapp.com/send?phone=55{{ str_replace($retira, '', $configs->config_phone) }}" target="_Blank" class="box-whatsapp"><i class="fab fa-whatsapp"></i> <span>fale conosco</span></a>
          @endif

          <!-- Footer
  ============================================= -->
          <footer id="footer" class="dark">

            <div class="container">

              <div class="row">

                <!-- Footer Widgets
        ============================================= -->
                <div class="footer-widgets-wrap clearfix">

                  <div class="col-xs-12 col-md-4">
                    <div class="widget widget_links clearfix">
                      <h4>Navegação</h4>
                      <ul>
                        <li class=""><a href="{{ url('/') }}">
                            <div>Início</div>
                          </a></li>
                        <li class=""><a href="{{ url('/sobre') }}">
                            <div>Sobre Nós</div>
                          </a></li>
                        <li class=""><a href="{{ url('/anunciar') }}">
                            <div>Quero Anunciar</div>
                          </a></li>
                        <li class=""><a href="{{ url('/planos') }}">
                            <div>Planos & Benefícios</div>
                          </a></li>
                        @foreach($pages as $page)
                        <li class=""><a href="/pagina/{{ $page->slug }}">
                            <div>{{ $page->title }}</div>
                          </a></li>
                        @endforeach
                        <li class=""><a href="{{ url('/contato') }}">
                            <div>Entre em Contato</div>
                          </a></li>
                      </ul>
                    </div>
                  </div><!-- cols -->

                  <div class="col-xs-12 col-md-8">
                    <div class="row">
                      <div class="col-xs-12 col-md-6 pull-right">
                        <div class="widget widget_links clearfix text-right">
                          <h4>Entre em Contato</h4>
                          <div class="mb-5">
                            <abbr title="E-mail"><strong>E-mail:</strong></abbr> <a href="mailto:{{ $configs->config_email }}">{{ $configs->config_email }}</a>
                          </div>

                          @if($configs->redesocial_facebook != '#' || $configs->redesocial_instagram != '#' || $configs->redesocial_twitter != '#' || $configs->redesocial_linkedin != '#')
                          <h4>Siga-nos</h4>
                          
                          <div class="pull-right clearfix">
                            @if($configs->redesocial_facebook != '#')
                            <a href="{{ $configs->redesocial_facebook }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Facebook" class="social-icon si-small si-borderless si-facebook">
                              <i class="icon-facebook"></i>
                              <i class="icon-facebook"></i>
                            </a>
                            @endif
                            
                            @if($configs->redesocial_instagram != '#')
                            <a href="{{ $configs->redesocial_instagram }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Instagram" class="social-icon si-small si-borderless si-instagram">
                              <i class="icon-instagram"></i>
                              <i class="icon-instagram"></i>
                            </a>
                            @endif

                            @if($configs->redesocial_twitter != '#')
                            <a href="{{ $configs->redesocial_twitter }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Twitter" class="social-icon si-small si-borderless si-twitter">
                              <i class="icon-twitter"></i>
                              <i class="icon-twitter"></i>
                            </a>
                            @endif
                            
                            @if($configs->redesocial_linkedin != '#')
                            <a href="{{ $configs->redesocial_linkedin }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="linkedin" class="social-icon si-small si-borderless si-linkedin">
                              <i class="icon-linkedin"></i>
                              <i class="icon-linkedin"></i>
                            </a>
                            @endif
                          </div>
                          @endif
                        </div>

                        <div class="widget widget_links clearfix text-right mt-5">
                          <h4>Nosso Facebook</h4>
                          <div id="fb-root"></div>
                          <div class="fb-page" data-href="https://www.facebook.com/juntospeloseubairro/" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/juntospeloseubairro/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/juntospeloseubairro/">Juntos pelo seu Bairro - Seu Parceiro para Divulgação</a></blockquote>
                          </div>
                        </div>
                      </div><!-- cols -->

                    </div>
                  </div><!-- col-md-6 -->

                </div><!-- .footer-widgets-wrap end -->

              </div><!-- row -->
            </div><!-- container -->

            <!-- Copyrights
    ============================================= -->
            <div id="copyrights">

              <div class="container clearfix">

                <div class="col_half">
                  Todos os direitos reservados á <b>{{ $configs->config_title ?? 'Catalogo Base' }}</b> {{ date('Y') }}
                  <br>
                  <small>Programação: <a href="https://innsystem.com.br" title="InnSystem Inovação em Sistemas" taget="_Blank">InnSystem</a></small>
                </div>

              </div>

            </div><!-- #copyrights end -->

          </footer><!-- #footer end -->

        </div><!-- boxed -->

      </div><!-- container -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
  ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

  </div>
  <!-- External JavaScripts
  ============================================= -->
  <script src="{{ asset('js/app.js?101') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/jquery.js') }}" crossorigin="anonymous"></script>
  <script defer src="{{ asset('front/js/fontawesome-all.js') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/plugins.js?110') }}"></script>
  <script type="text/javascript" src="{{ asset('front/js/functions.js?110') }}"></script>
  <script src="{{ asset('js/function_theme.js?2') }}"></script>
  @yield('jsPage')

  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v8.0&appId=1094654680570525&autoLogAppEvents=1" nonce="0h9GDFIY"></script>
  <script data-ad-client="ca-pub-4990954202375879" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script src="https://www.googleoptimize.com/optimize.js?id=OPT-M8WLJL2"></script>
  <!-- Facebook Pixel Code -->
  <script>
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1202304329787354');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1202304329787354&ev=PageView&noscript=1" /></noscript>
  <!-- End Facebook Pixel Code -->

  <script>
    function loadScript(a) {
      var b = document.getElementsByTagName("head")[0],
        c = document.createElement("script");
      c.type = "text/javascript", c.src = "https://tracker.metricool.com/resources/be.js", c.onreadystatechange = a, c.onload = a, b.appendChild(c)
    }
    loadScript(function() {
      beTracker.t({
        hash: "ea2d3027a39398a93ac7a398d172fb30"
      })
    });
  </script>

  <script src="{{ asset('front/plugins/select2/js/select2.min.js') }}"></script>
  <script src="{{ asset('front/plugins/select2/js/i18n/pt-BR.js') }}"></script>
  <script>
    $(document).ready(function() {
      $('#sdistricoff, #alloff').on('input', function() {
        $('#top-search button').prop('disabled', $(this).val().length < 2);
      });
    });

    $('#sdistricoff').select2({
      placeholder: "bairro ou cidade",
      minimumInputLength: 2,
      language: 'pt-BR',
      ajax: {
        url: '{{url("/estabelecimentos/buscadistrito")}}',
        dataType: 'json',

        data: function(params) {
          return {
            sdistric: params.term
          }
        },

        processResults: function(data) {
          return {
            results: data.result.map(function(sdistric) {
              var name = '';
              var id = '';
              if (data.tipo == 'distric') {
                name = sdistric.distric,
                  id = sdistric.distric
              }
              if (data.tipo == 'city') {
                name = sdistric.city,
                  id = sdistric.city
              }
              if (data.tipo == 'state') {
                name = sdistric.state,
                  id = sdistric.state
              }
              return {
                text: name,
                id: id
              };
            })
          };

        },
        cache: true
      }
    });

    // Ao seleciona o distric usando o Enter ele já faz a busca
    $('.select2').on('select2:select', function() {
      $('.select2-selection').on('keyup', function(e) {
        if (e.keyCode === 13) {
          console.log('foi');
          $(this).closest('form').submit();
        }
      });
      //$('#top-search form').submit();
    });
  </script>
  <script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
  <script>
    $(document).ready(function() {

      $('#config_phone').mask('(00) 0000-00009');
      $('#config_phone').blur(function(event) {
        if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#config_phone').mask('(00) 0000-0000');
        } else {
          $('#config_phone').mask('(00) 90000-0000');
        }
      });

      $('#config_cellphone').mask('(00) 0000-00009');
      $('#config_cellphone').blur(function(event) {
        if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#config_cellphone').mask('(00) 0000-0000');
        } else {
          $('#config_cellphone').mask('(00) 90000-0000');
        }
      });


      $('#phone').blur(function(event) {
        if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#phone').mask('(00) 0000-0000');
        } else {
          $('#phone').mask('(00) 90000-0000');
        }
      });

      $('#cellphone').blur(function(event) {
        if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#cellphone').mask('(00) 0000-0000');
        } else {
          $('#cellphone').mask('(00) 90000-0000');
        }
      });

      $('#hour').mask('00:00');

      $(".priceFormated").mask('####.##', {
        reverse: true
      });
      var options = {
        onKeyPress: function(cpfcnpj, e, field, options) {
          var masks = ['000.000.000-009', '00.000.000/0000-00'];
          var mask = (cpfcnpj.length > 14) ? masks[1] : masks[0];
          $('#document').mask(mask, options);
        }
      };

      $('#document').mask('000.000.000-009', options);
    });
  </script>
</body>

</html>