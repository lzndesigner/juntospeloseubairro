@extends('front.base')
@section('title', '')
@section('metaHead')
<meta name="description" content="{{ $configs->config_description ?? '' }}">
<meta name="keywords" content="{{ $configs->config_keywords ?? '' }}" />
@endsection
@section('headerHome')
<header class="headerHome">
  <div id="header-wrap">
    <div class="container clearfix">
      <div class="d-column d-sm-column d-md-flex">
        <div class="">
          <div id="logo">
            <a href="{{ url('/')}}" class="standard-logo" data-dark-logo="{{ asset('galerias/logo_branco.png') }}"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}"></a>
            <a href="{{ url('/')}}" class="retina-logo" data-dark-logo="{{ asset('galerias/logo_branco.png') }}"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}"></a>
          </div><!-- #logo end -->
        </div>
        <div class="flex-grow-1">
          @include('front.includes.menu-top')
          @yield('search')
        </div>
      </div>
    </div><!-- container -->
  </div><!-- header-wrap -->
  <div id="header-descrip">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-12">
          <h1><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Juntos pelo seu Bairro</span> é uma plataforma que <span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">conecta vendedores e compradores</span> o que gera Impacto Social e Impacto Positivo</h1>
          <hr class="invisible">

          <!-- hide -->
          <div class="hide">
            <h3>Vantagens:</h3>
            <ul class="vantagens">
              <li><span class="icon"></span> Maior alcance para seu negócio: nossa plataforma aproxima vendedores e compradores para movimentar seu negócio e alcançar novos consumidores</li>
              <li><span class="icon"></span> Não pare de vender! Com o suporte da nossa plataforma, você pode realizar suas vendas pelo Whatsapp, Instagram, Facebook ou seu site.</li>
              <li><span class="icon"></span> Possuímos até um gerador de link para que você inclua seu anúncio do “Juntos” em suas mídias ou via Whatsapp.</li>
            </ul>

            <h3>Outros benefícios:</h3>
            <ul class="vantagens">
              <li><span class="icon"></span> Você pode destacar sua empresa na busca do Bairro </li>
              <li><span class="icon"></span> Você também pode divulgar sua empresa no nosso Outdoor que fica aqui página principal do nosso site.</li>
              <li><span class="icon"></span> Cadastrar mais de 1 estabelecimento e publicar várias fotos ou vídeos do seu produto ou serviço</li>
            </ul>
            <hr class="invisible">
            <div class="row">
              <div class="col-sm-6 col-lg-4 text-left hide">
                <i class="i-plain i-xlarge mb-0 mr-3 icon-building"></i>
                <div class="counter counter-large" style="color: #3ab8bf;">
                  <span data-from="1" data-to="{{ count($homeestablishments) }}" data-refresh-interval="50" data-speed="2000"></span>
                </div>
                <h5>Estabelecimentos</h5>
              </div><!-- col -->

              <div class="col-sm-6 col-lg-5 text-left">
                <i class="i-plain i-xlarge mb-0 mr-4 icon-heart"></i>
                <div class="counter counter-large" style="color: #3ab8bf;">
                  <span data-from="1000" data-to="{{ $countAllViews }}" data-refresh-interval="50" data-speed="2000"></span>
                </div>
                <h5>Total de Visualizações</h5>
              </div><!-- col -->
            </div><!-- row -->

          </div>
          <!-- hide -->
        </div><!-- cols -->
      </div><!-- row -->
    </div><!-- container -->
  </div><!-- header-descrip -->
</header><!-- #header end -->
@endsection

@section('content')
<div id="section-home-direct" class="page-section m-0">
  <div class="container">
    <div class="row">

      <div class="col-xs-12 col-md-7 col-lg-6">
        <div class="feature-box fbox-center fbox-home fbox-bg fbox-light fbox-effect">
          <div class="fbox-icon">
            <a href="{{ url('encontrar-estabelecimentos') }}"><i class="i-alt">Quero Comprar</i></a>
          </div>

          <p>Você comprador, de forma simples e prática poderá realizar buscas de estabelecimentos diferenciados: </p>
          <ul>
            <li><span class="inside">•</span> pelo bairro</li>
            <li><span class="inside">•</span> pela categoria</li>
            <li><span class="inside">•</span> pelo produto/serviço</li>
            <li><span class="inside">•</span> pelo nome da empresa</li>
          </ul>
          <p class="m-5"><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Surpreenda-se com os negócios do seu Bairro!</span></p>
          <p>Nossos parceiros anunciantes estão prontos para atender você diretamente via Whatsapp, Instagram ou Facebook.</p>


          @if($categoriesHome->count() > 0)
          <div id="section-home-categories" class="nobg noshadowbox my-5 mb-4">
            <h4>Encontre por Categorias</h4>
            <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="40" data-loop="true" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="4" data-items-md="4" data-items-lg="4">
              @foreach($categoriesHome as $categoryHome)
              <div class="oc-item item-category">
                <a href="/categorias/{{ $categoryHome->slug }}">
                  <picture><img src="{{ $categoryHome->image ? asset('storage/categories/'. $categoryHome->id .'/photos/' . $categoryHome->image) : asset('galerias/sem_image.png') }}" class="img-rounded" alt="{{ $categoryHome->title }}"></picture>
                  <h2>{{ $categoryHome->title }}</h2>
                </a>
              </div>
              @endforeach
            </div>
            <a href="{{ url('categorias/')}}">ver todas categorias</a>
          </div>
          @endif

          <p class="mb-2">Encontre o que você procura aqui </p>
          @include('front.includes.search')

          <div class="row mt-5">
            <div class="col-sm-12 text-center">
              <h5 class="title-counter">Visualizações no site</h5>
              <i class="i-plain i-xlarge mb-0 mr-4 icon-heart"></i>
              <div class="counter counter-large" style="color: #3ab8bf;">
                <span data-from="1000" data-to="{{ $countAllViews }}" data-refresh-interval="50" data-speed="2000"></span>
              </div>
            </div><!-- col -->
          </div>

        </div>
      </div><!-- col -->

      <div class="col-xs-12 col-md-5 col-lg-6">
        <div class="feature-box fbox-center fbox-home fbox-bg fbox-light fbox-effect">
          <div class="fbox-icon">
            @if(Auth::guard('customer')->check())
            <a href="{{ route('estabelecimentos.index', Auth::guard('customer')->user()->id) }}"><i class="i-alt">Quero Anunciar</i></a>
            @else
            <a href="{{ url('minha-conta/register') }}"><i class="i-alt">Quero Anunciar</i></a>
            @endif
          </div>
          <p><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Vantagens para você Vendedor:</span></p>
          <ul>
            <li><span class="inside">•</span> Maior alcance para seu negócio: alcance novos consumidores</li>
            <li><span class="inside">•</span> Não pare de vender! Com o suporte da nossa plataforma, você pode realizar suas vendas pelo Whatsapp, Instagram, Facebook ou seu site.</li>
            <li><span class="inside">•</span> Oferecemos o link do seu anúncio para que você use nas suas divulgações via whatsapp ou suas mídias sociais </li>
          </ul>

          <p class="mt-5"><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Outros benefícios:</span></p>
          <ul>
            <li><span class="inside">•</span> Você pode destacar sua empresa na busca do Bairro</li>
            <li><span class="inside">•</span> Você também pode divulgar sua empresa no Outdoor da página principal na nossa plataforma</li>
            <li><span class="inside">•</span> Publicar várias fotos ou vídeos no seu anúncio</li>
            <li><span class="inside">•</span> Cadastrar mais de 1 estabelecimento</li>
            <li><span class="inside">•</span> E muito mais! confira nos Planos e Benefícios</li>
          </ul>
          @if(Auth::guard('customer')->check())
          <a href="{{ route('estabelecimentos.index', Auth::guard('customer')->user()->id) }}" class="btn btn-lg btn-thema mt-2">Clique aqui para Anunciar</a>
          @else
          <a href="{{ url('minha-conta/register') }}" class="btn btn-lg btn-thema mt-2">Clique aqui para Anunciar</a>
          @endif

        </div>
      </div><!-- col -->


    </div><!-- row -->
  </div><!-- container -->
</div><!-- page -->

@if($apoios->count() > 0)
<section id="slider" class="my-5" style="margin-top:5rem !important;">
  <div class="container">
    <div class="emphasis-title center">
      <h2>Apoiar Causas Sociais está na nossa essência</h2>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <div class="row">
      @if($apoios->count() == 1)
      <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="1" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
          @elseif($apoios->count() == 2)
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="2" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
              @else
              <div class="col-xs-12 col-sm-12">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="3" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
                  @endif
                  @foreach($apoios as $apoio)
                  @if($apoio->url)
                  <a href="{{ $apoio->url }}" data-toggle="tooltip" data-original-title="{{$apoio->title}} - Clique para abrir"><img src="/storage/{{ $apoio->image }}" alt="{{ $apoio->title}}"></a>
                  @else
                  <a href="javascript:;" style="cursor:w-resize;"><img src="/storage/{{ $apoio->image }}" alt="{{ $apoio->title}}"></a>
                  @endif
                  @endforeach
                </div>
              </div>
            </div>
          </div>
</section>
@endif
<!-- END CONTAINER -->
@endsection