@extends('front.base')
@section('title', '')
@section('metaHead')
<meta name="description" content="{{ $configs->config_description ?? '' }}">
<meta name="keywords" content="{{ $configs->config_keywords ?? '' }}" />
@endsection
@section('headerHome')
<header class="headerHome">
  <div id="header-wrap">
    <div class="container clearfix">
      <div class="d-column d-sm-column d-md-flex">
        <div class="">
          <div id="logo">
            <a href="{{ url('/')}}" class="standard-logo" data-dark-logo="{{ asset('galerias/logo_branco.png') }}"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}"></a>
            <a href="{{ url('/')}}" class="retina-logo" data-dark-logo="{{ asset('galerias/logo_branco.png') }}"><img src="{{ asset('galerias/logo_preto.png') }}" alt="{{ $configs->config_title ?? 'Catalogo Base' }}"></a>
          </div><!-- #logo end -->
        </div>
        <div class="flex-grow-1">
          @include('front.includes.menu-top')
          @yield('search')
        </div>
      </div>
    </div><!-- container -->
  </div><!-- header-wrap -->
  <div id="header-descrip">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-9">
          <h1><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Juntos pelo seu Bairro</span> é uma plataforma que <span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">conecta vendedores e compradores</span> o que gera Impacto Social e Impacto Positivo</h1>
        </div><!-- cols -->
        <div class="col-xs-12 col-md-3">
          @if($configs->redesocial_facebook != '#' || $configs->redesocial_instagram != '#' || $configs->redesocial_twitter != '#' || $configs->redesocial_linkedin != '#')
          <div class="text-center">
            <h4>Siga-nos</h4>

            <div class="clearfix">
              @if($configs->redesocial_facebook != '#')
              <a href="{{ $configs->redesocial_facebook }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Facebook" class="social-icon si-small si-borderless si-facebook">
                <i class="icon-facebook"></i>
                <i class="icon-facebook"></i>
              </a>
              @endif

              @if($configs->redesocial_instagram != '#')
              <a href="{{ $configs->redesocial_instagram }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Instagram" class="social-icon si-small si-borderless si-instagram">
                <i class="icon-instagram"></i>
                <i class="icon-instagram"></i>
              </a>
              @endif

              @if($configs->redesocial_twitter != '#')
              <a href="{{ $configs->redesocial_twitter }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="Twitter" class="social-icon si-small si-borderless si-twitter">
                <i class="icon-twitter"></i>
                <i class="icon-twitter"></i>
              </a>
              @endif

              @if($configs->redesocial_linkedin != '#')
              <a href="{{ $configs->redesocial_linkedin }}" target="_Blank" data-toggle="tooltip" data-placement="bottom" data-original-title="linkedin" class="social-icon si-small si-borderless si-linkedin">
                <i class="icon-linkedin"></i>
                <i class="icon-linkedin"></i>
              </a>
              @endif
            </div>
          </div>
          @endif

          <div class="text-center mt-4">
            <h5 class="title-counter">Visualizações no site</h5>
            <i class="i-plain i-xlarge mb-0 mr-4 icon-heart"></i>
            <div class="counter counter-large" style="color: #3ab8bf;">
              <span data-from="5000" data-to="{{ $countAllViews }}" data-refresh-interval="50" data-speed="2000">0000</span>
            </div>
          </div><!-- text-center -->
        </div>
      </div><!-- row -->
    </div><!-- container -->
  </div><!-- header-descrip -->
</header><!-- #header end -->
@endsection

@section('content')
<div id="section-home-direct" class="page-section py-2 m-0">
  <div class="container">
    <div class="row">

      <div class="col-xs-12 col-md-12 col-lg-12">
        <div class="feature-box fbox-center fbox-home fbox-bg fbox-light fbox-effect">
          <div class="fbox-icon">
            <i class="i-alt">Quero Comprar</i>
          </div>

          <p>Você comprador, de forma simples e prática poderá realizar buscas de estabelecimentos diferenciados: </p>
          <ul>
            <li><span class="inside">•</span> pelo cidade/bairro</li>
            <li><span class="inside">•</span> pela categoria</li>
            <li><span class="inside">•</span> pelo produto/serviço</li>
            <li><span class="inside">•</span> pelo nome da empresa</li>
          </ul>
          <p class="m-5"><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Surpreenda-se com os negócios da sua região!</span></p>
          <p>Nossos parceiros anunciantes estão prontos para atender você diretamente via Whatsapp, Instagram ou Facebook.</p>

          <p class="mt-5 mb-2">Encontre o que você procura aqui </p>
          @include('front.includes.search')

        </div><!-- card -->
      </div><!-- col -->
    </div><!-- row -->
  </div><!-- container -->
</div><!-- page -->

@if($sliders->count() > 0)
<section id="slider" class="my-5">
  <div class="container">
    <div class="emphasis-title center">
      <h2>Vitrine</h2>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>
    <div class="row">
      @if($sliders->count() == 1)
      <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="1" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="5000">
          @elseif($sliders->count() == 2)
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="2" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="5000">
              @else
              <div class="col-xs-12 col-sm-12">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="3" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="5000">
                  @endif
                  @foreach($sliders as $slider)
                  @if($slider->url)
                  <a href="{{ $slider->url }}" data-toggle="tooltip" data-original-title="{{$slider->title}} - Clique para abrir"><img src="/storage/{{ $slider->image }}" alt="{{ $slider->title}}"></a>
                  @else
                  <a href="javascript:;" style="cursor:w-resize;"><img src="/storage/{{ $slider->image }}" alt="{{ $slider->title}}"></a>
                  @endif
                  @endforeach
                </div>
              </div>
            </div>
          </div>
</section>
@endif

@if($categoriesHome->count() > 0)
<div id="section-home-categories" class="nobg noshadowbox my-5 mb-4">
  <div class="container">
  <div class="emphasis-title center">
      <h2>Encontre por Categorias</h2>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>
    <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="40" data-loop="true" data-nav="true" data-pagi="false" data-items-xxs="2" data-items-xs="3" data-items-sm="6" data-items-md="7" data-items-lg="7">
      @foreach($categoriesHome as $categoryHome)
      <div class="oc-item item-category">
        <a href="/categorias/{{ $categoryHome->slug }}">
          <picture><img src="{{ $categoryHome->image ? asset('storage/categories/'. $categoryHome->id .'/photos/' . $categoryHome->image) : asset('galerias/sem_image.png') }}" class="img-rounded" alt="{{ $categoryHome->title }}"></picture>
          <h2>{{ $categoryHome->title }}</h2>
        </a>
      </div>
      @endforeach
    </div>
    <a href="{{ url('categorias/')}}">ver todas categorias</a>
  </div>
</div>
@endif

@if($apoios->count() > 0)
<section id="slider" class="my-5" style="margin-top:5rem !important;">
  <div class="container">
    <div class="emphasis-title center">
      <h2>Apoiar Causas Sociais está na nossa essência</h2>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <div class="row">
      @if($apoios->count() == 1)
      <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="1" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
          @elseif($apoios->count() == 2)
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="2" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
              @else
              <div class="col-xs-12 col-sm-12">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="3" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
                  @endif
                  @foreach($apoios as $apoio)
                  @if($apoio->url)
                  <a href="{{ $apoio->url }}" data-toggle="tooltip" data-original-title="{{$apoio->title}} - Clique para abrir"><img src="/storage/{{ $apoio->image }}" alt="{{ $apoio->title}}"></a>
                  @else
                  <a href="javascript:;" style="cursor:w-resize;"><img src="/storage/{{ $apoio->image }}" alt="{{ $apoio->title}}"></a>
                  @endif
                  @endforeach
                </div>
              </div>
            </div>
          </div>
</section>
@endif
<!-- END CONTAINER -->
@endsection