
<div id="section-home-direct" class="page-section m-0">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-5 col-lg-6">
        <div class="panel panel-thema mb-0 d-flex justify-content-center align-items-center">
          <div class="panel-body">
            <h4>Quero Anunciar</h4>
            <p>Cadastre seu estabelecimento e obtenha maior visibilidade</p>
            <a href="{{ url('/minha-conta/register')}}" class="btn btn-lg btn-thema mb-3">Anuncie seu Estabelecimento</a>

            <p>Saiba como cadastrar <a href="/pagina/como-cadastrar-meu-estabelecimento">clicando aqui</a></p>
          </div><!-- panel -->
        </div><!-- panel -->
      </div><!-- col -->

      <div class="col-xs-12 col-md-7 col-lg-6">
        <div class="panel panel-thema mb-0 d-flex justify-content-center align-items-center">
          <div class="panel-body">
            <h4>Quero Comprar</h4>
            <p>Encontre estabelecimentos nos Bairros ou Cidades</p>
            @include('front.includes.search')
          </div><!-- panel -->
        </div><!-- panel -->
      </div><!-- col -->

    </div><!-- row -->
  </div><!-- container -->
</div><!-- page -->



@if(count($homeestablishments) > 0)
<div id="section-home-establishments" class="page-section">
  <div class="container clearfix">
    <div class="row">
      <div class="col-xs-12 col-md-12 text-center">
        <h1>Conheça os Estabelecimentos</h1>
        <h2>veja os principais locais do seu bairro ou cidade</h2>
        <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
      </div>
    </div><!-- row -->

    <div class="row">
      @foreach($homeestablishments as $homeestablishment)
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
        <div class="testimonial">
          <div class="testi-image">
            <a href="/categorias/{{ $homeestablishment->categoriaslug }}/{{ $homeestablishment->estabelecimentoslug }}">
              <img src="{{ $homeestablishment->image ? asset('storage/establishments/'. $homeestablishment->establishment_id .'/photos/thumb_' . $homeestablishment->image) : asset('galerias/sem_image.png') }}" alt="{{ $homeestablishment->estabelecimento }}">
            </a>
            @if($homeestablishment->featured == 1)
            <span class="label label-thema">Destaque</span>
            @endif
          </div>
          <div class="testi-content d-flex align-items-center justify-content-center">
            <div class="testi-meta">
              <a href="/categorias/{{ $homeestablishment->categoriaslug }}/{{ $homeestablishment->estabelecimentoslug }}">
                <h2>{{ $homeestablishment->estabelecimento }}</h2>
              </a>
              <p>{{ $homeestablishment->categoria }}</p>
              <div class="tags mt-3">
                <small> <i class="fa fa-map-marker-alt"></i> {{ $homeestablishment->distric }} - {{ $homeestablishment->city }} <br> {{ $homeestablishment->state }}</small>
              </div>
            </div>
          </div>
        </div>
      </div><!-- col-md-4 -->
      @endforeach
    </div>

  </div>
</div><!-- section-home -->
@endif






@if($sliders->count() > 0)
<section id="slider" class="my-5">
  <div class="container">
    <div class="row">
      @if($sliders->count() == 1)
      <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="1" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
          @elseif($sliders->count() == 2)
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="2" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
              @else
              <div class="col-xs-12 col-sm-12">
                <div id="oc-slider" class="owl-carousel carousel-widget" data-margin="20" data-items="3" data-items-sm="2" data-items-xs="1" data-pagi="true" data-dots="true" data-loop="true" data-animate-in="fade" data-speed="450" data-animate-out="fadeOut" data-autoplay="7000">
                  @endif
                  @foreach($sliders as $slider)
                  @if($slider->url)
                  <a href="{{ $slider->url }}" data-toggle="tooltip" data-original-title="{{$slider->title}} - Clique para abrir"><img src="/storage/{{ $slider->image }}" alt="{{ $slider->title}}"></a>
                  @else
                  <a href="javascript:;" style="cursor:w-resize;"><img src="/storage/{{ $slider->image }}" alt="{{ $slider->title}}"></a>
                  @endif
                  @endforeach
                </div>
              </div>
            </div>
          </div>
</section>
@endif









@if($categories->count() > 0)
<div id="section-home-categories" class="page-section">
  <div class="container clearfix">
    <div class="row">
      <div class="col-xs-12 col-md-12 text-center">
        <h1>Conheça Empresas por Categorias</h1>
        <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
        <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="40" data-loop="true" data-nav="true" data-pagi="true" data-items-xxs="1" data-items-xs="2" data-items-sm="7" data-items-md="7" data-items-lg="7">
          @foreach($categories as $category)
          <div class="oc-item item-category">
            <a href="/categorias/{{ $category->slug }}">
              <picture><img src="{{ $category->image ? asset('storage/categories/'. $category->id .'/photos/' . $category->image) : asset('galerias/sem_image.png') }}" alt="{{ $category->title }}"></picture>
              <h2>{{ $category->title }}</h2>
            </a>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endif