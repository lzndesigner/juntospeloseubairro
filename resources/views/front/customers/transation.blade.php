@extends('front.base')
@section('title', 'Minhas Transações')

@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/minha-conta') }}">Minha Conta</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('elements.messages')
          <div class="row">
            @include('front.customers.includes.menu-account')

            <div class="col-sm-12 col-md-9">
              <div class="panel-account">
                <h2>@yield('title')</h2>

                <div class="table-responsive">
                  @if(count($transations) > 0)
                  <table class="table table-hover table-bordered table-striped" id="transations-table">
                    <thead>
                      <tr>
                        <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'id']) }}">#</a></td>
                        <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'plan_name']) }}">Plano</a></td>
                        <td>Período</td>
                        <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'plan_method']) }}">Método</a></td>
                        <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'plan_value']) }}">Valor</a></td>
                        <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'date']) }}">Data</a></td>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($transations as $transation)
                      <tr id="row-{{ $transation->id }}">
                        <td>
                          <b>{{ $transation->id }}</b>
                        </td>
                        <td>{{ $transation->plan_name }}</td>
                        <td>{{ $transation->plan_period }} dias</td>
                        <td>
                          @if($transation->plan_method == 'banktransfer')
                          Depósito/Transf.
                          @elseif($transation->plan_method == 'mercadopago')
                          Mercado Pago
                          @else
                          Grátis
                          @endif
                        </td>
                        <td>R$ {{ $transation->plan_value }}</td>
                        <td>{{ \Carbon\Carbon::parse($transation->date)->format('d/m/y H:i') }}</td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                  @else
                  <div class="alert alert-info">Nenhuma transação cadastrada.</div>
                  @endif
                  <hr>

                </div><!-- table-responsive -->


              </div><!-- panel-account -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection