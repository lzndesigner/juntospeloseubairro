@extends('front.base')
@section('title', 'Recuperar Senha')

@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/minha-conta/login') }}">Acessar Conta</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 col-md-offset-4">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('elements.messages')
          <form class="form-horizontal" method="POST" action="{{ route('customer.reset.request') }}">
            {{ csrf_field() }}
            <div class="d-flex">
              <div class="flex-fill mx-5">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="control-label">Seu E-mail</label>
                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="E-mail de acesso">
                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div><!-- form-group -->
              </div>
            </div>

            <div class="form-group mx-4">
              <button type="submit" class="btn btn-block btn-thema"><i class="fa fa-check"></i> Recuperar Senha</button>
              <hr>
              <p><b>Você já possui uma conta? <a href="/minha-conta/login">Faça seu login</a></b></p>
            </div>


          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('jsPage')
<script>
  // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
  $("form").submit(function(e) {
    //disable the submit button
    $("form button").attr("disabled", true);
    $('form').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
    // setTimeout(function() {
    //   $('form button').prop("disabled", false);
    //   $('form').parent().find('svg.fa-spinner').html('');
    // }, 8000);
  });
</script>
@endsection