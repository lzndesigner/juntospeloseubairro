@extends('front.base')
@section('title', 'Minha Conta')

@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('elements.messages')
          <div class="row">
            @include('front.customers.includes.menu-account')

            <div class="col-sm-12 col-md-9">
              <div class="panel-account">
                <h1>Bem-vindo, {{ Auth::user()->name }}</h1>
                @if (session('status'))
                <div class="alert alert-success">
                  {{ session('status') }}
                </div>
                @endif

                <div class="widget-account">
                  <div class="icon">{{ count($myestablishments) }}</div>
                  <a href="/minha-conta/estabelecimentos">Estabelecimentos</a>
                </div>

                @if($checkValidates)
                <hr>

                <div class="alert alert-info">
                  <h4>Vencimentos próximos!</h4>
                  <p>Olá, notamos que você possui {{ count($checkValidates)}} estabelecimento que está próximo do vencimento. <br> Você pode renovar o plano acessando a <a href="{{ route('estabelecimentos.index', Auth::user()->id) }}"><b>Área de Estabelecimentos</b></a> e clicando em <b>Renovar</b>.</p>
                  <br>
                  <p><b>Veja os estabelecimentos:</b></p>
                  <ul class="ml-5 mt-3">
                    @foreach($checkValidates as $checkValidate)
                    <li class="mb-4">
                      Estabelecimento: {{$checkValidate->name}}
                      <br>
                      Vence no dia <b>{{ \Carbon\Carbon::parse($checkValidate->plan_dateend)->format('d/m/Y') }}</b>
                      <br>
                      <a href="{{ route('customer.plan.renew', $checkValidate->idCustomerPlans) }}" class="btn btn-xs btn-danger btn-icon" data-toggle="tooltip" data-original-title="Renovar Plano">Renovar</a>
                    </li>
                    @endforeach
                  </ul>
                </div>
                @endif

              </div><!-- panel-account -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection