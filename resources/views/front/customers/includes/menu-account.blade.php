<div class="col-sm-12 col-md-3">
	<div class="menuMyaccount">
		<p><b>Menu de Navegação</b></p>
		<ul>
			<li class="@if(collect(request()->segments())->last() == 'minha-conta') current @endif"><a href="{{ route('customer.dashboard') }}">Página Inicial</a></li>
			<li class="@if(collect(request()->segments())->last() == 'estabelecimentos') current @endif"><a href="{{ route('estabelecimentos.index', Auth::user()->id) }}">Meus Estabelecimentos</a></li>
			<li class="@if(collect(request()->segments())->last() == 'estatisticas') current @endif"><a href="{{ route('statistics', Auth::user()->id) }}">Minhas Estatísticas</a></li>
			<li class="@if(collect(request()->segments())->last() == 'transacoes') current @endif"><a href="{{ route('customer.transation', Auth::user()->id) }}">Minhas Transações</a></li>
			<li class="divider"></li>
			<li class="@if(collect(request()->segments())->last() == 'edit') current @endif"><a href="{{ route('editar.edit', Auth::user()->id) }}">Editar Perfil</a></li>
			<li><a href="{{ route('customer.auth.logout') }}">Sair da Conta</a></li>
		</ul>
	</div>
</div>