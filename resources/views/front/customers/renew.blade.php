@extends('front.base')
@section('title', 'Renovação de Plano')
@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('elements.messages')
          <div class="row">
            @include('front.customers.includes.menu-account')

            <div class="col-sm-12 col-md-9">
              <div class="panel-account">
                <h2>@yield('title')</h2>
                @php
                if ($count_plans>= 1 && $discount_customer > 0) {
                $discount = (float) $discount_customer;
                $discount = $discount / 100.0;
                $price = (float) $planAtual->price;
                $pricefinal = $price - ($discount * $price);
                $pricefinalFormated = number_format($pricefinal, 2, ',', '');
                } else {
                $pricefinal = (float) $planAtual->price;
                $pricefinalFormated = number_format($pricefinal, 2, ',', '');
                }
                @endphp
                <div class="">
                  <div class="row">
                    <div class="col-xs-12 col-md-6">
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <h4>Plano Atual</h4>
                          <p><b>Plano:</b> {{ $planAtual->name }}</p>
                          <p><b>Vencimento:</b> {{ \Carbon\Carbon::parse($planAtual->plan_dateend)->format('d/m/Y')}}</p>
                          <p><b>Valor:</b> R$ {{ $pricefinalFormated }} @if($count_plans >= 1 && $discount_customer > 0 && $pricefinalFormated > 0)
                            <small>(com {{ $discount_customer }}% de Desconto)</small>
                            @endif</p>
                          <p><b>Período:</b> {{ $planAtual->period }} dias</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> 
                @if($planAtual->id <=4)
                <div class="">
                  <h5>Escolha uma das assinaturas abaixo para que seu anúncio possa voltar ao ar.</h5>
                  <div class="plans-box">
                    <!-- <div class="d-flex flex-column flex-sm-column flex-md-row flex-lg-row"> -->
                    <div class="row">
                      @foreach($plansAvailable as $plan)
                      @php
                      if($plan->id < 5){ if ($count_plans>= 1 && $discount_customer > 0) {
                        $discount = (float) $discount_customer;
                        $discount = $discount / 100.0;
                        $price = (float) $plan->price;
                        $pricefinal = $price - ($discount * $price);
                        $pricefinalFormated = number_format($pricefinal, 2, ',', '');
                        } else {
                        $price = (float) $plan->price;
                        $pricefinal = (float) $plan->price;
                        $pricefinalFormated = number_format($pricefinal, 2, ',', '');
                        }
                        }
                        @endphp
                        <!-- <div id="plan-{{ $plan->id }}" class="flex-fill panel panel-default"> -->
                        <div id="plan-{{ $plan->id }}" class="col-xs-12 col-sm-12 col-md-4 mb-3">
                          <div class="panel panel-default">
                            <form action="{{ route('customer.plan.renewpayment', $planAtual->establishment_id ) }}" name="plan-{{ $plan->id }}" id="plan-{{ $plan->id }}" class="panel-body">
                              <h2>{{ $plan->name }}</h2>

                              @php
                              $valor = $pricefinal;
                              switch ($plan->period) {
                              case 30:
                              $period = 1;
                              break;
                              case 90:
                              $period = 3;
                              break;
                              case 180:
                              $period = 6;
                              break;
                              case 360:
                              $period = 12;
                              break;
                              }
                              $pricemensal = $valor / $period;
                              @endphp

                              <div class="price">
                                @if ($count_plans >= 1 && $discount_customer > 0)
                                @if($price > 0)
                                <p><small>De: <strike>{{ number_format($price, 2, ',', '') }}</strike></small></p>
                                @endif
                                @endif
                                @if($plan->price > 0)
                                <p><small>investimento de</small></p>
                                <p class="price_plan">R$ {{ number_format($pricemensal, 2, ',', '') }}/mês</p>
                                @else
                                <p>Grátis</p>
                                @endif
                                <input type="hidden" name="price" id="price" value="{{ $pricefinal }}">
                                <input type="hidden" name="plan_id" id="plan_id" value="{{ $plan->id }}">
                                <input type="hidden" name="plan_atual_id" id="plan_atual_id" value="{{ $planAtual->plan_id }}">
                                <input type="hidden" name="discount_customer" id="discount_customer" value="{{ $discount_customer }}">

                                @php
                                if($plan->period == 30){
                                $mesplano = '1 mês';
                                }elseif($plan->period == 90){
                                $mesplano = '3 meses';
                                }elseif($plan->period == 180){
                                $mesplano = '6 meses';
                                }elseif($plan->period == 360){
                                $mesplano = '12 meses';
                                }else{
                                $mesplano = '-';
                                }
                                @endphp

                                <select name="select_period" id="select_period" class="form-control" readonly onclick="someValue({{ $plan->id }})">
                                  <option value="{{ $plan->period }}" data-pricemonth="{{ $plan->price }}">{{ $mesplano }}</option>
                                </select>

                                @if($plan->price > 0)
                                <p><small>Investimento total de R$ {{ number_format($plan->price, 2, ',', '') }}</small></p>
                                @endif

                              </div>
                              <div class="info">
                                @if($plan->description)
                                {!! html_entity_decode(str_replace('*', '<i class="fa fa-check-circle text-success mr-2"></i>', $plan->description)) !!}
                                @endif
                              </div>
                              <div class="buttons">
                                <button type="button" data-toggle="modal" data-target="#exampleModal-{{ $plan->id }}" class="btn btn-thema"><i class="fa fa-check"></i> Quero esse!</button>
                              </div>

                              <!-- Modal -->
                              <div class="modal fade" id="exampleModal-{{ $plan->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">{{ $plan->name }} <br> Escolha e clique no método de pagamento abaixo</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <div class="row">
                                        @if($pricefinal > 0)
                                        @if($configs->payment_banktransfer_status == 1)
                                        <div class="col-xs-12 col-md-12">
                                          <div class="radio radio_payment_method">
                                            <label>
                                              <input type="radio" name="plan_method" id="plan_method" value="banktransfer" required autofocus>
                                              <div class="info-bank">
                                                <div class="panel panel-default">
                                                  <div class="panel-body">
                                                    <div class="iconChecked">
                                                      <i class="fa fa-check"></i>
                                                    </div>
                                                    <h5>Depósito/Transf. Bancária</h5>
                                                    {!! html_entity_decode($configs->payment_banktransfer_info) !!}
                                                    <p>Em caso de preferência por depósito, TED ou transferência por favor entrar em contato pelo email: <a href="mailto:{{ $configs->config_email }}">{{ $configs->config_email }}</a></p>
                                                  </div>
                                                </div>
                                              </div>
                                            </label>
                                          </div>
                                        </div>
                                        @endif
                                        <div class="col-xs-12 col-md-12">
                                          <div class="radio radio_payment_method">
                                            <label>
                                              <input type="radio" name="plan_method" id="plan_method" value="mercadopago">
                                              <div class="info-mercadopago">
                                                <div class="panel panel-default">
                                                  <div class="panel-body">
                                                    <div class="iconChecked">
                                                      <i class="fa fa-check"></i>
                                                    </div>
                                                    <h5>Mercado Pago - Cartão & Boleto</h5>
                                                    <p>Realize o pagamento através de Cartão de Crédito ou Boleto Bancário.</p>
                                                    <img src="{{ asset('galerias/payment/mercadopago_parcelamento.png') }}" alt="Mercado Pago" class="img-responsive" style="max-width:80%;">
                                                  </div>
                                                </div>
                                              </div>
                                            </label>
                                          </div>
                                        </div>
                                        @else
                                        <div class="col-xs-12 col-md-12">
                                          <div class="radio radio_payment_method">
                                            <label>
                                              <input type="radio" name="plan_method" id="plan_method" value="gratis" checked>
                                              <div class="info-bank">
                                                <div class="panel panel-default" style="min-height:auto;">
                                                  <div class="panel-body">
                                                    <div class="iconChecked">
                                                      <i class="fa fa-check"></i>
                                                    </div>
                                                    <h5>Pagamento Grátis</h5>
                                                    <p>Finalize seu pedido e aguarde nossa equipe liberar seu cadastro.</p>
                                                  </div>
                                                </div>
                                              </div>
                                            </label>
                                          </div>
                                        </div>
                                        @endif
                                      </div><!-- row -->
                                      <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                          <div class="checkbox">
                                            <label>
                                              <input type="checkbox" name="checked_terms" id="checked_terms" value="1" required> Ao contratar o plano, <br> <b>eu declaro que li e aceito os <a href="javascript:;" class="openTerms">Termos de Contrato</a>.</b>
                                            </label>
                                          </div>

                                          <div class="boxTerms hide">
                                            <div class="button_closed openTerms"><i class="fa fa-times"></i></div>
                                            @foreach($pageTerms as $pageTerm)
                                            {!! html_entity_decode($pageTerm->body) !!}
                                            @endforeach
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="submit" class="btn btn-thema" id="button-plan-{{ $plan->id }}"><i class="fa fa-check"></i> Finalizar</button>
                                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
                                    </div>
                                  </div>
                                </div>
                              </div><!-- end modal -->
                              <!-- Modal -->
                            </form>
                          </div>
                        </div>
                        @endforeach
                    </div>

                    <img src="{{ asset('galerias/payment/mercadopago_parcelamento.png') }}" alt="Mercado Pago" class="img-responsive mt-5" style="max-width:600px">

                  </div>
                </div>
                @else
                

                @php
                      if($planAtual->id < 5){ if ($count_plans>= 1 && $discount_customer > 0) {
                        $discount = (float) $discount_customer;
                        $discount = $discount / 100.0;
                        $price = (float) $planAtual->price;
                        $pricefinal = $price - ($discount * $price);
                        $pricefinalFormated = number_format($pricefinal, 2, ',', '');
                        } else {
                        $price = (float) $planAtual->price;
                        $pricefinal = (float) $planAtual->price;
                        $pricefinalFormated = number_format($pricefinal, 2, ',', '');
                        }
                        }
                        @endphp
                        <!-- <div id="plan-{{ $planAtual->id }}" class="flex-fill panel panel-default"> -->
                        <div id="plan-{{ $planAtual->id }}" class="col-xs-12 col-sm-12 col-md-12">
                          <div class="">
                            <form action="{{ route('customer.plan.renewpayment', $planAtual->establishment_id ) }}" name="plan-{{ $planAtual->id }}" id="plan-{{ $planAtual->id }}">

                            <h4>Escolha o meio de pagamento</h4>

                              @php
                              $valor = $pricefinal;
                              switch ($planAtual->period) {
                              case 30:
                              $period = 1;
                              break;
                              case 90:
                              $period = 3;
                              break;
                              case 180:
                              $period = 6;
                              break;
                              case 360:
                              $period = 12;
                              break;
                              }
                              $pricemensal = $valor / $period;
                              @endphp

                                <input type="hidden" name="price" id="price" value="{{ $pricefinal }}">
                                <input type="hidden" name="plan_id" id="plan_id" value="{{ $planAtual->id }}">
                                <input type="hidden" name="plan_atual_id" id="plan_atual_id" value="{{ $planAtual->plan_id }}">
                                <input type="hidden" name="discount_customer" id="discount_customer" value="{{ $discount_customer }}">
                                <input type="hidden" name="select_period" id="select_period" value="{{ $planAtual->period }}">



                                      <div class="row">
                                        @if($pricefinal > 0)
                                        @if($configs->payment_banktransfer_status == 1)
                                        <div class="col-xs-12 col-md-12">
                                          <div class="radio radio_payment_method">
                                            <label>
                                              <input type="radio" name="plan_method" id="plan_method" value="banktransfer" required autofocus>
                                              <div class="info-bank">
                                                <div class="panel panel-default">
                                                  <div class="panel-body">
                                                    <div class="iconChecked">
                                                      <i class="fa fa-check"></i>
                                                    </div>
                                                    <h5>Depósito/Transf. Bancária</h5>
                                                    {!! html_entity_decode($configs->payment_banktransfer_info) !!}
                                                    <p>Em caso de preferência por depósito, TED ou transferência por favor entrar em contato pelo email: <a href="mailto:{{ $configs->config_email }}">{{ $configs->config_email }}</a></p>
                                                  </div>
                                                </div>
                                              </div>
                                            </label>
                                          </div>
                                        </div>
                                        @endif
                                        <div class="col-xs-12 col-md-12">
                                          <div class="radio radio_payment_method">
                                            <label>
                                              <input type="radio" name="plan_method" id="plan_method" value="mercadopago">
                                              <div class="info-mercadopago">
                                                <div class="panel panel-default">
                                                  <div class="panel-body">
                                                    <div class="iconChecked">
                                                      <i class="fa fa-check"></i>
                                                    </div>
                                                    <h5>Mercado Pago - Cartão & Boleto</h5>
                                                    <p>Realize o pagamento através de Cartão de Crédito ou Boleto Bancário.</p>
                                                    <img src="{{ asset('galerias/payment/mercadopago_parcelamento.png') }}" alt="Mercado Pago" class="img-responsive" style="max-width:80%;">
                                                  </div>
                                                </div>
                                              </div>
                                            </label>
                                          </div>
                                        </div>
                                        @else
                                        <div class="col-xs-12 col-md-12">
                                          <div class="radio radio_payment_method">
                                            <label>
                                              <input type="radio" name="plan_method" id="plan_method" value="gratis" checked>
                                              <div class="info-bank">
                                                <div class="panel panel-default" style="min-height:auto;">
                                                  <div class="panel-body">
                                                    <div class="iconChecked">
                                                      <i class="fa fa-check"></i>
                                                    </div>
                                                    <h5>Pagamento Grátis</h5>
                                                    <p>Finalize seu pedido e aguarde nossa equipe liberar seu cadastro.</p>
                                                  </div>
                                                </div>
                                              </div>
                                            </label>
                                          </div>
                                        </div>
                                        @endif
                                        <button type="submit" class="btn btn-thema" id="button-plan-{{ $planAtual->id }}"><i class="fa fa-check"></i> Finalizar</button>
                                      </div><!-- row -->
                               
                            </form>
                          </div>
                        </div>

                
                @endif
              </div>
            </div>

          </div><!-- panel-account -->
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection

@section('jsPage')
<script>
  // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
  // $("form").submit(function(e) {
  //   //disable the submit button
  //   $("form button").attr("disabled", true);
  //   $('form').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
  //   setTimeout(function() {
  //     $('form button').prop("disabled", false);
  //     $('form').parent().find('svg.fa-spinner').html('');
  //   }, 4000);
  // });

  // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
  $("form").submit(function(e) {
    //disable the submit button
    $("form .modal-footer button").attr("disabled", true);
    $('form .modal-body').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
    setTimeout(function() {
      $('form .modal-footer button').prop("disabled", false);
      $('form .modal-body').parent().find('svg.fa-spinner').html('');
    }, 10000);
  });
</script>
@endsection