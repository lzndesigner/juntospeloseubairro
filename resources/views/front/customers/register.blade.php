@extends('front.base')
@section('title', 'Registre sua Conta')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-7">
            <div id="header-descrip" class="sub-description py-0">
            <h1><span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">Juntos pelo seu Bairro</span> é uma plataforma que <span style="background:#3ab8bf; color:#FFF; padding:5px 10px;">conecta vendedores e compradores</span> o que gera Impacto Social e Impacto Positivo</h1>
                <a href="{{ url('/planos') }}" class="btn btn-thema btn-lg mt-3">Conheça nossos Planos & Benefícios</a>
            </div>

            @if($categories->count() > 0)
            <div id="section-home-categories" class="page-section nobg noshadowbox px-4 hidden-xs hidden-sm visible-md visible-lg">
                <div class="row">
                    <div class="emphasis-title center">
                        <h3>Conheça algumas Categorias</h3>
                        <p>Não encontrou sua categoria? Solicite agora mesmo <a href="{{ url('contato') }}">clicando aqui</a></p>
                    </div>
                    <div class="col-xs-12 col-md-12 text-center">
                        <div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-autoplay="5000" data-margin="40" data-loop="true" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="4" data-items-md="4" data-items-lg="4">
                            @foreach($categories as $category)
                            <div class="oc-item item-category">
                                <a href="/categorias/{{ $category->slug }}">
                                    <picture><img src="{{ $category->image ? asset('storage/categories/'. $category->id .'/photos/' . $category->image) : asset('galerias/sem_image.png') }}" alt="{{ $category->title }}"></picture>
                                    <h2>{{ $category->title }}</h2>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="col-xs-12 col-md-5">
            <div class="panel panel-default mt-5 clearfix">
                <div class="panel-body">
                    <h2>Faça seu Cadastro</h2>
                    <form class="form-horizontal" method="POST" action="{{ route('customer.register.store') }}">
                        {{ csrf_field() }}

                        <h4>Informações Pessoais</h4>
                        <div class="d-flex">
                            <div class="flex-fill mx-5">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="control-label">Nome Completo (<strong class="text-danger">*</strong>)</label>
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Seu nome Completo">
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div><!-- form-group -->
                            </div><!-- col -->
                        </div>
                        <div class="d-flex">
                            <div class="flex-fill mx-5">
                                <div class="form-group{{ $errors->has('document') ? ' has-error' : '' }}">
                                    <label for="document" class="control-label">CPF/CNPJ válido (<strong class="text-danger">*</strong>)</label>
                                    <input id="document" type="text" class="form-control" name="document" value="{{ old('document') }}" required placeholder="CPF/CNPJ Válido">
                                    @if ($errors->has('document'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('document') }}</strong>
                                    </span>
                                    @endif
                                </div><!-- form-group -->
                            </div><!-- col -->
                            <div class="flex-fill mx-5">
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="control-label">Telefone/WhatsApp (<strong class="text-danger">*</strong>)</label>
                                    <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required placeholder="Telefone/WhatsApp">
                                    @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div><!-- form-group -->
                            </div><!-- col -->
                        </div><!-- d-flex -->

                        <hr>

                        <h4>Dados de Acesso</h4>
                        <div class="d-flex">
                            <div class="flex-fill mx-5">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-mail válido (<strong class="text-danger">*</strong>)</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="E-mail de acesso">
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div><!-- form-group -->
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="flex-fill mx-5">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Senha (<strong class="text-danger">*</strong>)</label>
                                    <input id="password" type="password" class="form-control" name="password" required>
                                    <span class="help-block">Mínimo de 5 caracteres.</span>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div><!-- form-group -->
                            </div><!-- col -->
                            <div class="flex-fill mx-5">
                                <div class="form-group">
                                    <label for="password-confirm" class="control-label">Confirma senha (<strong class="text-danger">*</strong>)</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div><!-- form-group -->
                            </div><!-- col -->
                        </div><!-- -->

                        <div class="form-group mx-4">
                            <button type="submit" class="btn btn-thema btn-lg btn-block"><i class="fa fa-check"></i> Seguir para efetuar o cadastro do seu negócio</button>
                            <hr>
                            <p><b>Você já possui uma conta? <a href="/minha-conta/login">Faça seu login</a></b></p>
                        </div>


                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('jsPage')
<script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
<script>
    var options = {
        onKeyPress: function(cpfcnpj, e, field, options) {
            var masks = ['000.000.000-009', '00.000.000/0000-00'];
            var mask = (cpfcnpj.length > 14) ? masks[1] : masks[0];
            $('#cpf_cnpj').mask(mask, options);
        }
    };

    $('#cpf_cnpj').mask('000.000.000-009', options);
</script>
@endsection