@extends('front.base')
@section('title', 'Escolha um Upgrade')
@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/minha-conta') }}">Minha Conta</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('elements.messages')
          <div class="row">
            @include('front.customers.includes.menu-account')

            <div class="col-sm-12 col-md-9">
              <div class="panel-account">
                <h2>@yield('title')</h2>

                <div class="">
                  <p>Você pode escolher upgrade adicional para seu estabelecimento. Confira o upgrade disponível.</p>
                  <div class="">
                    <div class="plans-box">
                      <div class="d-flex flex-column flex-sm-column flex-md-row flex-lg-row">
                        @foreach($upgrades as $upgrade)
                        <div id="plan-{{ $upgrade->id }}" class="flex-fill panel panel-default">
                          <form action="{{ route('customer.plan.upgradespayment', $estabelecimento_id ) }}" name="plan-{{ $upgrade->id }}" id="plan-{{ $upgrade->id }}" class="panel-body">
                            <h2>{{ $upgrade->name }}</h2>

                            <div class="price">
                              <p class="price_plan">R$ {{ $upgrade->price }}</p>
                              <input type="hidden" name="price" id="price" value="{{ $upgrade->price }}">
                              <input type="hidden" name="plan_id" id="plan_id" value="{{ $upgrade->id }}">

                              <select name="select_period" id="select_period" class="form-control" onclick="someValue({{ $upgrade->id }})">
                                <option value="{{ $upgrade->period }}" data-pricemonth="{{ $upgrade->price }}">{{ $upgrade->period }} dias</option>
                              </select>
                            </div>
                            <div class="info">
                              @if($upgrade->description)
                              <p><b>Benefícios:</b> {{ $upgrade->description }}</p>
                              @endif
                              @if($upgrade->statistics == '1')
                              <p>Estatísticas: Ativado</p>
                              @endif
                            </div>
                            <div class="buttons">
                              <button type="button" data-toggle="modal" data-target="#exampleModal-{{ $upgrade->id }}" class="btn btn-thema"><i class="fa fa-check"></i> Quero esse!</button>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal-{{ $upgrade->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ $upgrade->name }} - Escolha o Método de Pagamento</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div class="row">
                                      @if($configs->payment_banktransfer_status == 1)
                                      <div class="col-xs-12 col-md-12">
                                        <div class="radio radio_payment_method">
                                          <label>
                                            <input type="radio" name="plan_method" id="plan_method" value="banktransfer" required autofocus>
                                            <div class="info-bank">
                                              <div class="panel panel-default">
                                                <div class="panel-body">
                                                  <div class="iconChecked">
                                                    <i class="fa fa-check"></i>
                                                  </div>
                                                  <h5>Depósito/Transf. Bancária</h5>
                                                  {!! html_entity_decode($configs->payment_banktransfer_info) !!}
                                                  <p>Em caso de preferência por depósito, TED ou transferência por favor entrar em contato pelo email: <a href="mailto:{{ $configs->config_email }}">{{ $configs->config_email }}</a>.</p>
                                                </div>
                                              </div>
                                            </div>
                                          </label>
                                        </div>
                                      </div>
                                      @endif
                                      <div class="col-xs-12 col-md-12">
                                        <div class="radio radio_payment_method">
                                          <label>
                                            <input type="radio" name="plan_method" id="plan_method" value="mercadopago">
                                            <div class="info-mercadopago">
                                              <div class="panel panel-default">
                                                <div class="panel-body">
                                                  <div class="iconChecked">
                                                    <i class="fa fa-check"></i>
                                                  </div>
                                                  <h5>Mercado Pago - Cartão & Boleto</h5>
                                                  <p>Realize o pagamento através de Cartão de Crédito ou Boleto Bancário.</p>
                                                  <img src="{{ asset('galerias/payment/mercadopago.png') }}" alt="Mercado Pago" class="img-responsive">

                                                </div>
                                              </div>
                                            </div>
                                          </label>
                                        </div>
                                      </div>
                                    </div><!-- row -->
                                    <div class="row">
                                      <div class="col-xs-12 col-md-12">
                                        <div class="checkbox">
                                          <label>
                                            <input type="checkbox" name="checked_terms" id="checked_terms" value="1" required> Ao contratar o plano, <br> <b>eu declaro que li e aceito os <a href="javascript:;" class="openTerms">Termos de Contrato</a>.</b>
                                          </label>
                                        </div>

                                        <div class="boxTerms hide">
                                          <div class="button_closed openTerms"><i class="fa fa-times"></i></div>
                                          @foreach($pageTerms as $pageTerm)
                                          {!! html_entity_decode($pageTerm->body) !!}
                                          @endforeach
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-thema" id="button-plan-{{ $upgrade->id }}"><i class="fa fa-check"></i> Finalizar</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
                                  </div>
                                </div>
                              </div>
                            </div><!-- end modal -->
                            <!-- Modal -->
                          </form>
                        </div>
                        @endforeach
                        </ul>
                      </div>

                    </div>
                  </div>

                </div><!-- panel-account -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection


  @section('jsPage')
  <script>
    $('.openTerms').click(function() {
      console.log($('.boxTerms'));
      $('.boxTerms').toggleClass('hide');
    });

    // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
    $("form").submit(function(e) {
      //disable the submit button
      $("form .modal-footer button").attr("disabled", true);
      $('form .modal-body').append('<i class="fa fa-spinner fa-spin ml-3"></i>');
      setTimeout(function() {
        $('form .modal-footer button').prop("disabled", false);
        $('form .modal-body').parent().find('svg.fa-spinner').html('');
      }, 4000);
    });

    // $('form button[type="submit"]').click(function() {
    //   $(this).prop("disabled",true);
    //   $(this).before("<i class='fas fa-spinner fa-spin mr-3'></i>");
    //   setTimeout(function(){
    //     $('form button[type="submit"]').prop('disabled', false);
    //     $('form .modal-footer svg.fa-spinner').remove();
    //   }, 2000);
    // });

    /*  // Function de Soma de valores das opções de acordo quando é marcado
     onclick="someValue( $establishment->establishment_id )"
     function someValue(idplan) {
       // Criar a var que armazena o valor do produto original e retira cifrao e pontuacao
       var priceProduct = Number($('#plan-' + idplan + ' .price .price_plan').text().replace("R$ ", "").replace(",", "."));

       // Quando um item foi click aciona a funcao
       $('#plan-' + idplan + ' .price select#select_period').click(function() {
         var target = $('#plan-' + idplan + ' .price select#select_period').children('option:selected');

         // Quando um input for marcado faz a soma do valor do produto
         var total = $('#plan-' + idplan + ' .price select#select_period option:selected').get().reduce(function(tot, el) {
           if (el.dataset.pricemonth) {
             return Number(el.dataset.pricemonth);
           } else {
             return tot;
           }
         }, priceProduct);

         // Formata o valor da soma e insere em uma var
         var totalFormated = total.toLocaleString('pt-BR', {
           style: 'currency',
           currency: 'BRL'
         });

         // Aplica no HTML o novo valor
         $('#plan-' + idplan + ' .price input[name="price"]').val(total);
         $('#plan-' + idplan + ' .price .price_plan').text(totalFormated);

       });
     } */
  </script>
  @endsection