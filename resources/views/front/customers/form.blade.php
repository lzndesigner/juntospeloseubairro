{!! csrf_field() !!}

<h4>Informações Pessoais</h4>

<div class="row">
  <div class="col-xs-12 col-md-12">
    <div class="form-group @if ($errors->has('name')) has-error @endif">
      <label for="name" class=" control-label">Nome</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ $customer->name ?? old('name') }}" placeholder="Nome" autofocus>
      @if ($errors->has('name'))
      <span class="help-block">
        <strong>{{ $errors->first('name') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>
</div>

<div class="row">
<div class="col-xs-12 col-md-6">
    <div class="form-group @if ($errors->has('document')) has-error @endif">
      <label for="document" class=" control-label">CPF/CNPJ</label>
      <input type="text" class="form-control" id="document" name="document" value="{{ $customer->document ?? old('document') }}" placeholder="CPF/CNPJ" readonly>
      @if ($errors->has('document'))
      <span class="help-block">
        <strong>{{ $errors->first('document') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>
  <div class="col-xs-12 col-md-6">
    <div class="form-group @if ($errors->has('phone')) has-error @endif">
      <label for="phone" class=" control-label">Telefone/WhatsApp (*)</label>
      <input type="text" class="form-control" id="phone" name="phone" value="{{ $customer->phone ?? old('phone') }}" placeholder="Telefone/WhatsApp">
      @if ($errors->has('phone'))
      <span class="help-block">
        <strong>{{ $errors->first('phone') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>
</div><!-- row -->
<hr>

<h4>Dados de Acesso</h4>

<div class="form-group @if ($errors->has('email')) has-error @endif">
  <label for="email" class=" control-label">E-mail</label>
  <input type="text" class="form-control" id="email" name="email" value="{{ $customer->email ?? old('email') }}" placeholder="E-mail">
  @if ($errors->has('email'))
  <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="row">
  <div class="col-xs-12 col-md-6">
    <div class="form-group">
      <label for="password" class=" control-label">Senha</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="******">
      <span class="help-block"><b>Deixe em branco</b>, caso não deseja alterar a senha. Mínimo de 5 caracteres.</span>
      @if ($errors->has('password'))
      <span class="help-block">
        <strong>{{ $errors->first('password') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div>

  <div class="col-xs-12 col-md-6">
    <div class="form-group">
      <label for="password" class=" control-label">Confirmar Senha</label>
      <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="******">
    </div><!-- form-group -->
  </div>
</div>

<div class="form-group mt-5">
  <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i> Salvar alterações</button>
  <a href="{{ route('editar.index') }}" class="btn btn-default"><i class="fa fa-reply"></i> Cancelar e voltar</a>
</div><!-- form-group -->