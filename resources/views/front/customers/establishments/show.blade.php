@extends('front.base')
@section('title', 'Detalhes do Estabelecimento')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $establishment->name }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('establishments.index') }}">Página de Estabelecimentos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End establishment Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $establishment->name }}</h2>
            <h4>{{ $establishment->getPostCategory() }}</h4>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $establishment->image }}" alt="Calendário" class="img-thumbnail">

            <p><small>Criado em {{ $establishment->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $establishment->updated_at->format('d/m/Y H:i:s') }}</small></p>

            <blockquote>
              <p><b>Localização</b></p>
              <ul>
                <li>Proprietário: {{ $establishment->proprietary }}</li>
                <li>CEP: {{ $establishment->cep }}</li>
                <li>Endereço: {{ $establishment->address }}, N° {{ $establishment->number }}</li>
                <li>Bairro: {{ $establishment->distric }}</li>
                <li>Cidade: {{ $establishment->city }}</li>
                <li>Estado: {{ $establishment->state }}</li>
                <li>Telefone: {{ $establishment->phone }}</li>
                @if($establishment->cellphone)
                <li>Celular: {{ $establishment->cellphone }}</li>
                @endif

                <li>Status: <span class="label label-{{ ($establishment->status == '1' ? 'success' : 'danger' ) }}">
                    {{ ($establishment->status == '1' ? 'Habilitado' : 'Desabilitado' ) }}
                  </span></li>
              </ul>
            </blockquote>

            @if($establishment->description)
            <blockquote>
              {{ $establishment->description }}
            </blockquote>
            @endif

            <hr>

            <a href="{{ route('estabelecimento.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('estabelecimento.edit', $establishment->id) }}" class="btn btn-xs btn-primary">Editar</a>
          </div><!-- panel-body -->


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection