{!! csrf_field() !!}

{{ $slot }}

<p class="text-danger mb-5"><b>ATENÇÃO: se sua foto não aparecer, vá nas configurações do seu celular e habilite o acesso à suas fotos.</b></p>

<input type="hidden" name="customer_id" id="customer_id" value="{{$currentUser->id}}">
<input type="hidden" name="featured" id="featured" value="0">

<!-- <div id="description" class="tab-pane active"> -->
<div class="heading-block">
  <h4>Informações do Estabelecimento</h4>
</div>

<div class="row">
  <div class="col-xs-12 col-md-6">
    <div class="form-group @if ($errors->has('name')) has-error @endif">
      <label for="name" class="form-label">Nome do Estabelecimento (<strong class="text-danger">*</strong>)</label>
      <input type="text" class="form-control" id="name" name="name" required value="{{ $establishment->name ?? old('name') }}" placeholder="Nome do Estabelecimento">
      @if ($errors->has('name'))
      <span class="help-block">
        <strong>{{ $errors->first('name') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

  <div class="col-xs-12 col-md-3">
    <div class="form-group @if ($errors->has('category_id')) has-error @endif">
      <label for="category_id" class="form-label">Nome da Categoria (<strong class="text-danger">*</strong>)</label>
      <select class="form-control" id="category_id" name="category_id">
        @if(isset($establishment->category_id))
        <optgroup label="Escolhido">
          <option value="{{ $establishment->category_id }}" selected>{{ $establishment->getPostCategory() }}</option>
        </optgroup>
        @endif

        <optgroup label="Escolha uma Opção">
          @foreach($categories as $category)
          <option value="{{ $category->id }}">{{ $category->title }}</option>
          @endforeach
        </optgroup>
      </select>

      @if ($errors->has('category_id'))
      <span class="help-block">
        <strong>{{ $errors->first('category_id') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

  <div class="col-xs-12 col-md-3">
    <div class="form-group @if ($errors->has('type')) has-error @endif">
      <label for="type" class="form-label">Tipo de Estabelecimento (<strong class="text-danger">*</strong>)</label>
      <select class="form-control" id="type" name="type">
        @if(isset($establishment->type))
        @if($establishment->type == 'Físico' )
        <option value="Físico" selected>Físico</option>
        <option value="Virtual">Virtual</option>
        <option value="Físico e Virtual">Físico e Virtual</option>
        @elseif($establishment->type == 'Virtual' )
        <option value="Físico">Físico</option>
        <option value="Virtual" selected>Virtual</option>
        <option value="Físico e Virtual">Físico e Virtual</option>
        @else
        <option value="Físico">Físico</option>
        <option value="Virtual">Virtual</option>
        <option value="Físico e Virtual" selected>Físico e Virtual</option>
        @endif
        @else
        <option value="Físico">Físico</option>
        <option value="Virtual">Virtual</option>
        <option value="Físico e Virtual">Físico e Virtual</option>
        @endif
      </select>

      @if ($errors->has('type'))
      <span class="help-block">
        <strong>{{ $errors->first('type') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

  <hr class="invisible">



  <div class="col-xs-12 col-md-12">
    <div class="form-group ">
      <label for="slug" class="form-label">Link Gerador do seu Anúncio (<strong class="text-danger">*</strong>)</label>
      <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">https://juntospeloseubairro.com.br/categorias/<span class="categoriaselected"></span>/</span>
        <div class="@if ($errors->has('slug')) has-error @endif">
          <input type="text" class="form-control" id="slug" name="slug" required value="{{ $establishment->slug ?? old('slug') }}" onkeyup="this.value=this.value.replace(' ', '-')" placeholder="Link direto para meu Estabelecimento">
        </div>
      </div>
      <span class="help-block">Preenchimento automático.</span>
      @if(isset($establishment->slug))
      <div class="help-block" style="padding:5px 0;"><b>Sua URL:</b>
        <div id="slugcopy" style="display:inline-block;">https://juntospeloseubairro.com.br/categorias/<span class='categoriaselected'></span>/<span class="establishmentname">{{ $establishment->slug ?? old('slug') }}</span></div>
        <button type="button" onclick="copyToClipboard('#slugcopy')" class="btn btn-xs btn-thema btn-icon" data-toggle="tooltip" data-original-title="Copiar URL"><i class="fa fa-copy"></i></button>
        <div id="slugcopied"></div>
      </div>
      @endif

      @if ($errors->has('slug'))
      <div class="has-error">
        <span class="help-block">
          <strong>{{ $errors->first('slug') }}</strong>
        </span>
      </div>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
</div><!-- row -->

<hr>

<div class="heading-block">
  <h4>Endereço do Estabelecimento</h4>
  <br>
  <p class="text-danger"><b>Estabelecimento Tipo Virtual não exibe para o consumidor o Endereço ou Mapa no site.</b></p>
  <p><b>Digite o CEP e aguarde o preenchimento.</b></p>
</div>

<div class="row">
  <div class="col-xs-12 col-md-2">
    <div class="form-group @if ($errors->has('cep')) has-error @endif">
      <label for="cep" class="form-label">CEP (<strong class="text-danger">*</strong>)</label>
      <input type="text" class="form-control" id="cep" name="cep" required value="{{ $establishment->cep ?? old('cep') }}" placeholder="_____-___" {{ (isset($establishment->cep) ? 'readonly' : '')}}>
      @if ($errors->has('cep'))
      <span class="help-block">
        <strong>{{ $errors->first('cep') }}</strong>
      </span>
      @endif
      <span class="help-block">
        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_Blank"><b>Não sabe seu CEP?</b></a>
      </span>
    </div><!-- form-group -->
  </div><!-- cols -->
  <div class="col-xs-12 col-md-7">
    <div class="form-group @if ($errors->has('address')) has-error @endif">
      <label for="address" class="form-label">Endereço Completo (<strong class="text-danger">*</strong>)</label>
      <input type="text" class="form-control" id="address" name="address" required value="{{ $establishment->address ?? old('address') }}" placeholder="Endereço">
      @if ($errors->has('address'))
      <span class="help-block">
        <strong>{{ $errors->first('address') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
  <div class="col-xs-12 col-md-2">
    <div class="form-group @if ($errors->has('number')) has-error @endif">
      <label for="number" class="form-label">Número <span id="reqnumber"></span></label>
      <input type="text" class="form-control" id="number" name="number" value="{{ $establishment->number ?? old('number') }}" placeholder="Número">
      @if ($errors->has('number'))
      <span class="help-block">
        <strong>{{ $errors->first('number') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
</div><!-- row -->
<div class="row">
  <div class="col-xs-12 col-md-12">
    <div class="form-group @if ($errors->has('complement')) has-error @endif">
      <label for="complement" class="form-label">Complemento</label>
      <input type="text" class="form-control" id="complement" name="complement" value="{{ $establishment->complement ?? old('complement') }}" placeholder="Complemento">
      @if ($errors->has('complement'))
      <span class="help-block">
        <strong>{{ $errors->first('complement') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
</div><!-- row -->
<div class="row">
  <div class="col-xs-12 col-md-3">
    <div class="form-group @if ($errors->has('distric')) has-error @endif">
      <label for="distric" class="form-label">Bairro (<strong class="text-danger">*</strong>)</label>
      <input type="text" class="form-control" id="distric" name="distric" value="{{ $establishment->distric ?? old('distric') }}" placeholder="Digite o Bairro" required>
      <span class="text-danger"><b>Importante: Qual Bairro deseja ser localizado?</b></span>
      @if ($errors->has('distric'))
      <span class="help-block">
        <strong>{{ $errors->first('distric') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
  <div class="col-xs-12 col-md-3">
    <div class="form-group @if ($errors->has('city')) has-error @endif">
      <label for="city" class="form-label">Cidade (<strong class="text-danger">*</strong>)</label>
      <input type="text" class="form-control" id="city" name="city" value="{{ $establishment->city ?? old('city') }}" readonly placeholder="Cidade">
      @if ($errors->has('city'))
      <span class="help-block">
        <strong>{{ $errors->first('city') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
  <div class="col-xs-12 col-md-3">
    <div class="form-group @if ($errors->has('state')) has-error @endif">
      <label for="state" class="form-label">Estado (<strong class="text-danger">*</strong>)</label>
      <input type="text" class="form-control" id="state" name="state" value="{{ $establishment->state ?? old('state') }}" readonly placeholder="Estado">
      @if ($errors->has('state'))
      <span class="help-block">
        <strong>{{ $errors->first('state') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
  <div class="col-xs-12 col-md-3">
    <div class="form-group @if ($errors->has('country')) has-error @endif">
      <label for="country" class="form-label">País (<strong class="text-danger">*</strong>)</label>
      <input type="text" class="form-control" id="country" name="country" value="Brasil" readonly placeholder="País">
      @if ($errors->has('country'))
      <span class="help-block">
        <strong>{{ $errors->first('country') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
</div><!-- row -->

<hr>

<div class="row">
  <div class="col-xs-12 col-md-12">
    <div class="heading-block">
      <h4>Formas de Contato</h4>
    </div>

    <div class="row">
      <div class="col-xs-12 col-md-6">
        <div class="form-group @if ($errors->has('email')) has-error @endif">
          <label for="email" class="form-label">E-mail de Contato</label>
          <input type="email" class="form-control" id="email" name="email" value="{{ $establishment->email ?? old('email') }}" placeholder="E-mail de Contato">
          @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
          @endif
        </div><!-- form-group -->
      </div><!-- col-md-6 -->

      <div class="col-xs-12 col-md-6">
        <div class="form-group @if ($errors->has('website')) has-error @endif">
          <label for="website" class="form-label">Link do Site</label>
          <input type="text" class="form-control" id="website" name="website" value="{{ $establishment->website ?? old('website') }}" placeholder="Link do Site">
          @if ($errors->has('website'))
          <span class="help-block">
            <strong>{{ $errors->first('website') }}</strong>
          </span>
          @endif
        </div><!-- form-group -->
      </div><!-- col-md-6 -->
    </div><!-- row -->


    <div class="row">
      <div class="col-xs-12 col-md-6">
        <div class="form-group @if ($errors->has('phone')) has-error @endif">
          <label for="phone" class="form-label">Telefone/Celular (DDD) (<strong class="text-danger">*</strong>)</label>
          <input type="text" class="form-control" id="phone" name="phone" required value="{{ $establishment->phone ?? old('phone') }}" placeholder="Telefone/Celular (DDD)">
          @if ($errors->has('phone'))
          <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
          </span>
          @endif

          <div class="checkbox2">
            <label>
              @if(isset($establishment->phone_whatsapp))
              @if($establishment->phone_whatsapp == 1)
              <input type="checkbox" name="phone_whatsapp" id="phone_whatsapp" value="1" checked>
              @else
              <input type="checkbox" name="phone_whatsapp" id="phone_whatsapp" value="0">
              @endif
              @else
              <input type="checkbox" name="phone_whatsapp" id="phone_whatsapp" value="1">
              @endif
              Telefone é WhatsApp?
            </label>
          </div>
        </div><!-- form-group -->
      </div><!-- cols -->
      <div class="col-xs-12 col-md-6">
        <div class="form-group @if ($errors->has('cellphone')) has-error @endif">
          <label for="cellphone" class="form-label">Telefone/Celular (DDD)</label>
          <input type="text" class="form-control" id="cellphone" name="cellphone" value="{{ $establishment->cellphone ?? old('cellphone') }}" placeholder="Telefone/Celular (DDD)">
          @if ($errors->has('cellphone'))
          <span class="help-block">
            <strong>{{ $errors->first('cellphone') }}</strong>
          </span>
          @endif
        </div><!-- form-group -->
      </div><!-- cols -->
    </div><!-- row -->

    <div class="row hide">
      <div class="col-xs-12 col-md-4">
        <div class="form-group @if ($errors->has('status')) has-error @endif">
          <label for="status" class="form-label">Status do Estabelecimento</label>
          <select class="form-control" id="status" name="status">
            @if(isset($establishment->status))
            @if($establishment->status == 0 )
            <option value="0" selected>Desabilitado</option>
            <option value="1">Habilitar</option>
            @else
            <option value="1" selected>Habilitado</option>
            <option value="0">Desabilitar</option>
            @endif
            @else
            <option value="1" selected>Habilitado</option>
            <option value="0">Desabilitado</option>
            @endif
          </select>

          @if ($errors->has('status'))
          <span class="help-block">
            <strong>{{ $errors->first('status') }}</strong>
          </span>
          @endif
        </div><!-- form-group -->
      </div><!-- cols -->
    </div>

  </div><!-- col -->
  <div class="col-xs-12 col-md-12">
    <div class="heading-block">
      <h4>Redes Sociais</h4>
    </div>
    <div class="col-xs-12 col-md-12">
      <div class="form-group @if ($errors->has('redesocial_facebook')) has-error @endif">
        <label for="redesocial_facebook" class="form-label">Facebook</label>
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">https://facebook.com/</span>
          
          <input type="text" class="form-control" id="redesocial_facebook" name="redesocial_facebook" value="@if(isset($establishment->redesocial_facebook)){{ str_replace('https://www.facebook.com/', '', $establishment->redesocial_facebook) }}@else{{ old('redesocial_facebook') }}@endif" placeholder="suapagina">
          
        </div>
        @if ($errors->has('redesocial_facebook'))
        <span class="help-block">
          <strong>{{ $errors->first('redesocial_facebook') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div>

    <div class="col-xs-12 col-md-12">
      <div class="form-group @if ($errors->has('redesocial_instagram')) has-error @endif">
        <label for="redesocial_instagram" class="form-label">Instagram</label>
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">https://instagram.com/</span>
          <input type="text" class="form-control" id="redesocial_instagram" name="redesocial_instagram" value="@if(isset($establishment->redesocial_instagram)){{ str_replace('https://www.instagram.com/', '', $establishment->redesocial_instagram) }}@else{{ old('redesocial_instagram') }}@endif" placeholder="suapagina">
        </div>
        @if ($errors->has('redesocial_instagram'))
        <span class="help-block">
          <strong>{{ $errors->first('redesocial_instagram') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div>

    <div class="col-xs-12 col-md-12">
      <div class="form-group @if ($errors->has('redesocial_twitter')) has-error @endif">
        <label for="redesocial_twitter" class="form-label">Twitter</label>
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">https://twitter.com/</span>
          <input type="text" class="form-control" id="redesocial_twitter" name="redesocial_twitter" value="@if(isset($establishment->redesocial_twitter)){{ str_replace('https://www.twitter.com/', '', $establishment->redesocial_twitter) }}@else{{ old('redesocial_twitter') }}@endif" placeholder="seuperfil">
        </div>
        @if ($errors->has('redesocial_twitter'))
        <span class="help-block">
          <strong>{{ $errors->first('redesocial_twitter') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div>

    <div class="col-xs-12 col-md-12">
      <div class="form-group @if ($errors->has('redesocial_linkedin')) has-error @endif">
        <label for="redesocial_linkedin" class="form-label">linkedin</label>
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">https://linkedin.com/</span>
        <input type="text" class="form-control" id="redesocial_linkedin" name="redesocial_linkedin" value="@if(isset($establishment->redesocial_linkedin)){{ str_replace('https://www.linkedin.com/', '', $establishment->redesocial_linkedin) }}@else{{ old('redesocial_linkedin') }}@endif" placeholder="seuperfil">
        </div>
        <span class="help-block">Usar <b>in/</b> para perfil pessoal ou <b>company/</b> para perfil comercial</span>
        @if ($errors->has('redesocial_linkedin'))
        <span class="help-block">
          <strong>{{ $errors->first('redesocial_linkedin') }}</strong>
        </span>
        @endif
      </div><!-- form-group -->
    </div>
  </div><!-- col -->
</div><!-- row -->

<hr>

<div class="heading-block">
  <h4>Descrições do Estabelecimento</h4>
  <p class="text-danger"><b>Encante o consumidor e fale sobre sua empresa, produtos, serviços, seus diferenciais e sobre seu atendimento - o que for importante para que você gere conexão com seu potencial novo cliente. </b></p>
  <p class="text-danger"><b>Esta descrição é MUITO importante, pois é através dela que seu estabelecimento irá aparecer nas buscas feitas por consumidores. Obrigada e Sucesso</b></p>
</div>

<div class="row">
  <div class="col-xs-12 col-md-12">
    <div class="form-group @if ($errors->has('description')) has-error @endif">
      <textarea name="description" id="description" cols="30" rows="6" class="form-control summernote" placeholder="Descrição ou serviços prestados...">{{ $establishment->description ??  old('description') }}</textarea>
      @if ($errors->has('description'))
      <span class="help-block">
        <strong>{{ $errors->first('description') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
</div><!-- row -->

<hr>

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i> Salvar Estabelecimento</button>
    <a href="{{ route('estabelecimentos.index') }}" class="btn btn-default"><i class="fa fa-reply"></i> Cancelar e voltar</a>
  </div><!-- col-md-6 -->
</div>