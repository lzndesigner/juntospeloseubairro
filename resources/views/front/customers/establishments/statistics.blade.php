@extends('front.base')
@section('title', 'Minhas Estatísticas')
@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/minha-conta/estabelecimentos') }}">Meus Estabelecimentos</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('elements.messages')
          <div class="row">
            @include('front.customers.includes.menu-account')

            <div class="col-sm-12 col-md-9">
              <div class="panel-account">

                <h1>@yield('title') ({{ count($establishments) }})</h1>

                <hr>

                <div class="table-responsive">
                  @if(count($establishments) > 0)
                  <table class="table table-hover table-bordered table-striped" id="establishments-table">
                    <thead>
                      <tr>
                        <td width="2%">#</td>
                        <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'name']) }}">Estabelecimento</a></td>
                        <td>Categoria</td>
                        <td>Visualizações</td>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($establishments as $establishment)
                      <tr id="row-{{ $establishment->id }}">
                        <td>
                          <b>{{ $establishment->establishment_id }}</b>
                        </td>
                        <td><a href="/categorias/{{$establishment->categoriaslug}}/{{$establishment->esabelecimenoslug}}">{{ $establishment->estabelecimento }}</a></td>
                        <td>{{ $establishment->categoria }}</td>
                        <td>{{ $establishment->views }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  @else
                  <div class="alert alert-info">Nenhum estabelecimento com estatisticas habilitado.</div>
                  @endif

                </div><!-- table-responsive -->

                <div class="row">
                  <div class="col-md-6 text-right">
                    {{ $establishments->links() }}
                  </div><!-- col-md-6 -->
                </div><!-- row -->

              </div><!-- panel-account -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection