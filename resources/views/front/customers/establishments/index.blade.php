@extends('front.base')
@section('title', 'Meus Estabelecimentos')
@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/minha-conta') }}">Minha Conta</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('elements.messages')
          <div class="row">
            @include('front.customers.includes.menu-account')

            <div class="col-sm-12 col-md-9">
              <div class="panel-account">

                <h1>@yield('title') ({{ count($establishments) }})
                  <button type="button" class="btn btn-xs btn-thema pull-right" data-toggle="modal" data-target="#exampleModal">Ver Upgrades <i class="fa fa-plus"></i></button>
                </h1>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                        <h4 class="mb-5">Faça um upgrade na visibilidade do seu estabelecimento, <br> coloque-o em destaque ou como um outdoor na página inicial.</h4>
                        <div class="plans-box">
                          <div class="d-flex flex-column flex-sm-column flex-md-row flex-lg-row text-center justify-content-center">
                            @foreach($upgrades as $upgrade)
                            <div id="plan-{{ $upgrade->id }}" class="col-md-5 mx-4 panel panel-default">
                              <form action="{{ route('estabelecimentos.index') }}" name="plan-{{ $upgrade->id }}" id="plan-{{ $upgrade->id }}" class="panel-body">
                                <h2>{{ $upgrade->name }}</h2>

                                @php
                                $valor = $upgrade->price;
                                switch ($upgrade->period) {
                                case 30:
                                $period = 1;
                                break;
                                case 90:
                                $period = 3;
                                break;
                                case 180:
                                $period = 6;
                                break;
                                case 360:
                                $period = 12;
                                break;
                                }
                                $pricemensal = $valor / $period;
                                @endphp

                                <div class="price">
                                  <p class="price_plan hide">R$ {{ $upgrade->price }}</p>
                                  @if($upgrade->price > 0)
                                  <p class="price_plan">R$ {{ number_format($pricemensal, 2, ',', '') }}/mês</p>
                                  @else
                                  <p>Grátis</p>
                                  @endif
                                  <input type="hidden" name="price" id="price" value="{{ $upgrade->price }}">
                                  <input type="hidden" name="plan_id" id="plan_id" value="{{ $upgrade->id }}">

                                  <select name="select_period" id="select_period" class="form-control" disabled onclick="someValue({{ $upgrade->id }})">
                                    <option value="{{ $upgrade->period }}" data-pricemonth="{{ $upgrade->price }}">{{ $upgrade->period }} dias</option>
                                  </select>
                                </div>
                                <div class="info">
                                  @if($upgrade->description)
                                  {!! html_entity_decode(str_replace('*', '<i class="fa fa-check-circle text-success mr-2"></i>', $upgrade->description)) !!}
                                  @endif
                                </div>
                              </form>
                            </div>
                            @endforeach
                          </div>
                        </div>
                        <div class="text-center">
                          <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
                          <h4>Escolha um Estabelecimento <br>
                            Ou anuncie agora mesmo seu Estabelecimento</h4>
                          <div class="d-flex justify-content-center">
                            <div class="mx-4"><button type="button" data-dismiss="modal" class="btn btn-lg btn-thema mt-4"><i class="fa fa-building"></i> Escolher um</button></div>
                            <div class="mx-4"><a href="{{ url('/minha-conta/estabelecimentos/create') }}" class="btn btn-lg btn-thema mt-4"><i class="fa fa-plus"></i> Novo Estabelecimento</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal -->
                <div class="table-responsive">
                  @if(count($establishments) > 0)
                  <table class="table table-hover table-bordered table-striped" id="establishments-table">
                    <thead>
                      <tr>
                        <td width="8%">Info</td>
                        <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'name']) }}">Estabelecimento</a></td>
                        <td>Plano</td>
                        <td>Vencimento</td>
                        <td width="8%">Status</td>
                        <td width="20%" class="text-right">Ações</td>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($establishments as $establishment)
                      @php
                        $dateWarning = \Carbon\Carbon::parse($establishment->plan_dateend)->subDay('7')->format('Y-m-y');
                        $dateNow = \Carbon\Carbon::now()->format('Y-m-y');
                        $unlockRenew = false;
                        if(strtotime($dateWarning) <= strtotime($dateNow)){
                          $unlockRenew = true;
                        }
                      @endphp

                      <tr id="row-{{ $establishment->id }}" class="{{ $unlockRenew ? 'table-warning' : '' }}">
                        <td>
                          @if($establishment->plano)
                          <button class="btn btn-xs btn-primary" id="info-estab" data-id="{{ $establishment->establishment_id }}" data-toggle="tooltip" data-original-title="Mais Informações"><i class="fa fa-plus"></i></button>
                          @endif
                        </td>
                        <td><small>#{{ $establishment->establishment_id }}</small> <a href="/estabelecimento/{{$establishment->esabelecimenoslug}}">{{ $establishment->estabelecimento }}</a> <br> <small>- {{ $establishment->categoria }}</small></td>
                        <td>{{ $establishment->plano ? $establishment->plano : '-' }}</td>
                        <td>{{ ($establishment->plan_dateend ? \Carbon\Carbon::parse($establishment->plan_dateend)->format('d/m/y') : '-') }}</td>
                        <td>
                          @if($establishment->status == 'Pago')
                          <span class="label label-success">{{$establishment->status}}</span>
                          @elseif($establishment->status == 'Pendente')
                          <span class="label label-warning">{{$establishment->status}}</span>
                          @elseif($establishment->status == 'Aguardando')
                          <span class="label label-primary">{{$establishment->status}}</span>
                          @elseif($establishment->status == 'Expirado')
                          <span class="label label-danger">{{$establishment->status}}</span>
                          @endif
                        </td>
                        <td class="text-right">
                          @if($establishment->status == 'Pendente')
                          <a href="{{ route('customer.plan', $establishment->id) }}" class="btn btn-xs btn-success btn-icon" data-toggle="tooltip" data-original-title="Publicar Estabelecimento">Publicar</a>
                          @elseif($establishment->status == 'Expirado')
                          <a href="{{ route('customer.plan.renew', $establishment->id) }}" class="btn btn-xs btn-danger btn-icon" data-toggle="tooltip" data-original-title="Renovar Plano">Renovar</a>
                          @elseif($establishment->status == 'Pago' && $unlockRenew)
                          <a href="{{ route('customer.plan.renew', $establishment->id) }}" class="btn btn-xs btn-danger btn-icon" data-toggle="tooltip" data-original-title="Renovar Plano">Renovar</a>
                          @elseif($establishment->status == 'Pago' && $establishment->upgrade === 0)
                          <a href="{{ route('customer.plan.upgrades', $establishment->establishment_id) }}" class="btn btn-xs btn-info btn-icon" data-toggle="tooltip" data-original-title="Upgrade">Upgrade</a>
                          @endif

                          @if($establishment->plan_id < 5) <a href="{{ route('estabelecimentos.edit', $establishment->establishment_id) }}" class="btn btn-xs btn-thema btn-icon" data-toggle="tooltip" data-original-title="Editar Estabelecimento"><i class="fa fa-edit"></i> Edita</a>
                            @endif
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  @else
                  <div class="alert alert-info">Nenhum estabelecimento cadastrado.</div>
                  @endif
                </div><!-- table-responsive -->
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                    {{ $establishments->links() }}
                  </div>
                </div>

                <hr>

                @if(count($establishmentsNotplan) > 0)
                <h1>Estabelecimentos sem plano ({{ count($establishmentsNotplan) }})</h1>
                <div class="table-responsive">
                  <table class="table table-hover table-bordered table-striped" id="establishments-table">
                    <thead>
                      <tr>
                        <td width="8%">Info</td>
                        <td>Estabelecimento</td>
                        <td>Plano</td>
                        <td>Vencimento</td>
                        <td width="8%">Status</td>
                        <td width="18%" class="text-right">Ações</td>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($establishmentsNotplan as $establishmentNotplan)

                      <tr id="row-{{ $establishmentNotplan->id }}">
                        <td>
                          @if($establishmentNotplan->plano)
                          <button class="btn btn-xs btn-primary" id="info-estab" data-id="{{ $establishmentNotplan->establishment_id }}" data-toggle="tooltip" data-original-title="Mais Informações"><i class="fa fa-plus"></i></button>
                          @endif
                        </td>
                        <td><small>#{{ $establishmentNotplan->establishment_id }}</small> <a href="/categorias/{{$establishmentNotplan->categoriaslug}}/{{$establishmentNotplan->esabelecimenoslug}}">{{ $establishmentNotplan->estabelecimento }}</a> <br> <small>- {{ $establishmentNotplan->categoria }}</small></td>
                        <td>{{ $establishmentNotplan->plano ? $establishmentNotplan->plano : '-' }}</td>
                        <td>{{ ($establishmentNotplan->plan_dateend ? \Carbon\Carbon::parse($establishmentNotplan->plan_dateend)->format('d/m/y') : '-') }}</td>
                        <td>
                          @if($establishmentNotplan->status == 'Pago')
                          <span class="label label-success">{{$establishmentNotplan->status}}</span>
                          @elseif($establishmentNotplan->status == 'Pendente')
                          <span class="label label-warning">{{$establishmentNotplan->status}}</span>
                          @elseif($establishmentNotplan->status == 'Aguardando')
                          <span class="label label-primary">{{$establishmentNotplan->status}}</span>
                          @elseif($establishmentNotplan->status == 'Expirado')
                          <span class="label label-danger">{{$establishmentNotplan->status}}</span>
                          @endif
                        </td>
                        <td class="text-right">
                          @if($establishmentNotplan->status == 'Pendente')
                          <a href="{{ route('customer.plan', $establishmentNotplan->establishment_id) }}" class="btn btn-xs btn-success btn-icon" data-toggle="tooltip" data-original-title="Publicar Estabelecimento">Publicar</a>
                          @elseif($establishmentNotplan->status == 'Expirado')
                          <a href="{{ route('customer.plan.renew', $establishmentNotplan->id) }}" class="btn btn-xs btn-danger btn-icon" data-toggle="tooltip" data-original-title="Renovar Plano">Renovar</a>
                          @elseif($establishmentNotplan->status == 'Pago' && $establishmentNotplan->upgrade === 0)
                          <a href="{{ route('customer.plan.upgrades', $establishmentNotplan->establishment_id) }}" class="btn btn-xs btn-info btn-icon" data-toggle="tooltip" data-original-title="Upgrade">Upgrade</a>
                          @endif
                          @if($establishmentNotplan->plan_id < 5) <a href="{{ route('estabelecimentos.edit', $establishmentNotplan->establishment_id) }}" class="btn btn-xs btn-thema btn-icon" data-toggle="tooltip" data-original-title="Editar Estabelecimento"><i class="fa fa-edit"></i></a>
                            @endif
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div><!-- table -->
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                    {{ $establishmentsNotplan->links() }}
                  </div>
                </div>
                @endif

                <div class="row">
                  <div class="col-md-12">
                    <hr>
                    <a href="{{ route('estabelecimentos.create') }}" class="btn btn-thema"><i class="fa fa-plus"></i> Novo Estabelecimento</a>
                  </div><!-- col-md-6 -->
                </div><!-- row -->


                <!-- Modal -->
                <div class="modal fade" id="infoEstab" tabindex="-1" role="dialog" aria-labelledby="infoEstab" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                        <h4>Informações do Estabelecimento</h4>
                        <div class="row">
                          <div class="col-xs-12 col-md-12">
                            <div class="panel panel-default">
                              <div class="panel-body" id="result"></div>
                            </div>
                          </div>
                        </div>

                        <h4>Planos Upgrades</h4>

                        <table class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                              <td>Plano</td>
                              <td>Vencimento</td>
                              <td>Status</td>
                              <td></td>
                            </tr>
                          </thead>
                          <tbody id="result2">
                          </tbody>
                        </table>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-thema" data-dismiss="modal">Fechar</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal -->

              </div><!-- panel-account -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('jsPage')
<script>
  $(document).on("click", "#info-estab", function() {
    $("#result, #result2").html('');
    let id = $(this).data('id');
    $("#infoEstab").modal('show');
    var url = `{{url('minha-conta/estabelecimentos/${id}/')}}`;
    //console.log(url);
    $.get(url,
      $(this)
      .addClass('modal-scrollfix')
      .find('.modal-body')
      .html('Carregando...'),
      function(data) {
        // console.log(data);

        //Info Geral
        var result = "";
        $.each(data.geral, function(key, item) {
          // format date
          var data = new Date(item.plan_dateend),
            dia = (data.getDate() + 1).toString(), //+1 pois no getDate começa com zero.
            diaF = (dia.length == 1) ? '0' + dia : dia,
            mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
            mesF = (mes.length == 1) ? '0' + mes : mes,
            anoF = data.getFullYear();
          var datafinal = diaF + "/" + mesF + "/" + anoF;

          result += '<ul class="list-unstyled">';
          result += "<li>Estabelecimento: " + item.estabelecimento + "</li>";
          result += "<li>Proprietário: " + item.cliente + "</li>";
          result += "<li>Plano: " + item.plano + "</li>";
          result += "<li>Vencimento: " + datafinal + "</li>";
          result += "<li>Status: " + item.status + "</li>";
          result += '</ul>';
        });
        $("#result").html(result);

        // Upgrades
        var result2 = "";
        $.each(data.upgrades, function(key, item) {
          // console.log(item);
          // format date
          var data = new Date(item.plan_dateend),
            dia = (data.getDate() + 1).toString(), //+1 pois no getDate começa com zero.
            diaF = (dia.length == 1) ? '0' + dia : dia,
            mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
            mesF = (mes.length == 1) ? '0' + mes : mes,
            anoF = data.getFullYear();
          var datafinal = diaF + "/" + mesF + "/" + anoF;

          
          result2 += '<tr>';
          result2 += `<td>${item.plano}</td>`;
          result2 += `<td>${datafinal}</td>`;
          result2 += `<td>${item.status}</td>`;
          result2 += `<td>`;
          if (item.status == 'Expirado') {
            var url = "{{url("/minha-conta/plano/renovar/")}}" +'/'+ item.id;
            result2 += "<a href='"+url+"' class='btn btn-xs btn-danger btn-icon' data-toggle='tooltip' data-original-title='Renovar Plano'>Renovar</a>";
          }
          result2 += `</td>`;
          result2 += '</tr>';
        });
        $("#result2").html(result2);

      });
  });




  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    $('#slugcopied').append('<div class="alert alert-success" style="width:220px;">Copiado</div>');
    setTimeout(function(e) {
      $('#slugcopied').html('');
    }, 2000, this);
  }
</script>
@endsection