@extends('front.base')
@section('title', 'Editar Meus Dados')

@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/minha-conta') }}">Minha Conta</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-body">
            @include('elements.messages')
          <div class="row">
            @include('front.customers.includes.menu-account')

            <div class="col-sm-12 col-md-9">
              <div class="panel-account">
                <form action="{{ route('editar.update', $customer->id)}}" method="POST">
                  <input type="hidden" name="_method" value="PUT">
                  @include('front.customers.form')
                </form>

                <hr>
                
                <h4>Excluir conta</h4>
                <p>Você pode excluir sua conta definitivamente.</p>
                <p>Ação permitida apenas se não houver nenhum estabelecimento cadastrado.</p>
                <form action="{{ route('customer.destroy', $customer->id)}}" method="POST">
                  <button class="btn btn-sm btn-danger" @if($myestablishments >= 1) disabled @endif><i class="fa fa-times"></i> Excluir definitivamente!</button>
                </form>
              </div><!-- panel-account -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('jsPage')
<script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
<script>
var options = {
        onKeyPress : function(cpfcnpj, e, field, options) {
            var masks = ['000.000.000-009', '00.000.000/0000-00'];
            var mask = (cpfcnpj.length > 14) ? masks[1] : masks[0];
            $('#cpf_cnpj').mask(mask, options);
        }
    };

$('#cpf_cnpj').mask('000.000.000-009', options);
</script>
@endsection