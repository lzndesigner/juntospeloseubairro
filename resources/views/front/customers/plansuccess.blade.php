@extends('front.base')
@section('title', 'Plano Contratado')
@section('breadcrumb')
<div id="section-search" class="section-page">
  <div class="container clearfix">
    <div class="emphasis-title center">
      <h1>@yield('title')</h1>
      <div class="divider divider-short divider-center"><i class="icon-predinhos"></i></div>
    </div>

    <ol class="breadcrumb">
      <li><a href="{{ url('/') }}">Início</a></li>
      <li><a href="{{ url('/minha-conta') }}">Minha Conta</a></li>
      <li class="active">@yield('title')</li>
    </ol>
  </div>
</div><!-- section-home -->
@endsection

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-body">
          @include('elements.messages')
          <div class="row">
            @include('front.customers.includes.menu-account')

            <div class="col-sm-12 col-md-9">
              <div class="panel-account">
                <h1>@yield('title')</h1>
                <h4>Obrigado por realizar sua contratação</h4>
                <p>Para finalizar sua contratação, entre em contato através do e-mail: <a href="mailto:{{ $configs->config_email }}">{{ $configs->config_email }}</a> solicitando instruções para depósito, transferência ou TED. Obrigada.</p>

                <a href="{{ route('estabelecimentos.index', $currentUser->id) }}" class="btn btn-thema">Meus estabelecimentos</a>
              </div><!-- panel-account -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection