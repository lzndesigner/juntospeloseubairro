<div class="sidebar clearfix">

    <ul class="sidebar-panel nav">
        <li class="sidetitle">NAVEGAÇÃO</li>
        <li><a href="{{ url('/') }}" target="_Blank"><span class="icon color5"><i class="fa fa-desktop"></i></span>Meu Site</a></li>
        <li><a href="{{ url('/dashboard') }}"><span class="icon color5"><i class="fa fa-home"></i></span>Painel de Controle</a></li>
        <li><a href="https://sso.godaddy.com/?realm=pass&app=email" target="_Blank"><span class="icon color5"><i class="fa fa-comments"></i></span>Webmail GoDaddy</a></li>
    </ul>

    <ul class="sidebar-panel nav">
        <li class="sidetitle">MEU SITE</li>
        <li><a href="#"><span class="icon color7"><i class="fa fa-cogs"></i></span> Configuração <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('configs.index') }}"><i class="fa fa-cog"></i>Site em Geral</a></li>
                <li><a href="{{ route('users.index') }}"><i class="fa fa-user"></i>Usuários Admin</a></li>
            </ul>
        </li>
        <li><a href="#"><span class="icon color7"><i class="fa fa-bank"></i></span> Catálogo <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('categories.index') }}"><i class="fa fa-bars"></i> Categorias</a></li>
                <li><a href="{{ route('establishments.index') }}"><i class="fa fa-heart"></i> Estabelecimentos</a></li>
            </ul>
        </li>
        </li>
        <li><a href="#"><span class="icon color7"><i class="fa fa-user"></i></span> Clientes <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('customers.index') }}"><i class="fa fa-users"></i> Clientes</a></li>
                <li><a href="{{ route('customerplans.index') }}"><i class="fa fa-address-card"></i> Planos dos Clientes</a></li>
                <li><a href="{{ route('plans.index') }}"><i class="fa fa-cubes"></i> Planos de Assinatura</a></li>
            </ul>
        </li>
        <li><a href="{{ route('transations.index') }}"><span class="icon color7"><i class="fa fa-dollar"></i></span> Transações</a></li>
        <li><a href="{{ route('sliders.index') }}"><span class="icon color7"><i class="fa fa-image"></i></span> Slides</a></li>
        <li><a href="{{ route('apoios.index') }}"><span class="icon color7"><i class="fa fa-heart"></i></span> Apoios</a></li>
        <li><a href="#"><span class="icon color7"><i class="fa fa-book"></i></span> Páginas <span class="caret"></span></a>
            <ul>
                <li><a href="{{ route('about.index') }}"><i class="fa fa-star"></i> Sobre</a></li>
                <li><a href="{{ route('pages.index') }}"><i class="fa fa-book"></i> Página de Informações</a></li>
            </ul>
        </li>
    </ul>

    <ul class="sidebar-panel nav">
        <li class="sidetitle">EXPORTAÇÃO</li>
        <li><a href="{{ route('export.customers') }}"><span class="icon color7"><i class="fa fa-users"></i></span> Clientes</a></li>
        <li><a href="{{ route('export.customers.establishments') }}"><span class="icon color7"><i class="fa fa-bank"></i></span> Clientes + Estab.</a></li>
    </ul>

    <ul class="sidebar-panel nav">
        <li class="sidetitle">OUTROS</li>
        <li><a href="{{ url('https://www.innsystem.com.br/central-ajuda') }}" target="_Blank"><span class="icon color15"><i class="fa fa-bank"></i></span>Central de Ajuda</a></li>
        <li><a href="{{ url('https://www.innsystem.com.br/contato') }}" target="_Blank"><span class="icon color15"><i class="fa fa-tags"></i></span>Ticket de Suporte</a></li>
    </ul>

</div><!-- sidebar -->