@extends('admin.base')
@section('title', 'Transações de Clientes')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li><a href="{{ url('/dashboard/transations') }}">Clientes</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              @if(count($transations) > 0)
              <table class="table table-hover table-bordered table-striped" id="transations-table">
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Cliente</td>
                    <td>Plano</td>
                    <td>Período</td>
                    <td>Método</td>
                    <td>Valor</td>
                    <td>Data</td>
                    <td class="text-right">Ações</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($transations as $transation)
                  <tr id="row-{{ $transation->id }}">
                    <td>
                      <b>{{ $transation->id }}</b>
                    </td>
                    <td>{{ $transation->customer }}</td>
                    <td>{{ $transation->plan_name }}</td>
                    <td>{{ $transation->plan_period }}</td>
                    <td>
                      @if($transation->plan_method == 'banktransfer')
                      Depósito/Transf.
                      @elseif($transation->plan_method == 'mercadopago')
                      Mercado Pago
                      @else
                      Grátis
                      @endif
                    </td>
                    <td>R$ {{ $transation->plan_value }}</td>
                    <td>{{ \Carbon\Carbon::parse($transation->date)->format('d/m/y H:i') }}</td>
                    <td class="text-right">
                      <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" data-id="{{ $transation->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i> Remover?</a>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
              @else
              <div class="alert alert-info">Nenhuma transação cadastrada.</div>
              @endif
              <hr>

            </div><!-- table-responsive -->
          </div><!-- panel-body -->

          <div class="row">
            <div class="col-md-12 text-right">
              {{ $transations->links() }}
            </div><!-- col-md-6 -->
          </div>


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('cssPage')
@endsection

@section('jsPage')
<script>
  $(function() {
    let btn = $('.btn-delete');
    let btnAll = $('.btn-remove-all');

    function remove(id) {
      let tr = $('#transations-table #row-' + id);
      let request = $.ajax({
        url: '/dashboard/transations/' + id,
        type: 'DELETE',
        statusCode: {
          204: function() {
            tr.fadeOut();
          },
          403: function() {
            let copy = tr.innerHTML;

            tr.html('').append('<td colspan="7">Essa transação não foi removida.</td>')
              .css({
                'background-color': 'red',
                'color': 'white',
                'text-align': 'center'
              });
          }
        }
      });
      return request;
    }

    btn.on('click', function(e) {
      e.preventDefault();
      let action = $(e.currentTarget);
      let request = remove(action.data('id'));
      request.fail(function() {
        window.alert('Não foi possivel remover a transação')
      });
    })

    btnAll.on('click', function(e) {
      e.preventDefault();

      let checkbox = $('input:checkbox[class="transations"]:checked');

      if (checkbox.length > 0 && window.confirm('Gostaria de remover a(s) ' + checkbox.length + ' transações ?')) {
        //
        checkbox.each(function(index, el) {
          let id = $(el).data('id')
          let request = remove(id);
        })
      }

    })
  })
</script>
@endsection