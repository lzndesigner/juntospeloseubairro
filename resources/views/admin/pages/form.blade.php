{!! csrf_field() !!}
<div class="form-group @if ($errors->has('title')) has-error @endif">
  <label for="title" class="form-label">Título da Página</label>
  <input type="text" class="form-control" id="title" name="title" value="{{ $page->title ?? old('title') }}" placeholder="Título da Página" autofocus>
  @if ($errors->has('title'))
  <span class="help-block">
    <strong>{{ $errors->first('title') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('slug')) has-error @endif">
  <label for="slug" class="form-label">URL Amigável</label>
  <input type="text" class="form-control" id="slug" name="slug" value="{{ $page->slug ?? old('slug') }}" placeholder="url-amigavel">
  @if ($errors->has('slug'))
  <span class="help-block">
    <strong>{{ $errors->first('slug') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('body')) has-error @endif">
  <label for="body" class="form-label">Conteúdo da Página</label>
  <textarea name="body" id="body" cols="30" rows="10" class="form-control summernote" placeholder="Conteúdo...">{{ $page->body ??  old('body') }}</textarea>
  @if ($errors->has('body'))
  <span class="help-block">
    <strong>{{ $errors->first('body') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('position')) has-error @endif">
  <label for="position" class="form-label">Ordem de Exibição</label>
  <input type="text" class="form-control" id="position" name="position" value="{{ $page->position ?? old('position') }}" placeholder="0" autofocus>
  @if ($errors->has('position'))
  <span class="help-block">
    <strong>{{ $errors->first('position') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('pages.index') }}" class="btn btn-default"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>