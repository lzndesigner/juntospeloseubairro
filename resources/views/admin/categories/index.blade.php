@extends('admin.base')
@section('title', 'Lista de Categorias')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->

<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              @if(count($categories) > 0)
              <table class="table table-hover table-bordered table-striped" id="categories-table">
                <thead>
                  <tr>
                    <td width="5%">#</td>
                    <td width="5%">Imagem</td>
                    <td>Title</td>
                    <td width="8%">Status</td>
                    <td width="14%">Criação</td>
                    <td width="14%">Alteração</td>
                    <td width="20%" class="text-right">Ações</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($categories as $category)
                  <tr id="row-{{ $category->id }}">
                    <td>
                          <b>{{ $category->id }}</b>
                    </td>
                    <td>
                      <img src="{{ $category->image ? asset('storage/categories/'. $category->id .'/photos/' . $category->image) : asset('galerias/sem_image.png') }}"  alt="{{ $category->title }}" class="img-thumbnail"> </td>
                    <td>{{ $category->title }}</td>
                    <td><span class="label label-{{ ($category->status == '1' ? 'success' : 'danger' ) }}">
                      {{ ($category->status == '1' ? 'Habilitado' : 'Desabilitado' ) }}
                    </span>
                  </td>
                  <td>{{ \Carbon\Carbon::parse($category->created_at)->format('d/m/y H:i')  }}</td>
                  <td>{{ \Carbon\Carbon::parse($category->updated_at)->format('d/m/y H:i') }}</td>
                  <td class="text-right">
                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i> Editar</a>
                    <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" data-id="{{ $category->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i> Remover?</a>
                  </td>
                </tr>
                @endforeach

              </tbody>
            </table>
            @else
            <div class="alert alert-info">Nenhuma categoria cadastrada.</div>
            @endif
            <hr>

          </div><!-- table-responsive -->
        </div><!-- panel-body -->

        <div class="row">
          <div class="col-md-12">
            <a href="{{ route('categories.create') }}" class="btn btn-thema"><i class="fa fa-plus"></i>Nova Categoria</a>
          </div><!-- col-md-6 -->

          <div class="col-md-12 text-right">
            {{ $categories->links() }}
          </div><!-- col-md-6 -->
        </div>


      </div><!-- panel-default -->
    </div><!-- col-md-12 -->


  </div><!-- row -->
</div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('jsPage')
<script>
$(function() {
  let btn = $('.btn-delete');
  let btnAll = $('.btn-remove-all');

  function remove(id) {
    let tr = $('#categories-table #row-' + id);
    let request = $.ajax({
     url: '/dashboard/categories/' + id,
     type: 'DELETE',
     statusCode: {
       204: function() {
         tr.fadeOut();
       },
       403: function() {
         let copy = tr.innerHTML;

         tr.html('').append('<td colspan="7">Essa categoria não foi removida.</td>')
         .css({'background-color': 'red','color': 'white', 'text-align': 'center'});
       }
     }
   });
    return request;
  }

  btn.on('click', function(e) {
    e.preventDefault();
    let action = $(e.currentTarget);
    let request = remove(action.data('id'));
    request.fail(function() {
      window.alert('Não foi possivel remover a categoria')
    });
  })

  btnAll.on('click', function(e) {
    e.preventDefault();

    let checkbox = $('input:checkbox[class="categories"]:checked');

    if(checkbox.length > 0 && window.confirm('Gostaria de remover a(s) ' + checkbox.length + ' selecionada(s) categoria(s)?')) {
          //
          checkbox.each(function(index, el) {
            let id = $(el).data('id')
            let request = remove(id);
          })
        }

      })
})
</script>
@endsection