@extends('admin.base')
@section('title', 'Editar Cliente')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $customer->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('customers.index') }}">Página de Cliente</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End customer Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-title">
            @yield('title') - {{ $customer->title }}
          </div>
          <div class="panel-body">
            <form action="{{ route('customers.update', $customer->id)}}" enctype="multipart/form-data" method="POST">
              <input type="hidden" name="_method" value="PUT">
              @include('admin.customers.form')
            </form>
          </div><!-- panel-body -->
        </div><!-- panel-default -->
      </div><!-- col-md-12 -->
    </div><!-- row -->
    <div class="row">
      <div class="col md-12">
        <div class="panel panel-default">
          <div class="panel-title">
            Planos Contratados
          </div>
          <div class="panel-body">
            <div class="table-responsiveOff">
              <table class="table striped table-bordered table-hover">
                <thead>
                  <th>#</th>
                  <th>Estabelecimento</th>
                  <th>Plano</th>
                  <th>Inicio</th>
                  <th>Venc.</th>
                  <th width='5%'>Renov.</th>
                  <th class="text-center" width='15%'>Valor/Método</th>
                  <th class="text-right" width='10%'>Status</th>
                  <th></th>
                </thead>
                <tbody>
                  @foreach($customerPlans as $customerPlan)
                  <tr>
                    <td>{{$customerPlan->id}}</td>
                    <td>{{ $customerPlan->estabelecimento }} <a href="{{ route('establishments.edit', $customerPlan->estabelecimentoid) }}" class="btn-xs" data-toggle="tooltip" data-original-title="Editar Estabelecimento"><i class="fa fa-edit"></i></a> </td>
                    <td>{{$customerPlan->plano}}</td>
                    <td>{{ \Carbon\Carbon::parse($customerPlan->plan_datebegin)->format('d/m/y') }}</td>
                    <td>
                      <div id="dateAtual{{$customerPlan->id}}">
                        {{ \Carbon\Carbon::parse($customerPlan->plan_dateend)->format('d/m/y') }}
                      </div>
                    </td>
                    <td>{{ $customerPlan->renews}} vezes</td>
                    <td class="text-center">@if($customerPlan->price)
                      R$ {{$customerPlan->price}} /
                      <small>
                        @if($customerPlan->plan_method == 'banktransfer')
                        Depósito/Transf.
                        @elseif($customerPlan->plan_method == 'mercadopago')
                        Mercado Pago
                        @else
                        Grátis
                        @endif
                      </small>
                      @endif</td>
                    <td>
                      <div class="btn-group pull-right">
                        <div id="statusAtual{{$customerPlan->id}}" class="pull-left">
                          @if($customerPlan->status == 'Pago')
                          <span class="label label-success">{{$customerPlan->status}}</span>
                          @elseif($customerPlan->status == 'Pendente')
                          <span class="label label-warning">{{$customerPlan->status}}</span>
                          @elseif($customerPlan->status == 'Aguardando')
                          <span class="label label-primary">{{$customerPlan->status}}</span>
                          @elseif($customerPlan->status == 'Expirado')
                          <span class="label label-danger">{{$customerPlan->status}}</span>
                          @endif
                        </div>
                        @if($customerPlan->plano)
                        <button type="button" data-toggle="dropdown" class="btn btn-xs btn-dark dropdown-toggle"><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a class="ajaxstatus" data-customerplan_id="{{$customerPlan->id}}" data-typestatus="Aguardando">Aguardando</a></li>
                          <li><a class="ajaxstatus" data-customerplan_id="{{$customerPlan->id}}" data-typestatus="Pendente">Pendente</a></li>
                          <li><a class="ajaxstatus" data-customerplan_id="{{$customerPlan->id}}" data-typestatus="Expirado">Expirado</a></li>
                          <li><a class="ajaxstatus" data-customerplan_id="{{$customerPlan->id}}" data-typestatus="Pago">Pago</a></li>
                        </ul>
                        @endif
                      </div>
                    </td>
                    <td>
                      <a href="{{ route('customerplans.edit', $customerPlan->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar Plano"><i class="fa fa-edit"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link rel="stylesheet" href="/backend/js/bootstrap-datepicker/css/bootstrap-datepicker.css" />
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js?1"></script>
<script src="/backend/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/backend/js/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js"></script>
<script>
  $('.datepicker').datepicker({
    format: "yyyy-mm-dd",
    language: "pt-BR",
    todayHighlight: true
  });
</script>

<script>
  function remove(customer, photo) {
    let doc = $('#photo-' + photo);
    let request = $.ajax({
      url: '/dashboard/customers/' + customer + '/photos/' + photo,
      type: 'DELETE',
      statusCode: {
        204: function() {
          doc.fadeOut(); // ocultar registro
        }
      }
    })
  }

  /* Gerar Previa */
  $('.update-photo').change(function(e) {
    console.log('updated.')
    const ref = e.target;

    if (!ref.files) return;

    let id = ref.id,
      image = $('#photo-preview-' + id),
      file = ref.files[0],
      old = image.attr('src')

    var reader = new FileReader();

    reader.onload = function(event) {

      image.css({
        filter: blur(1.5)
      })

      image.fadeOut(500, function() {
        image.attr('src', event.target.result);
        image.fadeIn(200, function() {
          image.css({
            filter: blur(0)
          })
        });
      })
    }
    //
    reader.readAsDataURL(file);
  });

  /* Adicionar arquivos */
  $('#add-photos-to-customer').click(function(e) {
    e.preventDefault();
    $("#input-add-photos").click();
  });

  $("#input-add-photos").change(function(e) {
    let display = $('span#display-add-photos'),
      files = e.target.files,
      count = files.length;

    if (count > 0) {
      display.empty().html(count + ' selecionados.')
    } else {
      display.empty().html('0 selecionados.')
    }
  });



  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote();
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="title"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>

<script type="text/javascript">
  $('.ajaxstatus').click(function() {
    console.log('clicou');
    var customerplan_id = $(this).data("customerplan_id");
    var type_status = $(this).data("typestatus");

    $.ajax({
      url: '{{route('customers.setstatus')}}',
      type: 'get',
      data: {
        customerplan_id: customerplan_id,
        type_status: type_status
      },
      dataType: 'html',
      success: function(html) {
        var dateend = JSON.parse(html).dateformat;
        if (html != '') {
          if (type_status == 'Pago') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-success">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-success">' + type_status + '</span>');
          } else if (type_status == 'Pendente') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-warning">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-warning">' + type_status + '</span>');
          } else if (type_status == 'Aguardando') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-primary">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-primary">' + type_status + '</span>');
          } else if (type_status == 'Expirado') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-danger">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-danger">' + type_status + '</span>');
          } else {
            $('#dateAtual' + customerplan_id).html('<span class="label label-default">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-default">' + type_status + '</span>');
          }
        }
      }
    });
  });
</script>
@endsection