@extends('admin.base')
@section('title', 'Novo Cliente')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('customers.index') }}">Página de Clientes</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">

            <form action="{{ route('customers.store')}}" enctype="multipart/form-data" method="POST">
              @include('admin.customers.form')
            </form>

          </div><!-- panel-body -->


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link rel="stylesheet" href="/backend/js/bootstrap-datepicker/css/bootstrap-datepicker.css" />
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js?1"></script>
<script src="/backend/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/backend/js/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js"></script>
<script>
$('.datepicker').datepicker({
  format: "yyyy-mm-dd",
  language: "pt-BR",
  todayHighlight: true
});
</script>

<script>

/* Adicionar arquivos */
$('#add-photos-to-customer').click(function(e) {
  e.preventDefault();
  $("#input-add-photos").click();
});

$("#input-add-photos").change(function(e) {
  let display = $('span#display-add-photos'),
  files = e.target.files,
  count = files.length;

  if(count > 0) {
    display.empty().html(count + ' selecionados.')
  }else{
    display.empty().html('0 selecionados.')
  }
});

/* BOOTSTRAP WYSIHTML5 */
$('.textarea').wysihtml5();

/* SUMMERNOTE*/
$(document).ready(function() {
  $('.summernote').summernote();
});
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
$('input[name="name"]').stringToSlug({
  setEvents: 'keyup keydown blur',
  getPut: 'input[name="slug"]',
  space: '-',
  replace: '/\s?\([^\)]*\)/gi',
AND: 'e'
});
</script>
@endsection