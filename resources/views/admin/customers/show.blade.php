@extends('admin.base')
@section('title', 'Detalhes do Cliente')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $customer->name }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('customers.index') }}">Página de Clientes</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End customer Header -->


<!-- START CONTAINER -->
<div class="container-default">
  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-pills" role="tablist">
                <li role="presentation" class="active"><a href="#tab_info" aria-controls="tab_info" role="tab" data-toggle="tab">Informações</a></li>
                <li role="presentation"><a href="#tab_plans" aria-controls="tab_plans" role="tab" data-toggle="tab">Planos</a></li>
                <li role="presentation"><a href="#tab_transations" aria-controls="tab_transations" role="tab" data-toggle="tab">Transações</a></li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab_info">
                  <div class="row">
                    <div class="col-xs-12 col-md-4">
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <h4>Informações do Cliente Proprietário</h4>

                          <p><b>{{ $customer->name }}</b></p>
                          <ul>
                            <li>E-mail: {{ $customer->email }}</li>
                            <li>CPF/CNPJ: {{ $customer->document }}</li>
                            <li>Desconto: {{ $customer->discount }}%</li>
                            <li>Método de Pagamento: {{ ($customer->plan_method == 'banktransfer' ? 'Depósito Bancário' : 'Mercado Pago') }}</li>
                            <li>Desde: {{ $customer->created_at->format('d/m/Y H:i') }}</li>
                            <li>Status: <span class="label label-{{ ($customer->status == '1' ? 'success' : 'danger' ) }}">
                                {{ ($customer->status == '1' ? 'Habilitado' : 'Desabilitado' ) }}
                              </span></li>
                          </ul>
                        </div><!-- panel-body -->
                      </div><!-- panel -->
                    </div>
                  </div>
                </div><!-- tab_info -->

                <div role="tabpanel" class="tab-pane" id="tab_plans">
                  <div class="row">
                    <div class="col-xs-12 col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <h4>Planos</h4>

                          <div class="table-responsiveOff">
                            <table class="table striped table-bordered table-hover">
                              <thead>
                                <th>#</th>
                                <th>Estabelecimento</th>
                                <th>Tipo</th>
                                <th>Inicio</th>
                                <th>Vencimento</th>
                                <th>Valor/Método</th>
                                <th>Status</th>
                                <th></th>
                              </thead>
                              <tbody>
                                @foreach($customerPlans as $customerPlan)
                                <tr>
                                  <td>{{$customerPlan->id}}</td>
                                  <td>{{$customerPlan->estabelecimento}}</td>
                                  <td>{{$customerPlan->plano}}</td>
                                  <td>{{ \Carbon\Carbon::parse($customerPlan->plan_datebegin)->format('d/m/y') }}</td>
                                  <td>{{ \Carbon\Carbon::parse($customerPlan->plan_dateend)->format('d/m/y') }}</td>
                                  <td>@if($customerPlan->price)R$ {{$customerPlan->price}} / {{$customerPlan->plan_method}}@endif</td>
                                  <td>
                                    <div id="statusAtual{{$customerPlan->id}}" class="pull-left">
                                      @if($customerPlan->status == 'Pago')
                                        <span class="label label-success">{{$customerPlan->status}}</span>
                                      @elseif($customerPlan->status == 'Pendente')
                                        <span class="label label-warning">{{$customerPlan->status}}</span>
                                      @elseif($customerPlan->status == 'Aguardando')
                                        <span class="label label-primary">{{$customerPlan->status}}</span>
                                      @elseif($customerPlan->status == 'Expirado')
                                        <span class="label label-danger">{{$customerPlan->status}}</span>
                                      @endif
                                    </div>
                                  </td>
                                  <td>
                                    @if($customerPlan->plano)
                                    <div class="btn-group pull-right">
                                      <button type="button" data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle"><span class="caret"></span></button>
                                      <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a class="ajaxstatus" data-customerplan_id="{{$customerPlan->id}}" data-typestatus="Aguardando">Aguardando</a></li>
                                        <li><a class="ajaxstatus" data-customerplan_id="{{$customerPlan->id}}" data-typestatus="Pendente">Pendente</a></li>
                                        <li><a class="ajaxstatus" data-customerplan_id="{{$customerPlan->id}}" data-typestatus="Expirado">Expirado</a></li>
                                        <li><a class="ajaxstatus" data-customerplan_id="{{$customerPlan->id}}" data-typestatus="Pago">Pago</a></li>
                                      </ul>
                                    </div>
                                    @endif
                                  </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                        </div><!-- panel-body -->
                      </div><!-- panel -->
                    </div>
                  </div>
                </div><!-- tab_plans -->

                <div role="tabpanel" class="tab-pane" id="tab_transations">
                  <div class="row">
                    <div class="col-xs-12 col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <h4>Transações</h4>

                          <div class="table-responsive">
                            <table class="table table-hover table-bordered table-striped" id="transations-table">
                              <thead>
                                <tr>
                                  <td width="5%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'id']) }}">#</a></td>
                                  <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'customer']) }}">Cliente</a></td>
                                  <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'plan_name']) }}">Plano</a></td>
                                  <td width="12%">Período</td>
                                  <td width="8%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'plan_method']) }}">Método</a></td>
                                  <td width="8%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'plan_value']) }}">Valor</a></td>
                                  <td width="8%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'status']) }}">Status</a></td>
                                  <td width="14%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'date']) }}">Data</a></td>
                                  <td width="15%" class="text-right">Ações</td>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($customerTransations as $customerTransation)
                                <tr id="row-{{ $customerTransation->id }}">
                                  <td>
                                    <div class="checkbox">
                                      <input class="customerTransations" id="checkbox{{ $customerTransation->id }}" data-id="{{ $customerTransation->id }}" type="checkbox">
                                      <label for="checkbox{{ $customerTransation->id }}">
                                        <b>{{ $customerTransation->id }}</b>
                                      </label>
                                    </div>
                                  </td>
                                  <td>{{ $customerTransation->customer }}</td>
                                  <td>{{ $customerTransation->plan_name }}</td>
                                  <td>{{ $customerTransation->plan_period }}</td>
                                  <td>{{ ($customerTransation->plan_method == 'banktransfer' ? 'Depósito Bancário' : 'Mercado Pago') }}</td>
                                  <td>R$ {{ $customerTransation->plan_value }}</td>
                                  <td>{{ $customerTransation->status }}</td>
                                  <td>{{ \Carbon\Carbon::parse($customerTransation->date)->format('d/m/y H:i') }}</td>
                                  <td class="text-right">
                                    <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" data-id="{{ $customerTransation->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i></a>
                                  </td>
                                </tr>
                                @endforeach

                              </tbody>
                            </table>
                          </div>
                        </div><!-- panel-body -->
                      </div><!-- panel -->
                    </div>
                  </div>
                </div><!-- tab_transations -->

              </div><!-- tab-content -->

              <hr>

              <a href="{{ route('customers.index') }}" class="btn btn-warning">Voltar</a>
              <a href="{{ route('customers.edit', $customer->id) }}" class="btn btn-primary">Editar</a>
              <form id="delete-form-customers" action="{{ route('customers.destroy', $customer->id) }}" method="POST" style="display: none;">
                {!! csrf_field() !!}
                <input type="hidden" name="_method" value="DELETE">
              </form>

            </div><!-- panel-body -->
          </div><!-- panel-default -->
        </div><!-- col-md-12 -->
      </div><!-- row -->
    </div><!-- container-padding -->

  </div><!-- container-default -->
  <!-- END CONTAINER -->
  @endsection

  @section('jsPage')
  <script type="text/javascript">
    $('.ajaxstatus').click(function() {
      console.log('clicou');
      var customerplan_id = $(this).data("customerplan_id");
      var type_status = $(this).data("typestatus");
      
        $.ajax({
          url: '{{ route('customers.setstatus') }}',
          type: 'get',
          data: {
            customerplan_id: customerplan_id,
            type_status: type_status
          },
          dataType: 'html',
          success: function(html) {
            if (html != '') {
              console.log(html);
              if(type_status == 'Pago'){
                $('#statusAtual' + customerplan_id).html('<span class="label label-success">'+type_status+'</span>');
              }else if(type_status == 'Pendente'){
                $('#statusAtual' + customerplan_id).html('<span class="label label-warning">'+type_status+'</span>');
              }else if(type_status == 'Aguardando'){
                $('#statusAtual' + customerplan_id).html('<span class="label label-primary">'+type_status+'</span>');
              }else if(type_status == 'Expirado'){
                $('#statusAtual' + customerplan_id).html('<span class="label label-danger">'+type_status+'</span>');
              }else{
                $('#statusAtual' + customerplan_id).html('<span class="label label-default">'+type_status+'</span>');
              }

            }
          }
        });
    });
  </script>
  @endsection