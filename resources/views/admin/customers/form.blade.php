{!! csrf_field() !!}

<div class="heading-block">
  <h4>Descrição do Plano</h4>
</div>

<div class="row">
  <div class="col-xs-12 col-md-3">
    <div class="form-group @if ($errors->has('name')) has-error @endif">
      <label for="name" class="form-label">Nome do Proprietário</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ $customer->name ?? old('name') }}" required placeholder="Nome do Proprietário" autofocus>
      @if ($errors->has('name'))
      <span class="help-block">
        <strong>{{ $errors->first('name') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

  <div class="col-xs-12 col-md-4">
    <div class="form-group @if ($errors->has('email')) has-error @endif">
      <label for="email" class="form-label">E-mail</label>
      <input type="text" class="form-control" id="email" name="email" value="{{ $customer->email ?? old('email') }}" required placeholder="E-mail" >
      @if ($errors->has('email'))
      <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

  <div class="col-xs-12 col-md-3">
    <div class="form-group @if ($errors->has('phone')) has-error @endif">
      <label for="phone" class="form-label">Telefone/WhatsApp</label>
      <input type="text" class="form-control" id="phone" name="phone" value="{{ $customer->phone ?? old('phone') }}" required placeholder="Telefone/WhatsApp" >
      @if ($errors->has('phone'))
      <span class="help-block">
        <strong>{{ $errors->first('phone') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
</div>

<div class="row">
  <div class="col-xs-12 col-md-4">
    <div class="form-group">
      <label for="password" class="form-label">Senha</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="******">
      @if ($errors->has('password'))
      <span class="help-block">
        <strong>{{ $errors->first('password') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

  <div class="col-xs-12 col-md-4">
    <div class="form-group">
      <label for="password" class="form-label">Confirmar Senha</label>
      <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="******">
    </div><!-- form-group -->
  </div><!-- cols -->


  <div class="col-xs-12 col-md-2">
    <div class="form-group @if ($errors->has('status')) has-error @endif">
      <label for="status" class="form-label">Status do Cliente</label>
      <select class="form-control" id="status" name="status">
        @if(isset($customer->status))
        @if($customer->status == 0 )
        <option value="0" selected>Desabilitado</option> 
        <option value="1">Habilitar</option>
        @else
        <option value="1" selected>Habilitado</option>
        <option value="0">Desabilitar</option> 
        @endif
        @else
        <option value="1" selected>Habilitado</option>
        <option value="0">Desabilitado</option> 
        @endif
      </select>

      @if ($errors->has('status'))
      <span class="help-block">
        <strong>{{ $errors->first('status') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->
</div><!-- row -->

<hr>

<div class="row">  
  <div class="col-xs-12 col-md-4">
    <div class="form-group @if ($errors->has('document')) has-error @endif">
      <label for="document" class="form-label">CPF/CNPJ do Proprietário</label>
      <input type="text" class="form-control" id="document" name="document" value="{{ $customer->document ?? old('document') }}" required placeholder="CPF/CNPJ do Proprietário">
      @if ($errors->has('document'))
      <span class="help-block">
        <strong>{{ $errors->first('document') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

  <div class="col-xs-12 col-md-4">
    <div class="form-group @if ($errors->has('plan_method')) has-error @endif">
      <label for="plan_method" class="form-label">Método de Pagamento</label>
      <select class="form-control" id="plan_method" name="plan_method">
        @if(isset($customer->plan_method))
        @if($customer->plan_method == 'banktransfer' )
        <option value="banktransfer" selected>Depósito Bancário</option> 
        <option value="mercadopago">Mercado Pago</option>
        @else
        <option value="mercadopago" selected>Mercado Pago</option>
        <option value="banktransfer">Depósito Bancário</option> 
        @endif
        @else
        <option value="mercadopago" selected>Mercado Pago</option>
        <option value="banktransfer">Depósito Bancário</option> 
        @endif
      </select>

      @if ($errors->has('plan_method'))
      <span class="help-block">
        <strong>{{ $errors->first('plan_method') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

  <div class="col-xs-12 col-md-2">
    <div class="form-group @if ($errors->has('plan_discount')) has-error @endif">
      <label for="plan_discount" class="form-label">Desconto (%)</label>
      <input type="number" min="0" max="100" class="form-control" id="plan_discount" name="plan_discount" value="{{ $customer->plan_discount ?? old('plan_discount') }}" placeholder="Desconto"  >
      <div id="datePickermodel"></div>
      @if ($errors->has('plan_discount'))
      <span class="help-block">
        <strong>{{ $errors->first('plan_discount') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- cols -->

</div>



<hr>

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('customers.index') }}" class="btn btn-default"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>