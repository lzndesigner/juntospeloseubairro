@extends('admin.base')
@section('title', 'Lista de Clientes')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              @if(count($customers) > 0)
              <table class="table table-hover table-bordered table-striped" id="customers-table">
                <thead>
                  <tr>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'id', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> #</td>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'name', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Cliente</td>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'email', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> E-mail</td>
                    <td>Telefone</td>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'qtaEsta', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Estab.</td>
                    <td width="8%">Status</td>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'created_at', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Criação</td>
                    <td>Alteração</td>
                    <td class="text-right">Ações</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($customers as $customer)
                  <tr id="row-{{ $customer->id }}">
                    <td>
                      <b>{{ $customer->id }}</b>
                    </td>
                    <td><a href="{{ route('customers.edit', $customer->id) }}">{{ $customer->name }}</a></td>
                    <td>{{ $customer->email }}</td>
                    @php $retira = ["(", ")", " ", "-"]; @endphp
                    <td>
                      @if($customer->phone)
                      <a href="https://api.whatsapp.com/send?phone=55{{ str_replace($retira, '', $customer->phone) }}" target="_Blank">
                      <span class="label label-success"><i class="fab fa-whatsapp"></i></span>
                      {{ $customer->phone }}</a>
                      @else
                       -
                      @endif
                    </td>
                    <td>{{ $customer->qtaEsta }}</td>
                    <td><span class="label label-{{ ($customer->status == '1' ? 'success' : 'danger' ) }}">
                        {{ ($customer->status == '1' ? 'Habilitado' : 'Desabilitado' ) }}
                      </span>
                    </td>
                    <td>{{ \Carbon\Carbon::parse($customer->created_at)->format('d/m/y H:i')  }}</td>
                    <td>{{ \Carbon\Carbon::parse($customer->updated_at)->format('d/m/y H:i') }}</td>
                    <td class="text-right">
                      <a href="{{ route('customers.edit', $customer->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i> Editar</a>
                      <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" style="margin-left:10px;" data-id="{{ $customer->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i> Remover?</a>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
              @else
              <div class="alert alert-info">Nenhum cliente cadastrado.</div>
              @endif
              <hr>

            </div><!-- table-responsive -->
          </div><!-- panel-body -->

          <div class="row">
            <div class="col-md-6">
              <a href="{{ route('customers.create') }}" class="btn btn-thema"><i class="fa fa-plus"></i>Novo cliente</a>
            </div><!-- col-md-6 -->

            <div class="col-md-6 text-right">
              {{ $customers->appends(request()->except('page'))->links() }}
            </div><!-- col-md-6 -->
          </div>


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('jsPage')
<script>
  $(function() {
    let btn = $('.btn-delete');
    let btnAll = $('.btn-remove-all');

    function remove(id) {
      let tr = $('#customers-table #row-' + id);
      let request = $.ajax({
        url: '/dashboard/customers/' + id,
        type: 'DELETE',
        statusCode: {
          204: function() {
            tr.fadeOut();
          },
          403: function() {
            let copy = tr.innerHTML;

            tr.html('').append('<td colspan="7">Essa cliente não foi removida.</td>')
              .css({
                'background-color': 'red',
                'color': 'white',
                'text-align': 'center'
              });
          }
        }
      });
      return request;
    }

    btn.on('click', function(e) {
      e.preventDefault();
      let action = $(e.currentTarget);
      let request = remove(action.data('id'));
      request.fail(function() {
        window.alert('Não foi possivel remover a cliente')
      });
    })

    btnAll.on('click', function(e) {
      e.preventDefault();

      let checkbox = $('input:checkbox[class="customers"]:checked');

      if (checkbox.length > 0 && window.confirm('Gostaria de remover a(s) ' + checkbox.length + ' selecionada(s) cliente(s)?')) {
        //
        checkbox.each(function(index, el) {
          let id = $(el).data('id')
          let request = remove(id);
        })
      }

    })
  })
</script>
@endsection