{!! csrf_field() !!}

<!-- <div id="description" class="tab-pane active"> -->
<div class="heading-block">
  <h4>Descrição do Plano</h4>
</div>

<div class="row">
  <div class="col-xs-12 col-md-8">
    <div class="row">
      <div class="col-xs-12 col-md-9">
        <div class="form-group @if ($errors->has('name')) has-error @endif">
          <label for="name" class="form-label">Nome do Plano</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ $plan->name ?? old('name') }}" placeholder="Nome do Plano" autofocus>
          @if ($errors->has('name'))
          <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
        </div><!-- form-group -->
      </div><!-- cols -->

      <div class="col-xs-12 col-md-3">
        <div class="form-group @if ($errors->has('status')) has-error @endif">
          <label for="status" class="form-label">Status do Plano</label>
          <select class="form-control" id="status" name="status">
            @if(isset($plan->status))
            @if($plan->status == 0 )
            <option value="0" selected>Desabilitado</option>
            <option value="1">Habilitar</option>
            @else
            <option value="1" selected>Habilitado</option>
            <option value="0">Desabilitar</option>
            @endif
            @else
            <option value="1" selected>Habilitado</option>
            <option value="0">Desabilitado</option>
            @endif
          </select>

          @if ($errors->has('status'))
          <span class="help-block">
            <strong>{{ $errors->first('status') }}</strong>
          </span>
          @endif
        </div><!-- form-group -->
      </div><!-- cols -->
    </div><!-- row -->

    <div class="panel panel-default">
      <div class="panel-body">
        <h4>Recursos disponíveis</h4>

        <div class="col-xs-12 col-md-8">
          <div class="form-group @if ($errors->has('description')) has-error @endif">
            <label for="description" class="form-label">Descrição de Benefícios</label>
            <textarea name="description" id="description" class="form-control summernote" cols="30" rows="10">{{ $plan->description ?? old('description') }}</textarea>

            @if ($errors->has('description'))
            <span class="help-block">
              <strong>{{ $errors->first('description') }}</strong>
            </span>
            @endif
          </div><!-- form-group -->


        </div><!-- cols -->

        <div class="col-xs-12 col-md-4">
          <div class="form-group @if ($errors->has('statistics')) has-error @endif">
            <label for="statistics" class="form-label">Gráfico de Estatísticas</label>
            <select class="form-control" id="statistics" name="statistics">
              @if(isset($plan->statistics))
              @if($plan->statistics == 0 )
              <option value="0" selected>Desabilitado</option>
              <option value="1">Habilitar</option>
              @else
              <option value="1" selected>Habilitado</option>
              <option value="0">Desabilitar</option>
              @endif
              @else
              <option value="1" selected>Habilitado</option>
              <option value="0">Desabilitado</option>
              @endif
            </select>

            @if ($errors->has('statistics'))
            <span class="help-block">
              <strong>{{ $errors->first('statistics') }}</strong>
            </span>
            @endif
          </div><!-- form-group -->

          <div class="form-group">
            <div class="checkbox2">
              <label>
                @if(isset($plan->upgrade))
                @if($plan->upgrade == 1)
                <input type="checkbox" name="upgrade" id="upgrade" value="1" checked>
                @else
                <input type="checkbox" name="upgrade" id="upgrade" value="0">
                @endif
                @else
                <input type="checkbox" name="upgrade" id="upgrade" value="1">
                @endif
                Plano é um Upgrade?
              </label>
            </div>
          </div><!-- form-group -->
        </div><!-- cols -->

      </div><!-- panel-body -->
    </div>
  </div><!-- col -->

  <div class="col-xs-12 col-md-4">
    <div class="panel panel-default">
      <div class="panel-body">
        <h4>Tabela de Preços</h4>
        <div class="col-xs-6 col-md-6">
          <div class="form-group @if ($errors->has('period')) has-error @endif">
            <label for="period" class="form-label">Período</label>
            <input type="text" class="form-control" id="period" name="period" value="{{ $plan->period ?? old('period') }}" placeholder="Estabelecimentos Disponíveis">

            @if ($errors->has('period'))
            <span class="help-block">
              <strong>{{ $errors->first('period') }}</strong>
            </span>
            @endif
          </div><!-- form-group -->
        </div><!-- col -->

        <div class="col-xs-6 col-md-6">
          <div class="form-group @if ($errors->has('price')) has-error @endif">
            <label for="price" class="form-label">Preço</label>
            <input type="text" class="form-control priceFormated" id="price" name="price" value="{{ $plan->price ?? old('price') }}" placeholder="Estabelecimentos Disponíveis">

            @if ($errors->has('price'))
            <span class="help-block">
              <strong>{{ $errors->first('price') }}</strong>
            </span>
            @endif
          </div><!-- form-group -->
        </div><!-- col -->


      </div>
    </div>
  </div><!-- cols -->

</div><!-- row -->

<hr>

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i> Salvar</button>
    <a href="{{ route('plans.index') }}" class="btn btn-default"><i class="fa fa-reply"></i> Cancelar</a>
  </div><!-- col-md-6 -->
</div>