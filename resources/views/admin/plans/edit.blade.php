@extends('admin.base')
@section('title', 'Editar Plano')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $plan->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('plans.index') }}">Página de Plano</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End plan Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title') - {{ $plan->title }}
          </div>

          <div class="panel-body">

            <form action="{{ route('plans.update', $plan->id)}}" enctype="multipart/form-data" method="POST">
              <input type="hidden" name="_method" value="PUT">
              @include('admin.plans.form')
            </form>

          </div><!-- panel-body -->


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link href="{{ asset('backend/css/plugin/summernote/summernote.css?13') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugin/summernote/summernote-bs3.css?13') }}" rel="stylesheet">
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/front/js/summernote/summernote.min.js?1"></script>
<script type="text/javascript" src="/front/js/summernote/summernote_lang_pt-br.js?1"></script>

<script>
  function remove(plan, photo) {
    let doc = $('#photo-' + photo);
    let request = $.ajax({
      url: '/dashboard/plans/' + plan + '/photos/' + photo,
      type: 'DELETE',
      statusCode: {
        204: function() {
          doc.fadeOut(); // ocultar registro
        }
      }
    })
  }

  /* Gerar Previa */
  $('.update-photo').change(function(e) {
    console.log('updated.')
    const ref = e.target;

    if (!ref.files) return;

    let id = ref.id,
      image = $('#photo-preview-' + id),
      file = ref.files[0],
      old = image.attr('src')

    var reader = new FileReader();

    reader.onload = function(event) {

      image.css({
        filter: blur(1.5)
      })

      image.fadeOut(500, function() {
        image.attr('src', event.target.result);
        image.fadeIn(200, function() {
          image.css({
            filter: blur(0)
          })
        });
      })
    }
    //
    reader.readAsDataURL(file);
  });

  /* Adicionar arquivos */
  $('#add-photos-to-plan').click(function(e) {
    e.preventDefault();
    $("#input-add-photos").click();
  });

  $("#input-add-photos").change(function(e) {
    let display = $('span#display-add-photos'),
      files = e.target.files,
      count = files.length;

    if (count > 0) {
      display.empty().html(count + ' selecionados.')
    } else {
      display.empty().html('0 selecionados.')
    }
  });



  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote({
      lang: 'pt-BR'
    });
    $('button[data-event="showImageDialog"], button[data-event="showVideoDialog"], button[data-event="codeview"], .note-toolbar .note-table').hide();
  });

  jQuery(function($) {
    $(document).on('keypress', '#period', function(e) {
      var square = document.getElementById("period");
      var key = (window.event) ? event.keyCode : e.which;
      if ((key > 47 && key < 58)) {
        period.style.backgroundColor = "#ffffff";
        return true;

      } else {
        period.style.backgroundColor = "rgba(255, 0, 0, 0.2)";
        return (key == 8 || key == 0) ? true : false;

      }
    });
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="title"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>
@endsection