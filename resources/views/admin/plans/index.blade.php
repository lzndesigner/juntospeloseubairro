@extends('admin.base')
@section('title', 'Lista de Planos')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              @if(count($plans) > 0)
              <table class="table table-hover table-bordered table-striped" id="plans-table">
                <thead>
                  <tr>
                    <td>#</td>
                    <td>Plano</td>
                    <td>Período</td>
                    <td>Preço</td>
                    <td>Estatísticas</td>
                    <td>Status</td>
                    <td>Criação</td>
                    <td>Alteração</td>
                    <td class="text-right">Ações</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($plans as $plan)
                  <tr id="row-{{ $plan->id }}">
                    <td>
                      <b>{{ $plan->id }}</b>
                    </td>
                    <td>{{ $plan->name }}</td>
                    <td>{{ $plan->period }} dias</td>
                    <td>R$ {{ $plan->price }}</td>
                    <td><span class="label label-{{ ($plan->statistics == '1' ? 'success' : 'danger' ) }}">
                        {{ ($plan->statistics == '1' ? 'Habilitado' : 'Desabilitado' ) }}
                      </span>
                    <td><span class="label label-{{ ($plan->status == '1' ? 'success' : 'danger' ) }}">
                        {{ ($plan->status == '1' ? 'Habilitado' : 'Desabilitado' ) }}
                      </span>
                    </td>
                    <td>{{ \Carbon\Carbon::parse($plan->created_at)->format('d/m/y H:i')  }}</td>
                    <td>{{ \Carbon\Carbon::parse($plan->updated_at)->format('d/m/y H:i') }}</td>
                    <td class="text-right">
                      <a href="{{ route('plans.edit', $plan->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                      <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete hide" data-id="{{ $plan->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i></a>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
              @else
              <div class="alert alert-info">Nenhum plano cadastrado.</div>
              @endif
              <hr>

            </div><!-- table-responsive -->
          </div><!-- panel-body -->

          <div class="row">
            <div class="col-md-6">
              <a href="{{ route('plans.create') }}" class="btn btn-thema"><i class="fa fa-plus"></i>Novo plano</a>
              <a href="#" class="btn btn-danger btn-remove-all hide"><i class="fa fa-remove"></i>Remover selecionados</a>
            </div><!-- col-md-6 -->

            <div class="col-md-6 text-right">
              {{ $plans->links() }}
            </div><!-- col-md-6 -->
          </div>


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('cssPage')
@endsection

@section('jsPage')
<script>
  $(function() {
    let btn = $('.btn-delete');
    let btnAll = $('.btn-remove-all');

    function remove(id) {
      let tr = $('#plans-table #row-' + id);
      let request = $.ajax({
        url: '/dashboard/plans/' + id,
        type: 'DELETE',
        statusCode: {
          204: function() {
            tr.fadeOut();
          },
          403: function() {
            let copy = tr.innerHTML;

            tr.html('').append('<td colspan="7">Essa plano não foi removida.</td>')
              .css({
                'background-color': 'red',
                'color': 'white',
                'text-align': 'center'
              });
          }
        }
      });
      return request;
    }

    btn.on('click', function(e) {
      e.preventDefault();
      let action = $(e.currentTarget);
      let request = remove(action.data('id'));
      request.fail(function() {
        window.alert('Não foi possivel remover a plano')
      });
    })

    btnAll.on('click', function(e) {
      e.preventDefault();

      let checkbox = $('input:checkbox[class="plans"]:checked');

      if (checkbox.length > 0 && window.confirm('Gostaria de remover a(s) ' + checkbox.length + ' selecionada(s) plano(s)?')) {
        //
        checkbox.each(function(index, el) {
          let id = $(el).data('id')
          let request = remove(id);
        })
      }

    })
  })
</script>
@endsection