@extends('admin.base')
@section('title', 'Novo Plano')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('plans.index') }}">Página de Planos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">

            <form action="{{ route('plans.store')}}" enctype="multipart/form-data" method="POST">
              @component('admin.plans.form')
              <div class="row">
                <div class="col-md-12">
                  <div class="heading-block">
                    <h4>Imagens do Plano</h4>
                  </div>
                </div>

                <div class="col-md-12">
                  <input type="file" style="display: none;" name="photos[]" id="input-add-photos" multiple>
                  <button class="btn btn-thema" id="add-photos-to-plan" type="button">
                    <i class="fa fa-plus"></i> Selecionar Imagens
                  </button>
                  <span class="btn disabled" id="display-add-photos">0 selecionados</span>
                </div>

                <div class="col-md-12">
                  <span class="help-block">É obrigatório o envio de 1 imagem para Capa do Plano</span>
                  <hr>
                </div>
              </div>
          </div>
          @endcomponent
          </form>
        </div><!-- panel-body -->
      </div><!-- panel-default -->
    </div><!-- col-md-12 -->
  </div><!-- row -->
</div><!-- container-padding -->
</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link href="{{ asset('backend/css/plugin/summernote/summernote.css?13') }}" rel="stylesheet">
<link href="{{ asset('backend/css/plugin/summernote/summernote-bs3.css?13') }}" rel="stylesheet">
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/backend/js/summernote/summernote.min.js?1"></script>

<script>
  /* Adicionar arquivos */
  $('#add-photos-to-plan').click(function(e) {
    e.preventDefault();
    $("#input-add-photos").click();
  });

  $("#input-add-photos").change(function(e) {
    let display = $('span#display-add-photos'),
      files = e.target.files,
      count = files.length;

    if (count > 0) {
      display.empty().html(count + ' selecionados.')
    } else {
      display.empty().html('0 selecionados.')
    }
  });

  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote();
  });

  jQuery(function($) {
    $(document).on('keypress', '#period', function(e) {
      var square = document.getElementById("period");
      var key = (window.event) ? event.keyCode : e.which;
      if ((key > 47 && key < 58)) {
        period.style.backgroundColor = "#ffffff";
        return true;

      } else {
        period.style.backgroundColor = "rgba(255, 0, 0, 0.2)";
        return (key == 8 || key == 0) ? true : false;

      }
    });
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="name"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>
@endsection