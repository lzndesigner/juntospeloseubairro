<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="{{ $configs->config_description ?? '' }}">
  <meta name="keywords" content="{{ $configs->config_keywords ?? '' }}" />
  <title>{{ $configs->config_title ?? 'Catalogo Base' }} - Painel Administrativo</title>
  <link rel="shortcut icon" href="{{ asset('galerias/favicon.ico?6') }}" />
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- ========== Css Files ========== -->
  <!-- <link href="{{ asset('backend/css/root.css?13') }}" rel="stylesheet"> -->
  <link href="{{ asset('backend/css/bootstrap.css?23') }}" rel="stylesheet">
  <link href="{{ asset('backend/css/style.css?23') }}" rel="stylesheet">
  <link href="{{ asset('backend/css/responsive.css?23') }}" rel="stylesheet">
  <link href="{{ asset('backend/css/shortcuts.css?23') }}" rel="stylesheet">
  <link href="{{ asset('backend/css/custons.css?23') }}" rel="stylesheet">
  @yield('cssPage')
</head>

<body>
  <div id="app" style="height:100%;">
    <!-- Start Page Loading -->
    <div class="loading"><img src="{{ asset('backend/img/loading.gif') }}" alt="loading-img"></div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->


    <!-- START TOP -->
    <div id="top" class="clearfix">
      <!-- Start Sidebar Show Hide Button -->
      <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>
      <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>
      <!-- End Sidebar Show Hide Button -->

      <!-- Start Sidepanel Show-Hide Button -->
      {{--<a href="#sidepanel" class="sidepanel-open-button"><i class="fa fa-outdent"></i></a>--}}
      <!-- End Sidepanel Show-Hide Button -->

      <!-- Start Top Right -->
      <ul class="top-right">
        <li class="dropdown link">
          <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><b>{{ Auth::user()->name }}</b><span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa falist fa-power-off"></i> Sair da Conta</a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </ul>
        </li>
      </ul>
      <!-- End Top Right -->
    </div>
    <!-- END TOP -->

    <!-- START SIDEBAR -->
    @include('admin.includes.sidebar-left')
    <!-- END SIDEBAR -->

    <!-- START CONTENT -->
    <div class="content">

      @yield('content')
      <!-- Start Footer -->
      <div class="row footer">
        <div class="col-md-6 text-left">
          {{ $configs->config_title ?? 'Catalogo Base' }} - Todos os Direitos Reservados {{ date('Y') }} ©
        </div>
        <div class="col-md-6 text-right">
          Desenvolvido por <a href="https://innsystem.com.br" target="_blank">InnSystem Inovação em Sistemas</a>
        </div>
      </div>
      <!-- End Footer -->

    </div>
    <!-- End Content -->



    <!-- START SIDEPANEL -->
    @include('admin.includes.sidebar-right')
    <!-- END SIDEPANEL -->
  </div>

  <script src="{{ mix('js/app.js') }}"></script>
  <script src="https://kit.fontawesome.com/04571ab3d2.js" crossorigin="anonymous"></script>
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- ================================================
jQuery Library
================================================ -->
  <script type="text/javascript" src="{{ asset('backend/js/jquery.min.js') }}"></script>
  <!-- ================================================
Bootstrap Core JavaScript File
================================================ -->
  <script src="{{ asset('backend/js/bootstrap/bootstrap.min.js') }}"></script>
  <!-- ================================================
Plugin.js - Some Specific JS codes for Plugin Settings
================================================ -->
  <script type="text/javascript" src="{{ asset('backend/js/plugins.js') }}"></script>
  <script src="{{ asset('js/function_theme.js') }}"></script>
  <script type="text/javascript">
    $(function() {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        contentType: 'application/json'
      });
    })
  </script>
  <script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
  <script>
    $(document).ready(function() {

      $('#config_phone').mask('(00) 0000-00009');
      $('#config_phone').blur(function(event) {
        if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#config_phone').mask('(00) 0000-0000');
        } else {
          $('#config_phone').mask('(00) 90000-0000');
        }
      });

      $('#config_cellphone').mask('(00) 0000-00009');
      $('#config_cellphone').blur(function(event) {
        if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#config_cellphone').mask('(00) 0000-0000');
        } else {
          $('#config_cellphone').mask('(00) 90000-0000');
        }
      });


      $('#phone').mask('(00) 0000-00009');
      $('#phone').blur(function(event) {
        if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#phone').mask('(00) 0000-0000');
        } else {
          $('#phone').mask('(00) 90000-0000');
        }
      });

      $('#cellphone').mask('(00) 0000-00009');
      $('#cellphone').blur(function(event) {
        if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#cellphone').mask('(00) 0000-0000');
        } else {
          $('#cellphone').mask('(00) 90000-0000');
        }
      });

      $('#hour').mask('00:00');

      $(".priceFormated").mask('####.##', {
        reverse: true
      });

      var options = {
        onKeyPress: function(cpfcnpj, e, field, options) {
          var masks = ['000.000.000-009', '00.000.000/0000-00'];
          var mask = (cpfcnpj.length > 14) ? masks[1] : masks[0];
          $('#document').mask(mask, options);
        }
      };

      $('#document').mask('000.000.000-009', options);

    });
  </script>

  @yield('jsPage')
</body>

</html>