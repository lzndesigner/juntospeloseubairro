@extends('admin.base')
@section('title', 'Lista de Eventos')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              @if(count($calendars) > 0)
              <table class="table table-hover table-bordered table-striped" id="calendars-table">
                <thead>
                  <tr>
                    <td width="5%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'id']) }}">#</a></td>
                    <td width="6%">Imagem</td>
                    <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'title']) }}">Nome</a></td>
                    <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'day']) }}">Data</a></td>
                    <td><a href="{{ Request::fullUrlWithQuery(['sort' => 'day']) }}">Visualizações</a></td>
                    <td width="14%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'created_at']) }}">Criação</a></td>
                    <td width="14%"><a href="{{ Request::fullUrlWithQuery(['sort' => 'updated_at']) }}">Alteração</a></td>
                    <td width="15%" class="text-right">Ações</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($calendars as $calendar)
                  <tr id="row-{{ $calendar->id }}">
                    <td>
                      <div class="checkbox">
                        <input class="calendars" id="checkbox{{ $calendar->id }}" data-id="{{ $calendar->id }}" type="checkbox">
                        <label for="checkbox{{ $calendar->id }}">
                          <b>{{ $calendar->id }}</b>
                        </label>
                      </div>
                    </td>
                    <td><img src="{{ $calendar->photos->isNotEmpty() ? asset('storage/' . $calendar->photos[0]->filename) : ''}}" alt="{{ $calendar->title }}" class="img-thumbnail"> </td>
                    <td>{{ $calendar->name }}</td>
                    <td>{{ $calendar->day }}/{{ $calendar->month }}/{{ $calendar->year }} - {{ $calendar->hour }}</td>
                    <td>{{ $calendar->views }}</td>
                    <td>{{ \Carbon\Carbon::parse($calendar->created_at)->format('d/m/y H:i')  }}</td>
                    <td>{{ \Carbon\Carbon::parse($calendar->updated_at)->format('d/m/y H:i') }}</td>
                    <td class="text-right">
                      <a href="{{ route('calendars.show', $calendar->id) }}" class="btn btn-xs btn-default btn-icon" data-toggle="tooltip" data-original-title="Detalhes"><i class="fa fa-book"></i></a>
                      <a href="{{ route('calendars.edit', $calendar->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                      <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" data-id="{{ $calendar->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i></a></li>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
              @else
              <div class="alert alert-info">Nenhum calendário cadastrado.</div>
              @endif
              <hr>

            </div><!-- table-responsive -->
          </div><!-- panel-body -->

          <div class="row">
            <div class="col-md-6">
              <a href="{{ route('calendars.create') }}" class="btn btn-thema"><i class="fa fa-plus"></i>Novo</a>
              <a href="#" class="btn btn-danger btn-remove-all"><i class="fa fa-remove"></i>Remover selecionados</a>
            </div><!-- col-md-6 -->

            <div class="col-md-6 text-right">
              {{ $calendars->links() }}
            </div><!-- col-md-6 -->
          </div>
          

        </div><!-- panel-default -->
      </div><!-- col-md-12 -->
      

    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('cssPage')
@endsection

@section('jsPage')
<script>
  $(function() {
    let btn = $('.btn-delete');
    let btnAll = $('.btn-remove-all');

    function remove(id) {
      let tr = $('#calendars-table #row-' + id);
      let request = $.ajax({
       url: '/dashboard/calendars/' + id,
       type: 'DELETE',
       statusCode: {
         204: function() {
           tr.fadeOut();
         },
         403: function() {
           let copy = tr.innerHTML;

           tr.html('').append('<td colspan="7">Essa calendário não foi removida.</td>')
           .css({'background-color': 'red','color': 'white', 'text-align': 'center'});
         }
       }
     });
      return request;
    }

    btn.on('click', function(e) {
      e.preventDefault();
      let action = $(e.currentTarget);
      let request = remove(action.data('id'));
      request.fail(function() {
        window.alert('Não foi possivel remover a calendário')
      });
    })

    btnAll.on('click', function(e) {
      e.preventDefault();

      let checkbox = $('input:checkbox[class="calendars"]:checked');

      if(checkbox.length > 0 && window.confirm('Gostaria de remover a(s) ' + checkbox.length + ' selecionada(s) calendário(s)?')) {
          //
          checkbox.each(function(index, el) {
            let id = $(el).data('id')
            let request = remove(id);
          })
        }

      })
  })
</script>
@endsection