{!! csrf_field() !!}

<div class="row">
  <div class="col-md-2">
    @if( isset($apoio->image))
    <img src="/storage/{{ $apoio->image ?? old('image') }}" alt="Apoios" class="img-thumbnail">
    @else
    <img src="/galerias/sem_image.png" alt="Apoios" class="img-thumbnail">
    @endif
  </div>
  <div class="col-md-10">
    <div class="form-group @if ($errors->has('image')) has-error @endif">
      <label for="image" class="form-label">Imagem do Apoio</label>
      <input type="file" class="form-control" id="image" name="image" value="{{ $apoio->image ?? old('image') }}">
      @if ($errors->has('image'))
      <span class="help-block">
        <strong>{{ $errors->first('image') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
    <div class="alert alert-info">
      <p>O tamanho ideal para a imagem é de <b>550x550 pixels</b>.</p>
    </div>
  </div>
</div>

<hr class="invisible">

<div class="form-group @if ($errors->has('title')) has-error @endif">
  <label for="title" class="form-label">Título do Apoio</label>
  <input type="text" class="form-control" id="title" name="title" value="{{ $apoio->title ?? old('title') }}" placeholder="Título do Apoio" autofocus>
  @if ($errors->has('title'))
  <span class="help-block">
    <strong>{{ $errors->first('title') }}</strong>
  </span>
  @endif
</div><!-- form-group -->

<div class="form-group @if ($errors->has('url')) has-error @endif">
  <label for="url" class="form-label">Link do Apoio</label>
  <input type="text" class="form-control" id="url" name="url" value="{{ $apoio->url ?? old('url') }}" placeholder="URL do Apoio" autofocus>
  @if ($errors->has('url'))
  <span class="help-block">
    <strong>{{ $errors->first('url') }}</strong>
  </span>
  @endif
  <span class="help-block">Caso não possui link, deixar em branco.</span>
</div><!-- form-group -->



  <div class="form-group @if ($errors->has('status')) has-error @endif">
    <label for="status" class="form-label">Status do Apoio</label>
    <select class="form-control" id="status" name="status">
      @if(isset($apoio->status))
      @if($apoio->status == 0 )
      <option value="0" selected>Desabilitado</option>
      <option value="1">Habilitar</option>
      @else
      <option value="1" selected>Habilitado</option>
      <option value="0">Desabilitar</option>
      @endif
      @else
      <option value="1" selected>Habilitado</option>
      <option value="0">Desabilitado</option>
      @endif
    </select>

    @if ($errors->has('status'))
    <span class="help-block">
      <strong>{{ $errors->first('status') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('apoios.index') }}" class="btn btn-default"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>