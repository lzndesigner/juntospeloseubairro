@extends('admin.base')
@section('title', 'Detalhes do Apoio')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $apoio->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('apoios.index') }}">Página de Apoios</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End apoio Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $apoio->title }}</h2>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $apoio->image ?? old('image') }}" alt="Apoios" class="img-thumbnail">

            <p><small>Criado em {{ $apoio->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $apoio->updated_at->format('d/m/Y H:i:s') }}</small></p>
            <blockquote>{!! $apoio->body !!}</blockquote>

            <hr>

            <a href="{{ route('apoios.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('apoios.edit', $apoio->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('apoios.destroy', $apoio->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-apoios').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-apoios" action="{{ route('apoios.destroy', $apoio->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection