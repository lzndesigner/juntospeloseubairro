@extends('admin.base')
@section('title', 'Lista de Apoio')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              @if(count($apoios) > 0)
              <table class="table table-hover table-bordered table-striped" id="apoios-table">
                <thead>
                  <tr>
                    <td width="5%">#</td>
                    <td width="6%">Imagem</td>
                    <td>Título</td>
                    <td width="14%">Criação</td>
                    <td width="15%" class="text-right">Ações</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($apoios as $apoio)
                  <tr id="row-{{ $apoio->id }}">
                    <td>
                      <b>{{ $apoio->id }}</b>
                    </td>
                    <td><img src="/storage/{{ $apoio->image ?? old('image') }}" alt="{{ $apoio->title }}" class="img-thumbnail"> </td>
                    <td>{{ $apoio->title }}</td>
                    <td>{{ \Carbon\Carbon::parse($apoio->created_at)->format('d/m/y H:i')  }}</td>
                    <td class="text-right">
                      <a href="{{ route('apoios.edit', $apoio->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i> Editar</a>
                      <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" data-id="{{ $apoio->id }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i> Remover?</a>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
              @else
              <div class="alert alert-info">Nenhum apoio cadastrado.</div>
              @endif

              <hr>

            </div><!-- table-responsive -->
          </div><!-- panel-body -->

          <div class="row">
            <div class="col-md-6">
              <a href="{{ route('apoios.create') }}" class="btn btn-thema"><i class="fa fa-plus"></i>Novo</a>
            </div><!-- col-md-6 -->

            <div class="col-md-6 text-right">
              {{ $apoios->links() }}
            </div><!-- col-md-6 -->
          </div>


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('jsPage')
<script>
$(function() {
  let btn = $('.btn-delete');
  let btnAll = $('.btn-remove-all');

  function remove(id) {
    let tr = $('#apoios-table #row-' + id);
    let request = $.ajax({
     url: '/dashboard/apoios/' + id,
     type: 'DELETE',
     statusCode: {
       204: function() {
         tr.fadeOut();
       },
       403: function() {
         let copy = tr.innerHTML;

         tr.html('').append('<td colspan="7">Esse apoio não foi removido.</td>')
         .css({'background-color': 'red','color': 'white', 'text-align': 'center'});
       }
     }
   });
    return request;
  }

  btn.on('click', function(e) {
    e.preventDefault();
    let action = $(e.currentTarget);
    let request = remove(action.data('id'));
    request.fail(function() {
      window.alert('Não foi possivel remover o apoio')
    });
  })

  btnAll.on('click', function(e) {
    e.preventDefault();

    let checkbox = $('input:checkbox[class="apoios"]:checked');

    if(checkbox.length > 0 && window.confirm('Gostaria de remover a(s) ' + checkbox.length + ' selecionada(s) apoio(s)?')) {
          //
          checkbox.each(function(index, el) {
            let id = $(el).data('id')
            let request = remove(id);
          })
        }

      })
})
</script>
@endsection