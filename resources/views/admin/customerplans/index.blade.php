@extends('admin.base')
@section('title', 'Lista de Planos dos Clientes')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsiveoff">
              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <th>#</th>
                  <th>Cliente</th>
                  <th>Estabelecimento</th>
                  <th>Plano</th>
                  <th>Vencimento</th>
                  <th>Valor</th>
                  <th>Status</th>
                  <th></th>
                </thead>
                <tbody>
                  @foreach($customerplans as $customerplan)
                  <tr>
                    <td>{{ $customerplan->id }}</td>
                    <td>{{ $customerplan->cliente }} <a href="{{ route('customers.edit', $customerplan->clienteid) }}" class="btn-xs" data-toggle="tooltip" data-original-title="Editar Cliente"><i class="fa fa-edit"></i></a></td>
                    <td>{{ $customerplan->estabelecimento }} <a href="{{ route('establishments.edit', $customerplan->estabelecimentoid) }}" class="btn-xs" data-toggle="tooltip" data-original-title="Editar Estabelecimento"><i class="fa fa-edit"></i></a> </td>
                    <td>{{ $customerplan->plano }}</td>
                    <td>
                      <div id="dateAtual{{$customerplan->id}}">
                        {{ ($customerplan->plan_dateend ? \Carbon\Carbon::parse($customerplan->plan_dateend)->format('d/m/y') : '-') }}
                      </div>
                    </td>
                    <td>{{ $customerplan->price }}</td>
                    <td>
                      <div class="btn-group pull-right">
                        <div id="statusAtual{{$customerplan->id}}" class="pull-left">
                          @if($customerplan->status == 'Pago')
                          <span class="label label-success">{{$customerplan->status}}</span>
                          @elseif($customerplan->status == 'Pendente')
                          <span class="label label-warning">{{$customerplan->status}}</span>
                          @elseif($customerplan->status == 'Aguardando')
                          <span class="label label-primary">{{$customerplan->status}}</span>
                          @elseif($customerplan->status == 'Expirado')
                          <span class="label label-danger">{{$customerplan->status}}</span>
                          @endif
                        </div>
                        @if($customerplan->plano)
                        <button type="button" data-toggle="dropdown" class="btn btn-xs btn-dark dropdown-toggle"><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a class="ajaxstatus" data-customerplan_id="{{$customerplan->id}}" data-typestatus="Aguardando">Aguardando</a></li>
                          <li><a class="ajaxstatus" data-customerplan_id="{{$customerplan->id}}" data-typestatus="Pendente">Pendente</a></li>
                          <li><a class="ajaxstatus" data-customerplan_id="{{$customerplan->id}}" data-typestatus="Expirado">Expirado</a></li>
                          <li><a class="ajaxstatus" data-customerplan_id="{{$customerplan->id}}" data-typestatus="Pago">Pago</a></li>
                        </ul>
                        @endif
                      </div>
                    </td>
                    <td>
                      <a href="{{ route('customerplans.edit', $customerplan->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar Plano"><i class="fa fa-edit"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-12 text-right">
                {{ $customerplans->links() }}
              </div><!-- col-md-12 -->
            </div>

          </div><!-- panel-body -->
        </div><!-- panel-default -->
      </div><!-- col-md-12 -->
    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
<script type="text/javascript">
  $('.ajaxstatus').click(function() {
    console.log('clicou');
    var customerplan_id = $(this).data("customerplan_id");
    var type_status = $(this).data("typestatus");

    $.ajax({
      url: '{{route('customers.setstatus')}}',
      type: 'get',
      data: {
        customerplan_id: customerplan_id,
        type_status: type_status,
      },
      dataType: 'html',
      success: function(html) {
        var dateend = JSON.parse(html).dateformat;
        if (html != '') {
          if (type_status == 'Pago') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-success">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-success">' + type_status + '</span>');
          } else if (type_status == 'Pendente') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-warning">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-warning">' + type_status + '</span>');
          } else if (type_status == 'Aguardando') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-primary">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-primary">' + type_status + '</span>');
          } else if (type_status == 'Expirado') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-danger">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-danger">' + type_status + '</span>');
          } else {
            $('#dateAtual' + customerplan_id).html('<span class="label label-default">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-default">' + type_status + '</span>');
          }
        }
      }
    });
  });
</script>
@endsection