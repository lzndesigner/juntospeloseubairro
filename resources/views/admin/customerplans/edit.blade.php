@extends('admin.base')
@section('title', 'Editar Plano')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') do {{ $customerplans->estabelecimento }} - {{ $customerplans->cliente }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('customers.index') }}">Página de Clientes</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End plan Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">{{ $customerplans->estabelecimento }} - {{ $customerplans->cliente }}</div>

          <div class="panel-body">

            <form action="{{ route('customerplans.update', $customerplans->id)}}" enctype="multipart/form-data" method="POST">
              <input type="hidden" name="_method" value="PUT">
              @include('admin.customerplans.form')
            </form>

          </div><!-- panel-body -->


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link rel="stylesheet" href="/backend/js/bootstrap-datepicker/css/bootstrap-datepicker.css" />
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/front/js/summernote/summernote.min.js?1"></script>
<script type="text/javascript" src="/front/js/summernote/summernote_lang_pt-br.js?1"></script>
<script type="text/javascript" src="/backend/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/backend/js/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js"></script>
<script type="text/javascript">
$('.datepicker').datepicker({
  format: "yyyy-mm-dd",
  inline: true,
  language: "pt-BR",
  todayHighlight: true
});
</script>

<script>
  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote({
      lang: 'pt-BR'
    });
    $('button[data-event="showImageDialog"], button[data-event="showVideoDialog"], button[data-event="codeview"], .note-toolbar .note-table').hide();
  });

  jQuery(function($) {
    $(document).on('keypress', '#period', function(e) {
      var square = document.getElementById("period");
      var key = (window.event) ? event.keyCode : e.which;
      if ((key > 47 && key < 58)) {
        period.style.backgroundColor = "#ffffff";
        return true;

      } else {
        period.style.backgroundColor = "rgba(255, 0, 0, 0.2)";
        return (key == 8 || key == 0) ? true : false;

      }
    });
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/backend/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="title"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>
@endsection