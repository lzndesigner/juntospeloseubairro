@extends('admin.base')
@section('title', 'Detalhes do Plano')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $plan->name }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('plans.index') }}">Página de Planos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End plan Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            <h2>{{ $plan->name }}</h2>
            <h4>{{ $plan->getPostCategory() }}</h4>
          </div>

          <div class="panel-body">

            <img src="/storage/{{ $plan->image }}" alt="Calendário" class="img-thumbnail">

            <p><small>Criado em {{ $plan->created_at->format('d/m/Y H:i:s') }} | Atualizado em {{ $plan->updated_at->format('d/m/Y H:i:s') }}</small></p>

            <blockquote>
              <p><b>Localização</b></p>
              <ul>
                <li>Proprietário: {{ $plan->proprietary }}</li>
                <li>CEP: {{ $plan->cep }}</li>
                <li>Endereço: {{ $plan->address }}, N° {{ $plan->number }}</li>
                <li>Bairro: {{ $plan->distric }}</li>
                <li>Cidade: {{ $plan->city }}</li>
                <li>Estado: {{ $plan->state }}</li>
                <li>Telefone: {{ $plan->phone }}</li>
                @if($plan->cellphone)
                  <li>Celular: {{ $plan->cellphone }}</li>
                @endif

                <li>Status: <span class="label label-{{ ($plan->status == '1' ? 'success' : 'danger' ) }}">
                      {{ ($plan->status == '1' ? 'Habilitado' : 'Desabilitado' ) }}
                    </span></li>
              </ul>
            </blockquote>

            @if($plan->description)
            <blockquote>
              {{ $plan->description }}
            </blockquote>
            @endif

            <hr>

            <a href="{{ route('plans.index') }}" class="btn btn-xs btn-warning">Voltar</a>
            <a href="{{ route('plans.edit', $plan->id) }}" class="btn btn-xs btn-primary">Editar</a>
            <a href="{{ route('plans.destroy', $plan->id) }}" class="btn btn-xs btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-plans').submit();"><i class="fa fa-remove"></i> Deletar</a></li>
            <form id="delete-form-plans" action="{{ route('plans.destroy', $plan->id) }}" method="POST" style="display: none;">
             {!! csrf_field() !!}
             <input type="hidden" name="_method" value="DELETE">
           </form>

         </div><!-- panel-body -->


       </div><!-- panel-default -->
     </div><!-- col-md-12 -->


   </div><!-- row -->
 </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
@endsection

@section('jsPage')
@endsection