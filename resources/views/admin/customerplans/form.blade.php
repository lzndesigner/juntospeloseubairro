{!! csrf_field() !!}
<input type="hidden" class="form-control" id="establishment_id" name="establishment_id" value="{{ $customerplans->estabelecimentoid }}" placeholder="Nome do Estabelecimento" readonly>
<input type="hidden" class="form-control" id="customer_id" name="customer_id" value="{{ $customerplans->clienteid }}" placeholder="Nome do Estabelecimento" readonly>

<div class="row">
  <div class="col-xs-12 col-md-4">
    <div class="form-group @if ($errors->has('plan_id')) has-error @endif">
      <label for="plan_id" class="form-label">Plano Escolhido</label>
      <select class="form-control" id="plan_id" name="plan_id">
        @if(isset($customerplans->planoid))
        <optgroup label="Escolhido">
          <option value="{{ $customerplans->planoid }}" selected>{{ $customerplans->plano }}</option>
        </optgroup>
        @else
        <option value="" selected disabled>Nenhum plano escolhido</option>
        @endif

        <optgroup label="Escolha outro Plano">
          @foreach($plans as $plan)
          <option value="{{ $plan->id }}">{{ $plan->name }}</option>
          @endforeach
        </optgroup>
      </select>

      @if ($errors->has('plan_id'))
      <span class="help-block">
        <strong>{{ $errors->first('plan_id') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- col -->
  <div class="col-xs-12 col-md-4">
    <div class="form-group @if ($errors->has('plan_status')) has-error @endif">
      <label for="plan_status" class="form-label">Status do Plano</label>
      <select class="form-control" id="plan_status" name="plan_status">
        @if(isset($customerplans->status))
        @if($customerplans->status == 'Pago' )
        <option value="Pago" selected>Pago</option>
        <option value="Pendente">Pendente</option>
        <option value="Aguardando">Aguardando</option>
        <option value="Expirado">Expirado</option>
        @elseif($customerplans->status == 'Pendente' )
        <option value="Pendente" selected>Pendente</option>
        <option value="Pago">Pago</option>
        <option value="Aguardando">Aguardando</option>
        <option value="Expirado">Expirado</option>
        @elseif($customerplans->status == 'Aguardando' )
        <option value="Aguardando" selected>Aguardando</option>
        <option value="Pago">Pago</option>
        <option value="Pendente">Pendente</option>
        <option value="Expirado">Expirado</option>
        @else
        <option value="Expirado" selected>Expirado</option>
        <option value="Pago">Pago</option>
        <option value="Pendente">Pendente</option>
        <option value="Aguardando">Aguardando</option>
        @endif
        @else
        <option value="" selected disabled>Selecione um Status</option>
        <option value="Pago">Pago</option>
        <option value="Pendente">Pendente</option>
        <option value="Aguardando">Aguardando</option>
        <option value="Expirado">Expirado</option>
        @endif
      </select>

      @if ($errors->has('status'))
      <span class="help-block">
        <strong>{{ $errors->first('status') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- col -->
  <div class="col-xs-12 col-md-4">
    <div class="form-group @if ($errors->has('plan_method')) has-error @endif">
      <label for="plan_method" class="form-label">Método de Pagamento</label>
      <select class="form-control" id="plan_method" name="plan_method">
        @if($customerplans->plan_method)
        @if($customerplans->plan_method == 'banktransfer' )
        <option value="banktransfer" selected>Depósito Bancário</option>
        <option value="mercadopago">Mercado Pago</option>
        <option value="gratis">Grátis</option>
        @elseif($customerplans->plan_method == 'mercadopago' )
        <option value="mercadopago" selected>Mercado Pago</option>
        <option value="banktransfer">Depósito Bancário</option>
        <option value="gratis">Grátis</option>
        @else
        <option value="gratis" selected>Grátis</option>
        <option value="mercadopago">Mercado Pago</option>
        <option value="banktransfer">Depósito Bancário</option>
        @endif
        @else
        <option value="" selected disabled>Selecione uma Forma</option>
        <option value="mercadopago">Mercado Pago</option>
        <option value="banktransfer">Depósito Bancário</option>
        <option value="gratis">Grátis</option>
        @endif
      </select>

      @if ($errors->has('plan_method'))
      <span class="help-block">
        <strong>{{ $errors->first('plan_method') }}</strong>
      </span>
      @endif
    </div><!-- form-group -->
  </div><!-- col -->
</div><!-- row -->
<div class="row">
      <div class="col-xs-12 col-md-6">
        <div class="form-group @if ($errors->has('plan_datebegin')) has-error @endif">
          <label for="plan_datebegin" class="form-label">Data do Início</label>
          <input type="text" class="form-control datepicker" id="plan_datebegin" name="plan_datebegin" value="{{ $customerplans->plan_datebegin ?? old('plan_datebegin') }}" placeholder="Data de Início">
          <div id="datePickermodel"></div>
          <span class="help-block">Preservar data no formato EUA (ano-mes-dia).</span>

          @if ($errors->has('plan_datebegin'))
          <span class="help-block">
            <strong>{{ $errors->first('plan_datebegin') }}</strong>
          </span>
          @endif
        </div><!-- form-group -->
      </div><!-- col -->
      <div class="col-xs-12 col-md-6">
        <div class="form-group @if ($errors->has('plan_dateend')) has-error @endif">
          <label for="plan_dateend" class="form-label">Data do Vencimento</label>
          <input type="text" class="form-control datepicker" id="plan_dateend" name="plan_dateend" value="{{ $customerplans->plan_dateend ?? old('plan_dateend') }}" placeholder="Data de Vencimento">
          <div id="datePickermodel"></div>

          <span class="help-block">Preservar data no formato EUA (ano-mes-dia).</span>
          @if ($errors->has('plan_dateend'))
          <span class="help-block">
            <strong>{{ $errors->first('plan_dateend') }}</strong>
          </span>
          @endif
        </div><!-- form-group -->
      </div><!-- col -->
  </div>
  <hr>

  <div class="row">
    <div class="col-md-6">
      <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i> Salvar</button>
      <a href="{{ route('customerplans.index') }}" class="btn btn-default"><i class="fa fa-reply"></i> Cancelar</a>
    </div><!-- col-md-6 -->
  </div>