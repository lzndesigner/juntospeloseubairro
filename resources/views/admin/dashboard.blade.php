@extends('admin.base')
@section('title', 'Painel de Controle')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-success">
        <div class="panel-heading">Seja bem vindo!</div>

        <div class="panel-body">
          Você está no seu <b>Painel Administrativo</b>, nessa área você terá acesso aos usuários administrativo cadastrados, toda a configuração geral do site e muitos outros recursos nesse completo Gerenciador de Conteúdo.
        </div>
      </div>
    </div>
  </div>

  <div class="row">
        <div class="col-md-12 col-lg-4 col-xl-4">
          <section class="panel panel-featured-left panel-featured-primary">
            <div class="panel-body">
              <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                  <div class="summary-icon bg-primary">
                    <i class="fa fa-users"></i>
                  </div>
                </div>
                <div class="widget-summary-col">
                  <div class="summary">
                    <h4 class="title">Clientes</h4>
                    <div class="info">
                      <strong class="amount">{{ $totalCustomers }}</strong>
                    </div>
                  </div>
                  <div class="summary-footer">
                    <a href="{{ route('customers.index') }}" class="text-muted text-uppercase">(ver todos)</a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div class="col-md-12 col-lg-4 col-xl-4">
          <section class="panel panel-featured-left panel-featured-secondary">
            <div class="panel-body">
              <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                  <div class="summary-icon bg-secondary">
                    <i class="fa fa-building"></i>
                  </div>
                </div>
                <div class="widget-summary-col">
                  <div class="summary">
                    <h4 class="title">Estabelecimentos</h4>
                    <div class="info">
                      <strong class="amount">{{ $totalEstablishments }}</strong>
                    </div>
                  </div>
                  <div class="summary-footer">
                  <a href="{{ route('establishments.index') }}" class="text-muted text-uppercase">(ver todos)</a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div class="col-md-12 col-lg-4 col-xl-4">
          <section class="panel panel-featured-left panel-featured-tertiary">
            <div class="panel-body">
              <div class="widget-summary">
                <div class="widget-summary-col widget-summary-col-icon">
                  <div class="summary-icon bg-tertiary">
                    <i class="fa fa-shopping-cart"></i>
                  </div>
                </div>
                <div class="widget-summary-col">
                  <div class="summary">
                    <h4 class="title">Planos Ativos</h4>
                    <div class="info">
                      <strong class="amount">{{ $totalCustomerPlans }}</strong>
                    </div>
                  </div>
                  <div class="summary-footer">
                  <a href="{{ route('customerplans.index') }}" class="text-muted text-uppercase">(ver todos)</a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div><!-- row -->


  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Próximos Vencimentos</div>

        <div class="panel-body">
          <div class="row">
            <div class="col-xs-12 col-md-12">
              <div class="table-responsiveoff">
                <table class="table table-hover table-striped table-bordered">
                  <thead>
                    <th><a href="{{ request()->fullUrlWithQuery(['column' => 'id', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> #</th>
                    <th><a href="{{ request()->fullUrlWithQuery(['column' => 'cliente', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Cliente</th>
                    <th><a href="{{ request()->fullUrlWithQuery(['column' => 'estabelecimento', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Estabelecimento</th>
                    <th>Telefone</th>
                    <th><a href="{{ request()->fullUrlWithQuery(['column' => 'plano', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Plano</th>
                    <th><a href="{{ request()->fullUrlWithQuery(['column' => 'plan_dateend', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Vencimento</th>
                    <th>Valor</th>
                    <th width='5%'>Renov.</th>
                    <th>Status</th>
                    <th></th>
                  </thead>
                  <tbody>
                    @foreach($proxvencimentos as $proxvencimento)
                    <tr>
                      <td>{{ $proxvencimento->id }}</td>
                      <td>{{ $proxvencimento->cliente }} <a href="{{ route('customers.edit', $proxvencimento->clienteid) }}" class="btn-xs" data-toggle="tooltip" data-original-title="Editar Cliente"><i class="fa fa-edit"></i></a></td>
                      <td>{{ $proxvencimento->estabelecimento }} <a href="{{ route('establishments.edit', $proxvencimento->estabelecimentoid) }}" class="btn-xs" data-toggle="tooltip" data-original-title="Editar Estabelecimento"><i class="fa fa-edit"></i></a> </td>
                      @if($proxvencimento->iswhatsapp == 0)
                      <td><span class="label label-primary"><i class="fa fa-phone"></i></span> {{ $proxvencimento->phone }}</td>
                      @else
                      @php $retira = ["(", ")", " ", "-"]; @endphp
                      <td>
                      <a href="https://api.whatsapp.com/send?phone=55{{ str_replace($retira, '', $proxvencimento->phone) }}" target="_Blank">
                        <span class="label label-success"><i class="fab fa-whatsapp"></i></span>
                        {{ $proxvencimento->phone }}</a>
                      </td>
                      @endif
                      <td>{{ ($proxvencimento->plano ? $proxvencimento->plano : '-') }}</td>
                      <td>
                      @if($proxvencimento->plan_dateend)
                        <div id="dateAtual{{$proxvencimento->id}}">
                          {{ \Carbon\Carbon::parse($proxvencimento->plan_dateend)->format('d/m/y') }}
                        </div>
                        @else
                        -
                        @endif
                      </td>
                      <td>{{ ($proxvencimento->price ? $proxvencimento->price : '-')}}</td>
                      <td>{{ $proxvencimento->renews}} vezes</td>
                      <td>
                        <div class="btn-group pull-right">
                          <div id="statusAtual{{$proxvencimento->id}}" class="pull-left">
                            @if($proxvencimento->status == 'Pago')
                            <span class="label label-success">{{$proxvencimento->status}}</span>
                            @elseif($proxvencimento->status == 'Pendente')
                            <span class="label label-warning">{{$proxvencimento->status}}</span>
                            @elseif($proxvencimento->status == 'Aguardando')
                            <span class="label label-primary">{{$proxvencimento->status}}</span>
                            @elseif($proxvencimento->status == 'Expirado')
                            <span class="label label-danger">{{$proxvencimento->status}}</span>
                            @endif
                          </div>
                          @if($proxvencimento->plano)
                          <button type="button" data-toggle="dropdown" class="btn btn-xs btn-dark dropdown-toggle"><span class="caret"></span></button>
                          <ul class="dropdown-menu dropdown-menu-right">
                            <li><a class="ajaxstatus" data-customerplan_id="{{$proxvencimento->id}}" data-typestatus="Aguardando">Aguardando</a></li>
                            <li><a class="ajaxstatus" data-customerplan_id="{{$proxvencimento->id}}" data-typestatus="Pendente">Pendente</a></li>
                            <li><a class="ajaxstatus" data-customerplan_id="{{$proxvencimento->id}}" data-typestatus="Expirado">Expirado</a></li>
                            <li><a class="ajaxstatus" data-customerplan_id="{{$proxvencimento->id}}" data-typestatus="Pago">Pago</a></li>
                          </ul>
                          @endif
                        </div>
                      </td>
                      <td>
                        <a href="{{ route('customerplans.edit', $proxvencimento->id) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar Plano"><i class="fa fa-edit"></i></a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <hr>
              <div class="row">
                <div class="col-xs-12 col-md-9">
                  {{ $proxvencimentos->appends(request()->except('page'))->links() }}
                </div>
                <div class="col-xs-12 col-md-3 pull-right text-right">
                  <p>Mostrando {{ count($proxvencimentos) }} de {{ $totalvencimentos }}</p>
                </div>
              </div>

            </div>
          </div><!-- row -->
        </div>
      </div>
    </div>
  </div>
</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('jsPage')
<script type="text/javascript">
  $('.ajaxstatus').click(function() {
    console.log('clicou');
    var customerplan_id = $(this).data("customerplan_id");
    var type_status = $(this).data("typestatus");

    $.ajax({
      url: '{{route('customers.setstatus')}}',
      type: 'get',
      data: {
        customerplan_id: customerplan_id,
        type_status: type_status,
      },
      dataType: 'html',
      success: function(html) {
        var dateend = JSON.parse(html).dateformat;
        if (html != '') {
          if (type_status == 'Pago') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-success">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-success">' + type_status + '</span>');
          } else if (type_status == 'Pendente') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-warning">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-warning">' + type_status + '</span>');
          } else if (type_status == 'Aguardando') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-primary">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-primary">' + type_status + '</span>');
          } else if (type_status == 'Expirado') {
            $('#dateAtual' + customerplan_id).html('<span class="label label-danger">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-danger">' + type_status + '</span>');
          } else {
            $('#dateAtual' + customerplan_id).html('<span class="label label-default">' + dateend + '</span>');
            $('#statusAtual' + customerplan_id).html('<span class="label label-default">' + type_status + '</span>');
          }
        }
      }
    });
  });
</script>
@endsection