@extends('admin.base')
@section('title', 'Lista de Estabelecimentos')

@section('content')


<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard/') }}">Inicio</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  @include('elements.messages')

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">
            <div class="table-responsive">
              @if(count($establishments) > 0)
              <table class="table table-hover table-bordered table-striped" id="establishments-table">
                <thead>
                  <tr>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'estabelecimentoid', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> #</td>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'estabelecimento', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Estabelecimento</td>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'cliente', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Proprietário</td>
                    <td>Telefone</td>
                    <td><a href="{{ request()->fullUrlWithQuery(['column' => 'views', 'order' => $order]) }}"><i class="fas fa-sort"></i></a> Views</td>
                    <td>Status</td>
                    <td>Criação</td>
                    <td>Alteração</td>
                    <td class="text-right" width="10%">Ações</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach($establishments as $establishment)
                  <tr id="row-{{ $establishment->estabelecimentoid }}">
                    <td>
                          <b>{{ $establishment->estabelecimentoid }}</b>
                    </td>
                    <td>{{ $establishment->estabelecimento }} <br> <small>- Categoria: {{ $establishment->categoria }}</small></td>
                    <td>{{ $establishment->cliente }}</td>
                    <td>
                    @php $retira = ["(", ")", " ", "-"]; @endphp
                    @if($establishment->iswhatsapp == 1)
                    <a href="https://api.whatsapp.com/send?phone=55{{ str_replace($retira, '', $establishment->phone) }}" target="_Blank">
                        <span class="label label-success"><i class="fab fa-whatsapp"></i></span>
                        {{ $establishment->phone }}</a>
                    @else
                    <span class="label label-primary"><i class="fa fa-phone"></i></span> {{ $establishment->phone }} 
                    @endif
                      @if($establishment->cellphone) <br> <span class="label label-info"><i class="fa fa-phone"></i></span>  {{ $establishment->cellphone }} @endif
                    </td>
                    <td>{{ $establishment->views }}</td>
                    <td><span class="label label-{{ ($establishment->status == '1' ? 'success' : 'danger' ) }}">
                      {{ ($establishment->status == '1' ? 'Habilitado' : 'Desabilitado' ) }}
                    </span>
                  </td>
                  <td>{{ \Carbon\Carbon::parse($establishment->created_at)->format('d/m/y H:i')  }}</td>
                  <td>{{ \Carbon\Carbon::parse($establishment->updated_at)->format('d/m/y H:i') }}</td>
                  <td class="text-right">
                    <a href="{{ route('establishments.edit', $establishment->estabelecimentoid) }}" class="btn btn-xs btn-primary btn-icon" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-edit"></i> Editar</a>
                    <a href="#" class="btn btn-xs btn-danger btn-icon btn-delete" data-id="{{ $establishment->estabelecimentoid }}" data-toggle="tooltip" data-original-title="Remover"><i class="fa fa-remove"></i> Remover?</a>
                  </td>
                </tr>
                @endforeach

              </tbody>
            </table>
            @else
            <div class="alert alert-info">Nenhum estabelecimento cadastrado.</div>
            @endif
            <hr>

          </div><!-- table-responsive -->
        </div><!-- panel-body -->

        <div class="row">
          <div class="col-md-12 text-right">
            {{ $establishments->appends(request()->except('page'))->links() }}
          </div><!-- col-md-6 -->
        </div>


      </div><!-- panel-default -->
    </div><!-- col-md-12 -->


  </div><!-- row -->
</div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('jsPage')
<script>
$(function() {
  let btn = $('.btn-delete');
  let btnAll = $('.btn-remove-all');

  function remove(id) {
    let tr = $('#establishments-table #row-' + id);
    let request = $.ajax({
     url: '/dashboard/establishments/' + id,
     type: 'DELETE',
     statusCode: {
       204: function() {
         tr.fadeOut();
       },
       403: function() {
         let copy = tr.innerHTML;

         tr.html('').append('<td colspan="7">Essa estabelecimento não foi removida.</td>')
         .css({'background-color': 'red','color': 'white', 'text-align': 'center'});
       }
     }
   });
    return request;
  }

  btn.on('click', function(e) {
    e.preventDefault();
    let action = $(e.currentTarget);
    let request = remove(action.data('id'));
    request.fail(function() {
      window.alert('Não foi possivel remover a estabelecimento')
    });
  })

  btnAll.on('click', function(e) {
    e.preventDefault();

    let checkbox = $('input:checkbox[class="establishments"]:checked');

    if(checkbox.length > 0 && window.confirm('Gostaria de remover a(s) ' + checkbox.length + ' selecionada(s) estabelecimento(s)?')) {
          //
          checkbox.each(function(index, el) {
            let id = $(el).data('id')
            let request = remove(id);
          })
        }

      })
})
</script>
@endsection