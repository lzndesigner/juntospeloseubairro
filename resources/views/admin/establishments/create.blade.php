@extends('admin.base')
@section('title', 'Novo Estabelecimento')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title')</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('establishments.index') }}">Página de Estabelecimentos</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End Page Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">

          <div class="panel-title">
            @yield('title')
          </div>

          <div class="panel-body">

          <form action="{{ route('establishments.store')}}" enctype="multipart/form-data" method="POST">
              @component('admin.establishments.form')
              <div class="row">
                <div class="col-xs-12 col-md-4">
                  <div class="heading-block">
                    <h4>Foto principal do anúncio</h4>
                  </div><!-- heading -->
                  <div class="col-xs-12">
                    <div class="panel panel-default">
                      <div class="panel-body text-center">
                        <input type="file" style="opacity:0; height:15px;" name="image" required id="input-add-image" accept="image/jpg, image/jpeg, image/png" autofocus>
                        <!-- Capa Principal -->
                        <div id="box-imagens" class="gallery-box">
                          <ul>
                            <li id="add-image-to-establishment" style="cursor:pointer;">
                              <picture>
                                <img id="capa-0" src="{{ asset('galerias/sem_image.png') }}" alt="" class="img-responsive">
                              </picture>
                              <div class="btn-group">
                                <button type="button" class="btn btn-xs btn-thema"><i class="fa fa-image"></i></button>
                              </div>
                            </li>
                          </ul>
                          <p><b>Use seu logo</b></p>

                        </div><!-- gallery -->
                        <div class="form-group">

                          @if ($errors->has('image'))
                          <div class="alert alert-danger">
                            <span class="help-block">
                              <strong>{{ $errors->first('image') }}</strong>
                            </span>
                          </div><!-- alert -->
                          @endif
                        </div><!-- form-group -->
                      </div>
                    </div><!-- panel -->
                  </div><!-- col -->
                </div><!-- col -3 -->

                <div class="col-xs-12 col-md-8">
                  <div class="heading-block">
                    <h4>Miniaturas</h4>
                  </div><!-- heading -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <!-- Miniaturas -->
                      <input type="file" multiple name="photos[]" id="photos" style="opacity:0; width:40px; height:10px;" accept="image/jpg, image/jpeg, image/png">
                      <div class="gallery-box">
                        <ul>
                          <li style="border-right:1px solid #DDD;">
                            <picture>
                              <button class="btn btn-default" id="add-photos-to-establishment" type="button">
                                <i class="fa fa-plus"></i>
                                <span>Nova Miniatura</span>
                              </button>
                            </picture>
                            <hr class="invisible my-1">
                            @for ($i = 0; $i < 5; $i++) @php $errorsPhotos='photos.' .$i @endphp @if ($errors->has($errorsPhotos))
                              <div class="alert alert-danger">
                                <span class="help-block">
                                  <strong>{{ $errors->first($errorsPhotos) }}</strong>
                                </span>
                              </div><!-- alert -->
                              @endif
                              @endfor
                          </li>
                        </ul>
                        <ul id="box-imagens-miniaturas"></ul>
                      </div><!-- end box-imagns -->
                      <p><small>- Tamanho máximo de 2mb por foto.</small></p>
                    </div>
                  </div>
                </div><!-- col -9 -->
              </div><!-- row -->
              @endcomponent
            </form>

          </div><!-- panel-body -->


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection


@section('cssPage')
<link href="{{ asset('front/js/summernote/summernote.css?13') }}" rel="stylesheet">
<link href="{{ asset('front/js/summernote/summernote-bs4.css?13') }}" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="/js/dropzone/basic.css?1" />
<link type="text/css" rel="stylesheet" href="/js/dropzone/dropzone.css?1" />
<style>
  .dropzone {
    min-height: 150px;
    border: 2px solid rgb(58, 184, 191);
    background: white;
    padding: 10px;
    margin: 10px;
    border-radius: 20px;
  }

  .dropzone h4 {
    color: rgb(58, 184, 191);
  }

  .dropzone .dz-preview .dz-remove {
    font-size: 12px;
  }
</style>
@endsection

@section('jsPage')
<script>
  // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
  $("form").submit(function(e) {
    console.log('teste');
    //disable the submit button
    $("form .btn.btn-thema").attr("disabled", true);
    $('form .btn.btn-thema').parent().append('<i class="fa fa-spinner fa-spin ml-3"></i>');
    setTimeout(function() {
      $('form .btn.btn-thema').prop("disabled", false);
      $('form .btn.btn-thema').parent().find('svg.fa-spinner').html('');
    }, 4000);
  });
</script>

<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/front/js/summernote/summernote.js?2"></script>
<script type="text/javascript" src="/front/js/summernote/lang/summernote-pt-BR.js?2"></script>
<script type="text/javascript" src="/js/dropzone/dropzone.js?2"></script>
<script>
  // Dropzone.autoDiscover = false;
  // jQuery(document).ready(function() {

  //   $("div#my-awesome-dropzone").dropzone({
  //     url: '{{ route('estabelecimentos.store') }}',
  //     paramName: "file",
  //     autoProcessQueue: false,
  //     addRemoveLinks: true,
  //     dictRemoveFile: "Remover imagem",
  //     acceptedFiles: ".png,.jpg,.jpeg",
  //     hiddenInputContainer: 'form',
  //   });

  // });

  /*
 var uploadedDocumentMap = {}
  Dropzone.options.documentDropzone = {
    url: '/',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
      uploadedDocumentMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedDocumentMap[file.name]
      }
      $('form').find('input[name="document[]"][value="' + name + '"]').remove()
    },
    init: function () {
      @if(isset($project) && $project->document)
      var files =
      {!! json_encode($project->document) !!}
      for (var i in files) {
        var file = files[i]
        this.options.addedfile.call(this, file)
        file.previewElement.classList.add('dz-complete')
        $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
      }
      @endif
    }
  } */
  function readURL(input) {
    if (input.files && input.files[0]) {
      for (x in input.files) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[x]);
        reader.onload = function(e) {
          $('<li><picture><img></picture></li>').appendTo("ul#box-imagens-miniaturas").children("picture").children("img").attr('src', e.target.result);
        }
      }
    }
  }

  $("#photos").change(function() {
    $("ul#box-imagens-miniaturas").html('');
    readURL(this);
  });



  $('#add-photos-to-establishment').click(function(e) {
    e.preventDefault();
    $("#photos").click();
  });
</script>
<script>
  $("#type option:selected").each(function() {
    var selectedType = $(this).val();
    if (selectedType == 'Virtual') {
      $("#number").removeAttr("required");
      $("#reqnumber").html('');
    } else {
      $("#number").attr("required", "req");
      $("#reqnumber").html('(<strong class="text-danger">*</strong>)');
    }
  });

  $('#type').change(function() {
    var selectedType = $(this).val();
    if (selectedType == 'Virtual') {
      $("#number").removeAttr("required");
      $("#reqnumber").html('');
    } else {
      $("#number").attr("required", "req");
      $("#reqnumber").html('(<strong class="text-danger">*</strong>)');
    }
    console.log(selectedType);
  });

  // $("button[type='submit']").click(function() {
  //   var $fileUpload = $("input[type='file']");
  //   if (parseInt($fileUpload.get(0).files.length) > 6) {

  //     $('body,html').stop(true).animate({
  //       'scrollTop': 0
  //     }, 700, 'easeOutQuad');

  //     $('.error-image').html('');

  //     $('.error-image').append('<br><div class="alert alert-danger">Você só pode enviar apenas 6 fotos.</div>');
  //     // alert("Você só pode enviar 6 fotos.");
  //     return false;
  //   }
  // });


  /* Adicionar arquivos */
  $('#add-image-to-establishment').click(function(e) {
    e.preventDefault();
    $("#input-add-image").click();
  });

  $("#input-add-photos").change(function(e) {
    let display = $('span#display-add-photos'),
      files = e.target.files,
      count = files.length;

    if (count > 0) {
      display.empty().html(count + ' selecionados.')
    } else {
      display.empty().html('0 selecionados.')
    }
  });


  /* Gerar Previa */
  $('#input-add-image').change(function(e) {
    console.log('updated.')
    const ref = e.target;

    if (!ref.files) return;

    let id = ref.id,
      // image = $('#photo-preview-' + id),
      image = $('#capa-0'),
      file = ref.files[0],
      old = image.attr('src')

    var reader = new FileReader();

    reader.onload = function(event) {

      image.css({
        filter: blur(1.5)
      })

      image.fadeOut(500, function() {
        image.attr('src', event.target.result);
        image.fadeIn(200, function() {
          image.css({
            filter: blur(0)
          })
        });
      })
    }
    //
    reader.readAsDataURL(file);
  });


  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote({
      lang: 'pt-BR'
    });
    $('button[data-event="showImageDialog"], button[data-event="showVideoDialog"], button[data-event="codeview"], .note-toolbar .note-table').hide();
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/front/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="name"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"]',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>

<script src="/front/js/jquery.maskedinput.js"></script>
<script>
  $(document).ready(function() {

    $('#cep').mask('00000-000');

    if ($('#phone').val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
      $('#phone').mask('(00) 0000-0000');
    } else {
      $('#phone').mask('(00) 90000-0000');
    }

    $('#phone').blur(function(event) {
      if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
        $('#phone').mask('(00) 0000-0000');
      } else {
        $('#phone').mask('(00) 90000-0000');
      }
    });

    if ($('#cellphone').val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
      $('#cellphone').mask('(00) 0000-0000');
    } else {
      $('#cellphone').mask('(00) 90000-0000');
    }

  });


  var regexp = /[^a-zA-Z_ \-0-9]/g;
  $("#slug").on("input", function() {
    if (this.value.match(regexp)) {
      $(this).val(this.value.replace(regexp, ''));
    }
  });

  // Incremento para URL + categoria sem acentos
  function remAcentos(v) {
    var c = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ';
    var s = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
    var n = '';
    for (var x = 0; x < v.length; x++) {
      c.search(v.substr(x, 1)) >= 0 ?
        n += s.substr(c.search(v.substr(x, 1)), 1) :
        n += v.substr(x, 1);
    }
    return n;
  }

  var nameCategoria = $('select#category_id option:selected').html().replace('&amp;', 'e');
  $('.categoriaselected').html(remAcentos(nameCategoria.toLowerCase()));

  $('select#category_id ').on('change', function() {
    var nameCategoria = $('select#category_id option:selected').html().replace('&amp;', 'e');
    $('.categoriaselected').html(remAcentos(nameCategoria.toLowerCase()));
  });
  // Incremento para URL + categoria sem acentos
</script>
@endsection