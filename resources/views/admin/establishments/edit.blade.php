@extends('admin.base')
@section('title', 'Editar Estabelecimento')

@section('content')
<!-- Start Page Header -->
<div class="page-header">
  <h1 class="title">@yield('title') - {{ $establishment->title }}</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/dashboard') }}">Inicio</a></li>
    <li><a href="{{ route('establishments.index') }}">Página de Estabelecimento</a></li>
    <li class="active">@yield('title')</li>
  </ol>
</div>
<!-- End establishment Header -->


<!-- START CONTAINER -->
<div class="container-default">

  <div class="container-padding">
    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
          @include('elements.messages')
          <!-- Drop zone -- testar depois -->
          <!-- Drop zone -- testar depois -->
          <!-- Drop zone -- testar depois -->
          <!-- Drop zone -- testar depois -->
          <form method="post" enctype="multipart/form-data" action="{{url("/dashboard/establishments-upload/$establishment->id")}}" class="dropzone hide" id="myDropZone">
            {!! csrf_field() !!}
            <h4>Novas Miniaturas</h4>
            <div class="dz-message" data-dz-message><span></span></div>
          </form>
          <button type="button" id="submit-all" class="btn btn-thema hide" style="margin-bottom:40px;"><i class="fa fa-check"></i> Salvar Miniaturas</button>

          <!-- Drop zone -- testar depois -->
          <form action="{{ route('establishments.update', $establishment->id)}}" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="_method" value="PUT">
            @component('admin.establishments.form')
            <div class="row">
              <div class="col-xs-12 col-md-4">
                <div class="heading-block">
                  <h4>Foto principal do anúncio</h4>
                </div><!-- heading -->
                <div class="col-xs-12">
                  <div class="panel panel-default">
                    <div class="panel-body text-center">
                      <div id="box-imagens" class="gallery-box capa">
                        <ul>
                          @if($establishment->image)
                          <li>
                            <picture>
                              <img id="capa-0" src="{{ asset('storage/establishments/'. $establishment->id .'/photos/' . $establishment->image) }}" alt="{{ $establishment->slug }}" class="img-responsive">
                            </picture>
                          </li>
                          @endif
                        </ul>
                        <p><b>Use seu logo</b></p>
                      </div><!-- gallery -->

                      <button class="btn btn-sm btn-default mb-3" type="button" id="add-image-to-establishment"><i class="fa fa-edit"></i> Editar Capa</button>
                      <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i> Salvar Capa</button>
                      <div class="form-group">
                        <input type="file" style="opacity:0; height:15px;" name="image" id="input-add-image" accept="image/jpg, image/jpeg, image/png">
                        @if ($errors->has('image'))
                        <div class="alert alert-danger">
                          <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                          </span>
                        </div><!-- alert -->
                        @endif
                      </div><!-- form-group -->
                    </div>
                  </div><!-- panel -->
                </div><!-- col -->
              </div><!-- col -->

              <div class="col-xs-12 col-md-8">
                <div class="heading-block">
                  <h4>Miniaturas</h4>
                </div><!-- heading -->
                <div class="col-md-4 @if ($errors->has('photos.0')) has-error @endif">

                  <span id="erro-image" class="help-block @if($establishment->photos) hide @endif">
                    <div class="alert alert-danger">
                      <p><strong>É obrigatório uma imagem de capa</strong></p>
                    </div>
                  </span>
                  <span class="error-image"></span>
                </div><!-- col -->

                @slot('establishment', $establishment)
                <div class="col-xs-12 col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      @php $qtaPhotos = count($establishment->photos) @endphp
                      <div id="miniaturas-dropzone"></div>
                      <div id="box-imagens-miniaturas" class="gallery-box">
                        <ul>
                          <li style="border-right:1px solid #DDD;">
                            <picture>
                              <input type="file" style="opacity:0; height:15px; width:40px;" name="photos[]" id="input-add-photos" multiple accept="image/jpg, image/jpeg, image/png">
                              <button class="btn btn-default" id="add-photos-to-establishment" type="button">
                                <i class="fa fa-plus"></i>
                                <span>Nova Miniatura</span>
                              </button>
                            </picture>

                            <hr class="invisible my-1">
                            @for ($i = 0; $i < 5; $i++) @php $errorsPhotos='photos.' .$i @endphp @if ($errors->has($errorsPhotos))
                              <div class="alert alert-danger">
                                <span class="help-block">
                                  <strong>{{ $errors->first($errorsPhotos) }}</strong>
                                </span>
                              </div><!-- alert -->
                              @endif
                              @endfor
                          </li>

                          @forelse($establishment->photos as $photo)
                          <li id="photo-{{ $photo->id }}">
                            <picture>
                              <img id="photo-preview-{{ $photo->id }}" src="{{ asset('storage/establishments/'. $establishment->id .'/photos/' . $photo->filename) }}" alt="{{ $establishment->slug }}" class="img-responsive" alt="#{{ $photo->id}}">
                            </picture>
                            <div class="btn-group btn-group-xs">
                              <button class="btn btn-block btn-danger" type="button" onclick="remove({{ $establishment->id }}, {{ $photo->id }})"><i class="fa fa-trash"></i></button>
                            </div><!-- btn-group -->
                          </li>
                          @empty
                          @endforelse
                        </ul>
                      </div><!-- gallery -->
                      <p><small>- Tamanho máximo de 2mb por foto.</small></p>
                    </div><!-- panel -->
                  </div>
                </div>

              </div><!-- col -->
            </div><!-- row -->
            <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="heading-block">
                  <h4>Ferramenta para Fidelização do Consumidor</h4>
                </div>
                <div class="form-group @if ($errors->has('promotion')) has-error @endif">
                  <label for="promotion" class="form-label">Fidelize aqui seu cliente oferecendo uma promoção exclusiva, um código de desconto ou um brinde</label>
                  <textarea name="promotion" id="promotion" cols="30" rows="3" class="form-control" placeholder="Fidelize aqui seu cliente oferecendo uma promoção exclusiva, um código de desconto ou um brinde">{{ $establishment->promotion ??  old('promotion') }}</textarea>
                  @if ($errors->has('promotion'))
                  <span class="help-block">
                    <strong>{{ $errors->first('promotion') }}</strong>
                  </span>
                  @endif
                </div><!-- form-group -->
                <hr>
              </div><!-- cols -->
            </div>
            @endcomponent
            </form>

          </div><!-- panel-body -->


        </div><!-- panel-default -->
      </div><!-- col-md-12 -->


    </div><!-- row -->
  </div><!-- container-padding -->

</div><!-- container-default -->
<!-- END CONTAINER -->
@endsection

@section('cssPage')
<link href="{{ asset('front/js/summernote/summernote.css?13') }}" rel="stylesheet">
<link href="{{ asset('front/js/summernote/summernote-bs4.css?13') }}" rel="stylesheet">

<link type="text/css" rel="stylesheet" href="/js/dropzone/basic.css?1" />
<link type="text/css" rel="stylesheet" href="/js/dropzone/dropzone.css?1" />
<style>
  .dropzone {
    min-height: 150px;
    border: 2px solid rgb(58, 184, 191);
    background: white;
    padding: 10px;
    margin: 10px;
    border-radius: 20px;
  }

  .dropzone h4 {
    color: rgb(58, 184, 191);
  }

  .dropzone .dz-preview .dz-remove {
    font-size: 12px;
  }
</style>
@endsection

@section('jsPage')
<!-- ================================================
Bootstrap WYSIHTML5
================================================ -->
<!-- main file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js"></script>
<!-- bootstrap file -->
<script type="text/javascript" src="/backend/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>


<!-- ================================================
Summernote
================================================ -->
<script type="text/javascript" src="/front/js/summernote/summernote.js?2"></script>
<script type="text/javascript" src="/front/js/summernote/lang/summernote-pt-BR.js?2"></script>

<!-- ================================================
Summernote
================================================ -->
<script>
    // Ao clicar no button desabilita botao e coloca icon loading depois de 3s retira
    $("form").submit(function(e) {
      //disable the submit button
      $("form .btn.btn-thema").attr("disabled", true);
      $('form .btn.btn-thema').parent().append('<i class="fa fa-spinner fa-spin ml-3"></i>');
      setTimeout(function() {
        $('form .btn.btn-thema').prop("disabled", false);
        $('form .btn.btn-thema').parent().find('svg.fa-spinner').html('');
      }, 4000);
    });
    </script>
<script type="text/javascript" src="/js/dropzone/dropzone.js"></script>
<script>
  Dropzone.options.myDropZone = {
    autoProcessQueue: false,
    maxFilesize: 5,
    maxFiles: 100,
    parallelUploads: 100,
    addRemoveLinks: true,
    dictRemoveFile: "Remover imagem",
    acceptedFiles: ".png,.jpg,.jpeg",
    init: function() {
      var submitButton = document.querySelector('#submit-all');
      myDropzone = this;
      submitButton.addEventListener("click", function() {
        myDropzone.processQueue();
      });
      this.on("addedfile", function(file) {
        if ($('#myDropZone').hasClass('hide')) {
          $('#myDropZone').removeClass('hide');
          $('#submit-all').removeClass('hide');
        }
        $('html, body').animate({scrollTop:150}, 'slow'); //slow, medium, fast
      });
      this.on("complete", function() {
        if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
          var _this = this;
          _this.removeAllFiles();
        }
        // list_image();
        $(this).scrollTop(0);
        location.reload();
      });
      // $('#myDropZone').append('Miniaturas Atualizas. Recarregando');
    },
  };
  $('#add-photos-to-establishment').click(function(e) {
    e.preventDefault();
    $("#myDropZone").click();
  });
</script>
<script>
  $("#type option:selected").each(function() {
    var selectedType = $(this).val();
    if (selectedType == 'Virtual') {
      $("#number").removeAttr("required");
      $("#reqnumber").html('');
    } else {
      $("#number").attr("required", "req");
      $("#reqnumber").html('(<strong class="text-danger">*</strong>)');
    }
  });

  $('#type').change(function() {
    var selectedType = $(this).val();
    if (selectedType == 'Virtual') {
      $("#number").removeAttr("required");
      $("#reqnumber").html('');
    } else {
      $("#number").attr("required", "req");
      $("#reqnumber").html('(<strong class="text-danger">*</strong>)');
    }
    // console.log(selectedType);
  });




  // var qtaPhotos = "{{$qtaPhotos}}";
  // var maxPhotos = '6';

  // var disPhotos = maxPhotos - qtaPhotos;

  // $("button[type='submit']").click(function() {
  //   var $fileUpload = $("input[type='file']");
  //   if (parseInt($fileUpload.get(0).files.length) > disPhotos) {

  //     $('body,html').stop(true).animate({
  //       'scrollTop': 0
  //     }, 700, 'easeOutQuad');

  //     $('.error-image').html('');

  //     if (qtaPhotos == '0') {
  //       $('.error-image').append('<br><div class="alert alert-danger">Você só pode enviar apenas ' + disPhotos + ' fotos.</div>');
  //     } else if (qtaPhotos == '6') {
  //       $('.error-image').append('<br><div class="alert alert-danger">Quantidade de fotos excedidas.</div>');
  //     } else {
  //       $('.error-image').append('<br><div class="alert alert-danger">Você só pode enviar mais ' + disPhotos + ' fotos.</div>');
  //     }

  //     // alert("Você só pode enviar "+ disPhotos +" fotos.");
  //     return false;
  //   }
  // });
  // console.log(qtaPhotos);
</script>
<script>

  function remove(establishment, photo) {
    let doc = $('#photo-' + photo);
    let request = $.ajax({
      url: '/minha-conta/estabelecimentos/' + establishment + '/photos/' + photo,
      type: 'DELETE',
      statusCode: {
        204: function() {
          doc.fadeOut(); // ocultar registro
          // qtaPhotos = qtaPhotos - 1;
          // console.log(qtaPhotos);
        }
      }
    })
  }

  /* Gerar Previa */
  $('#input-add-image').change(function(e) {
    console.log('updated.')
    const ref = e.target;

    if (!ref.files) return;

    let id = ref.id,
      // image = $('#photo-preview-' + id),
      image = $('#capa-0'),
      file = ref.files[0],
      old = image.attr('src')

    var reader = new FileReader();

    reader.onload = function(event) {

      image.css({
        filter: blur(1.5)
      })

      image.fadeOut(500, function() {
        image.attr('src', event.target.result);
        image.fadeIn(200, function() {
          image.css({
            filter: blur(0)
          })
        });
      })
    }
    //
    reader.readAsDataURL(file);
  });


  /* Adicionar arquivos */
  $('#add-image-to-establishment').click(function(e) {
    e.preventDefault();
    $("#input-add-image").click();
  });

  $("#input-add-photos").change(function(e) {
    let display = $('span#display-add-photos'),
      files = e.target.files,
      count = files.length;

    if (count > 0) {
      display.empty().html(count + ' selecionados.')
    } else {
      display.empty().html('0 selecionados.')
    }
  });



  /* BOOTSTRAP WYSIHTML5 */
  $('.textarea').wysihtml5();

  /* SUMMERNOTE*/
  $(document).ready(function() {
    $('.summernote').summernote({
      lang: 'pt-BR'
    });
    $('button[data-event="showImageDialog"], button[data-event="showVideoDialog"], button[data-event="codeview"], .note-toolbar .note-table').hide();
  });
</script>

<!-- Query String ToSlug - Transforma o titulo em URL amigavel sem acentos ou espaço -->
<script type="text/javascript" src="/front/js/jquery.stringToSlug.min.js"></script>
<script type="text/javascript">
  $('input[name="name"]').stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: 'input[name="slug"], span.establishmentname',
    space: '-',
    replace: '/\s?\([^\)]*\)/gi',
    AND: 'e'
  });
</script>

<script src="/front/js/jquery.maskedinput.js"></script>
<script>
  $(document).ready(function() {

    $('#cep').mask('00000-000');

    if ($('#phone').val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
      $('#phone').mask('(00) 0000-0000');
    } else {
      $('#phone').mask('(00) 90000-0000');
    }

    $('#phone').blur(function(event) {
      if ($(this).val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
        $('#phone').mask('(00) 0000-0000');
      } else {
        $('#phone').mask('(00) 90000-0000');
      }
    });

    if ($('#cellphone').val().length == 14) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
      $('#cellphone').mask('(00) 0000-0000');
    } else {
      $('#cellphone').mask('(00) 90000-0000');
    }

  });


  // Incremento para URL + categoria sem acentos
  function remAcentos(v) {
    var c = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ ';
    var s = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC-';
    var n = '';
    for (var x = 0; x < v.length; x++) {
      c.search(v.substr(x, 1)) >= 0 ?
        n += s.substr(c.search(v.substr(x, 1)), 1) :
        n += v.substr(x, 1);
    }
    return n;
  }

  var nameCategoria = $('select#category_id option:selected').html().replace('&amp;', 'e');
  $('.categoriaselected').html(remAcentos(nameCategoria.toLowerCase()));

  $('select#category_id ').on('change', function() {
    var nameCategoria = $('select#category_id option:selected').html().replace('&amp;', 'e');
    $('.categoriaselected').html(remAcentos(nameCategoria.toLowerCase()));
  });
  // Incremento para URL + categoria sem acentos


  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
    $('#slugcopied').append('<div class="alert alert-success" style="width:220px;">Copiado</div>');
    setTimeout(function(e) {
      $('#slugcopied').html('');
    }, 2000, this);
  }

</script>
@endsection