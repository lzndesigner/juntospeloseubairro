{!! csrf_field() !!}

{{ $slot }}

<!-- <div id="description" class="tab-pane active"> -->
    <div class="heading-block">
      <h4>Descrição da Postagem</h4>
    </div>
  <div class="form-group @if ($errors->has('title')) has-error @endif">
    <label for="title" class="form-label">Título da Postagem</label>
    <input type="text" class="form-control" id="title" name="title" value="{{ $blog->title ?? old('title') }}" placeholder="Título da Postagem" autofocus>
    @if ($errors->has('title'))
    <span class="help-block">
      <strong>{{ $errors->first('title') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->

  <div class="form-group @if ($errors->has('slug')) has-error @endif">
    <label for="slug" class="form-label">URL Amigável</label>
    <input type="text" class="form-control" id="slug" name="slug" readonly value="{{ $blog->slug ?? old('slug') }}" placeholder="URL Amigável">
    <span class="help-block">* É preenchido automaticamente.</span>
    @if ($errors->has('slug'))
    <span class="help-block">
      <strong>{{ $errors->first('slug') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->

  <div class="form-group @if ($errors->has('tags')) has-error @endif">
    <label for="tags" class="form-label">Tags relacionadas</label>
    <input type="text" class="form-control" id="tags" name="tags" value="{{ $blog->tags ?? old('tags') }}" placeholder="Tags relacionadas">
    @if ($errors->has('tags'))
    <span class="help-block">
      <strong>{{ $errors->first('tags') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->

  <div class="form-group @if ($errors->has('body')) has-error @endif">
    <label for="body" class="form-label">Conteúdo da Postagem</label>
    <textarea name="body" id="body" cols="30" rows="10" class="form-control summernote" placeholder="Conteúdo da postagem...">{{ $blog->body ??  old('body') }}</textarea>
    @if ($errors->has('body'))
    <span class="help-block">
      <strong>{{ $errors->first('body') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->

  <div class="form-group @if ($errors->has('status')) has-error @endif">
    <label for="status" class="form-label">Status da Postagem</label>
    <select class="form-control" id="status" name="status">
      @if(isset($client->status))
      @if($client->status == 0 )
      <option value="0" selected>Desabilitado</option> 
      <option value="1">Habilitar</option>
      @else
      <option value="0">Desabilitar</option> 
      <option value="1" selected>Habilitado</option>
      @endif
      @else
      <option value="1" selected>Habilitado</option>
      <option value="0">Desabilitado</option> 
      @endif
    </select>

    @if ($errors->has('status'))
    <span class="help-block">
      <strong>{{ $errors->first('status') }}</strong>
    </span>
    @endif
  </div><!-- form-group -->
<!-- </div>tab-pane -->

<hr>

<div class="row">
  <div class="col-md-6">
    <button type="submit" class="btn btn-thema"><i class="fa fa-check"></i>Salvar</button>
    <a href="{{ route('blogs.index') }}" class="btn btn-default"><i class="fa fa-reply"></i>Cancelar</a>
  </div><!-- col-md-6 -->
</div>