$('#cep').blur(function() {
    var cep = $.trim($('#cep').val().replace('-', ''));
    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {
        if (!("erro" in dados)) {

            console.log(dados);

            $('#address').val(dados.logradouro);

            // if (dados.bairro) {
            //     $('#distric').val(dados.bairro);
            // } else {
            //     $('#distric').val(unescape(dados.localidade));
            // }

            $('#city').val(unescape(dados.localidade));

            switch (dados.uf) {
                case 'AC':
                    ufname = 'Acre';
                    break;
                case 'AL':
                    ufname = 'Alagoas';
                    break;
                case 'AP':
                    ufname = 'Amapá';
                    break;
                case 'AM':
                    ufname = 'Amazonas';
                    break;
                case 'BA':
                    ufname = 'Bahia';
                    break;
                case 'CE':
                    ufname = 'Ceará';
                    break;
                case 'DF':
                    ufname = 'Distrito Federal';
                    break;
                case 'ES':
                    ufname = 'Espírito Santo';
                    break;
                case 'GO':
                    ufname = 'Goiás';
                    break;
                case 'MA':
                    ufname = 'Maranhão';
                    break;
                case 'MT':
                    ufname = 'Mato Grosso';
                    break;
                case 'MS':
                    ufname = 'Mato Grosso do Sul';
                    break;
                case 'MG':
                    ufname = 'Minas Gerais';
                    break;
                case 'PA':
                    ufname = 'Pará';
                    break;
                case 'PB':
                    ufname = 'Paraíba';
                    break;
                case 'PR':
                    ufname = 'Paraná';
                    break;
                case 'PE':
                    ufname = 'Pernambuco';
                    break;
                case 'PI':
                    ufname = 'Piauí';
                    break;
                case 'RJ':
                    ufname = 'Rio de Janeiro';
                    break;
                case 'RN':
                    ufname = 'Rio Grande do Norte';
                    break;
                case 'RS':
                    ufname = 'Rio Grande do Sul';
                    break;
                case 'RO':
                    ufname = 'Rondônia';
                    break;
                case 'RR':
                    ufname = 'Roraima';
                    break;
                case 'SC':
                    ufname = 'Santa Catarina';
                    break;
                case 'SP':
                    ufname = 'São Paulo';
                    break;
                case 'SE':
                    ufname = 'Sergipe';
                    break;
                case 'TO':
                    ufname = 'Tocantins';
                    break;
            }

            $('#state').val(unescape(ufname));
            // $('select[name="country_id"]').find('option[value="30"]').attr('selected', true);
        }
    });
});